function indexData() {
    return {
        page1:true,
        page2:false,
        page3:false,
        jsonData: [],
        tempJsonData: {},
        showDropdown:false,
        inputText: '',
        sortedOptions: [],
        task_id:'577e8663-5a72-47c0-b877-1103dfe81e28',
        data:'',
        currentImageUrl: '',
        isVisible: true,
        locName:'________',
        Download_progress:25,
        progress:0,
        loadImgProg:0,
        uniqueImgURL:"",
        tileIndex:1,
        tileIndex2:1,
        downloadImg:'https://blender-render.gryd.in/static/uploads/hero_test/eb43fbdc15a3/c9a975f69a62.jpg',
        renderImg: [],
        bgImg:[],
        tempIndax: 0,
        userId:'',
        life:0,
        generated_img_id:null,
        like:false,
        reports:false,
        liked:[false,false,false,false],
        report:[false,false,false,false],
        renderTaskId:[['',false],['',false],['',false],['',false]],
        fTaskId:null,
        engagement_id: null,
        ds: null,
        user_name:'Motor-bike Enthusiast',
        base64Image:null,
        badgeShow:false,

        init() {  
            this.regUser()  
        },
        regUser(){
            this.ds = new DaveService("https://dashboard.iamdave.ai", "hero_moto_corp", {"signup_apikey": "635c733e-c56b-3392-a442-b9f2c51508aa"});
            var that = this;
            function aft() {
                that.initConversation()
                that.updateSpinnerM()
                that.fetchData()
                if(Utills.getCookie("user_name")){
                    that.user_name =  document.cookie.split('; ')
                    .find(row => row.startsWith('user_name='))
                    .split('=')[1]; 
                }
                else{
                    that.sweetInput()
                }
            }
            if(Utills.getCookie("authentication")){
                aft()
            }else{
                const uniqueIdentifier = Math.floor(Math.random() * 1000000);
                const dynamicEmail = `dinesh+${uniqueIdentifier}@i2ce.in`;
                this.ds.signup({
                    "user_id": dynamicEmail,
                    "person_type": "visitor",
                    "source": "web-avatar",
                    "origin": window.location.origin,
                    "validated": true,
                    "referrer": document.referrer,
                    "application": "reimagine"
                }).then(aft)
            }
        },

        initConversation() { 
            this.ds.postRaw(
                "/conversation/deployment_harley_davidson/"+this.ds.user_id,
                {},
                (data) => { 
                    this.engagement_id = data["engagement_id"]
                },
                (error) => { 
                    console.log(error)
                }
            )
        },

        sortOptions() {
            this.sortedOptions.length = 0;
            let count = 0; 
            this.jsonData.forEach(item => {
                let name = item.name;
                if (name.toLowerCase().includes(this.inputText.toLowerCase()) && count < 5) {
                    this.sortedOptions.push(item);
                    count++; 
                }
            });
        },
        

        checkAvbl(){
            if(!this.inputText.toLowerCase() == ''){
                history.pushState(null, null, window.location.href);
                this.celeryCheck()
                this.data = {'name':this.locName}
                this.pageToggler(4,this.inputText.toLowerCase())
                this.interaction('queried')
            }
            else{
                Swal.fire({
                    icon: 'warning',
                    title: 'Opps..!',
                    text: "Text Field Cant Be Empty",
                    focusConfirm: false,
                    customClass: {
                        confirmButton: 'custom-b-class1', 
                    },
                })
            }
            document.getElementById('searchInput').value = "";
        },

        fetchData() {
            this.ds.list("generated_image?user_id=null", {}, (data) => { 
                if (data && data.data && Array.isArray(data.data)) {
                    const shuffledData = this.shuffleArray(data.data);

                    this.tempJsonData = shuffledData;
                    this.tempJsonData = this.tempJsonData.filter((item) => typeof item.name !== 'undefined');
                    for (const item of this.tempJsonData) {
                        if (item.liked1 || item.liked2 || item.liked3 || item.liked4) {
                                this.jsonData.push(item);
                        }
                    }
                    console.log(this.jsonData); 
                    this.isVisible = false
                    console.log(this.ds.headers)
                    this.interaction('home_page_loaded')
                    
                } else {
                    this.sweetAlertX("Request failed with status:"+response.status)
                }
            }, (error) => { 
                this.sweetAlertX(error.error)
            })
        },

        shuffleArray(array) {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
            return array;
        },

        pageToggler(pageNo,datas){
            this.tileIndex = 1
            const currentURL = window.location.href;
            this.life=0;
            this.progress = 0;
            if(pageNo == 1){
                this.Download_progress = 25,
                this.progress = 0,
                this.isVisible=false,
                this.liked=[false,false,false,false],
                this.report=[false,false,false,false],
                this.page1 = true
                this.page2 = false
                this.page3 = false
                
                
                this.like = false;
                
                this.reports = false;
                this.generated_img_id = null;
                this.tempIndax = 0;
            }
            if(pageNo == 2){
                this.celeryCheck()
                history.pushState(null, null, window.location.href);
                this.page1 = false
                this.page2 = true
                this.page3 = false
                this.data = datas
                this.currentImageUrl = datas.rendered1
            }
            if(pageNo == 3){
                this.page1 = false
                this.page2 = false
                this.page3 = true
                this.isVisible = false
                this.renderImg = []
                this.bgImg = []
                if(currentURL.includes("https://hero-motocorp-reimagine.iamdave.ai/?color=denim_black&variant=premium")) {
                    this.generatetask_id(this.data.name)
                    console.log('Ai1tsKI65en9aTi0n')
                } 
                else if (currentURL.includes("https://hero-motocorp-reimagine-uat.iamdave.ai/?color=denim_black&variant=premium")) {
                    this.pickRandomToken()
                }
                else if (currentURL.includes("http://127.0.0.1:5500")) {
                    // this.pickRandomToken()
                    this.generatetask_id(this.data.name)
                }
                // this.generatetask_id(this.data.name)
                // this.generateImgURL()
                this.updateSpinner()
            }
            if(pageNo == 4){
                this.page1 = false
                this.page2 = false
                this.page3 = true
                this.isVisible = false
                this.renderImg = []
                this.bgImg = []
                if(currentURL.includes("https://hero-motocorp-reimagine.iamdave.ai/?color=denim_black&variant=premium")) {
                    this.generatetask_id(datas)
                    console.log('Ai1tsKI65en9aTi0n')
                } 
                else if (currentURL.includes("https://hero-motocorp-reimagine-uat.iamdave.ai/?color=denim_black&variant=premium") ) {
                    this.pickRandomToken()
                }
                else if (currentURL.includes("http://127.0.0.1:5500")) {
                    // this.pickRandomToken()
                    this.generatetask_id(datas)
                }
                // this.generatetask_id(datas)
                // this.generateImgURL() 
                this.updateSpinner()
            }
            document.getElementById('searchInput').value = "";
        },

        celeryCheck(){
            fetch("https://blender-render.gryd.in/trigger_celery", {
                method: 'GET',
                headers: this.ds.headers,
            })
            .then((response) => response.json())
            .then((data) => {
                console.log(data); 
            })
            .catch((error) => {
                console.log(error)
            });
        },

        pickRandomToken() {
            const task_ids = [
                '9a4a9716-676e-4b85-abde-237e5ff432c7',
                '7d877370-07cd-4854-aae3-d6ef76d97d04',
                '25f7ab23-613f-4843-9fe5-7152e1f26312'
            ];
            const randomIndex = Math.floor(Math.random() * task_ids.length);
            this.task_id = task_ids[randomIndex];
            this.generateImgURL()
        },

        generatetask_id(location) {
            this.badgeShow=true;
            this.ds.postRaw(
                "/conversation/deployment_harley_davidson/"+this.ds.user_id,
                {
                    "engagement_id": this.engagement_id,
                    "customer_response": location,
                    "customer_state": "cs_prompt",
                    "system_response": "sr_init"
                },
                (data) => { 
                    const responseData = data;
                    this.task_id = responseData.data.data.task_id;
                    
                    if(this.task_id != 'bc3f8f4b-26b1-4c0b-b01a-2e560b6be3b6' && this.task_id != undefined){
                        
                        // this.badgeCreation();
                        console.log("Task ID:", this.task_id);
                        this.locName = responseData.data.data.prompt;
                        this.data = {'name':this.locName}
                        this.Download_progress = 45;
                        this.updateSpinner();
                        this.generateImgURL();
                    }
                    else{
                        this.sweetAlert('We apply limited usage slots per hour to provide a superior experience . Please come back later to experience this feature again')
                    }
                },
                (error) => { 
                    this.sweetAlert('Invalid input, Please enter location name')
                }
            )
        },
        
        generateImgURL() {
            const life = this.life;
            if (life < 31) {
                setTimeout(() => {
                    this.ds.postRaw(
                        "/pass_through_api/dave",
                        {
                            "_application": "goapi",
                            "task_id": this.task_id
                        },
                        (data) => {
                            const responseData = data;
                            // console.log(responseData);
                
                            if (responseData.status != "finished") {
                                this.life++; 
                                this.generateImgURL();
                                this.Download_progress = this.Download_progress + 1 
                                this.updateSpinner();
                            } else {
                                this.Download_progress = 77;
                                this.updateSpinner();
                                this.uniqueImgURL = responseData.task_result.image_url;
                                this.fetchImageData();
                                this.interaction('midjourney_responded')
                            }
                        },
                        (error) => { 
                            
                            this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.") 
                            
                            console.log(error)
                        }
                    );
                }, 10000); 
            }
            else{
                this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.")
            }
        },

        fetchImageData() {
            var x = window.location.href;
            var url = new URL(x);
            var color = 'denim_black'
            var model = 'premium'
            if(url.searchParams.get('color') && url.searchParams.get('variant')){
                color = url.searchParams.get('color')
                model = url.searchParams.get('variant')
            }

            var names = this.user_name.match(/.{1,10}(?:\s|$)/g)
            var locations = this.locName.match(/.{1,25}(?:\s|$)/g)
            // console.log(names[0] + ' >> '+names[1] + ' >> '+locations[0] + ' >> '+locations[1])

            if(names[0] == undefined){
                names[0] = 'Motor-bike Enthusiast'
            }
            if(names[1] == undefined){
                names[1] = ' '
            }
            if(locations[0] == undefined){
                locations[0] = ' '
            }
            if(locations[1] == undefined){
                locations[1] = ' '
            }
            // console.log('before data request tile num = '+this.tileIndex)
            fetch("https://blender-render.gryd.in/image_process", {
                method: 'POST',
                headers: this.ds.headers,
                body: JSON.stringify({
                    
                    "image_urls" : [this.uniqueImgURL],
                    "split": true,
                    "render_id" : "hd440x-reimagine",
                    "color" : color,
                    "variant" : model,
                    "tile_num": this.tileIndex,
                    "first_name":names[0],
                    "last_name": names[1], 
                    "location_1": locations[0], 
                    "location_2":locations[1],
                    
                }),
            })
            .then((response) => response.json())
            .then((data) => {
                // console.log(data); 
                const rpData = data;
                    // console.log(rpData)
                    if(rpData == undefined){
                        this.sweetAlert("We apply limited usage slots per hour to provide a superior experience . Please come back later to experience this feature again")
                    }
                    else{
                        this.life = 0;
                        this.renderTaskId[this.tileIndex-1][0] = rpData.task_id;
                        this.tileIndex++;
                        // console.log(this.renderTaskId)
                        // console.log(this.fTaskId)
                        // this.getRendredImg();
                        this.Download_progress = this.Download_progress + 2;
                        
                        // console.log(this.tileIndex)
                        const xint = setInterval(() => {
                            if (this.tileIndex <= 4) {
                                this.fetchImageData();
                            }
                            else{
                                clearInterval(xint)
                                // this.tileIndex = 1
                            }
                        }, 5000);
                        if(this.tileIndex == 2){
                            this.fTaskId = this.renderTaskId[0][0]
                            this.getRendredImg();
                        }
                    }
            })
            .catch((error) => {
                
                this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.") 
                
                console.log(error)
            });
        },   

        fetchImageData2() {
        
            var x = window.location.href;
            var url = new URL(x);
            var color = 'denim_black'
            var model = 'premium'
            if(url.searchParams.get('color') && url.searchParams.get('variant')){
                color = url.searchParams.get('color')
                model = url.searchParams.get('variant')
            }

            var names = this.user_name.match(/.{1,10}(?:\s|$)/g)
            var locations = this.locName.match(/.{1,25}(?:\s|$)/g)
            // console.log(names[0] + ' >> '+names[1] + ' >> '+locations[0] + ' >> '+locations[1])

            if(names[0] == undefined){
                names[0] = 'Motor-bike Enthusiast'
            }
            if(names[1] == undefined){
                names[1] = ' '
            }
            if(locations[0] == undefined){
                locations[0] = ' '
            }
            if(locations[1] == undefined){
                locations[1] = ' '
            }
                
            this.ds.postRaw(
                "/pass_through_api/dave",
                {
                    "_application":"blender_render",
                    "image_urls" : [this.uniqueImgURL],
                    "split": true,
                    "render_id" : "hd440x-reimagine",
                    "color" : color,
                    "variant" : model,
                    "tile_num": this.tileIndex,
                    "first_name":names[0],
                    "last_name": names[1], 
                    "location_1": locations[0], 
                    "location_2":locations[1],
                },
                (data) => {
                    const rpData = data;
                    // console.log(rpData)
                    if(rpData == undefined){
                        this.sweetAlert("We apply limited usage slots per hour to provide a superior experience . Please come back later to experience this feature again")
                    }
                    else{
                        this.life = 0;
                        this.fTaskId = rpData.task_id;
                        // console.log(this.fTaskId)
                        this.getRendredImg2();
                        this.Download_progress = this.Download_progress + 9;
                    }
                },
                (error) => { 
                   
                    this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.") 
                    
                    console.log(error)
                }
            );  
        },

        getRendredImg() {

            fetch("https://blender-render.gryd.in/result",{
                method: 'POST',
                headers: this.ds.headers,
                body: JSON.stringify({
                    
                    // "_application":"blender_render_status",
                    "task_id" : this.fTaskId,
                    
                }),
            })
            .then((response) => response.json())
            .then((data) => {
                // console.log(data);

                    if(data == undefined){
                        this.sweetAlert("We apply limited usage slots per hour to provide a superior experience . Please come back later to experience this feature again")
                    }
                    else{
                        if(data.status == "PENDING"){
                            this.Download_progress = this.Download_progress + 0.25;
                            this.updateSpinner();
                            if(this.life < 25){
                                this.life++
                                setTimeout(() => {
                                    this.getRendredImg();
                                }, 5000);
                            }
                            else{
                                if(this.renderImg.length == 0 && this.tileIndex2 == 4){
                                    this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.") 
                                } 
                                else if (this.tileIndex2 <= 3) {
                                    this.fTaskId = this.renderTaskId[this.tileIndex2][0]
                                    
                                    // rin.innerText = this.renderImg.length
                                    this.tileIndex2++;
                                    this.updateActive(this.tileIndex2-1)
                                    
                                    this.getRendredImg();
                                }
                            }
                        }
                        else{
                            // console.log(data.result[0].data.result[0].rendered_url)
                            this.Download_progress = 99;
                            this.updateSpinner();
                            const renderedUrl = data.result[0].data.result[0].rendered_url;
                            const bgUrl = data.result[0].data.result[0].source_url;
                            this.renderImg.push(renderedUrl)
                            this.bgImg.push(bgUrl)

                            this.downloadImg = this.renderImg[this.tempIndax];
                            this.updateRenderImg(0)
                            
                            var rin = document.getElementById('renImgNo')
                            
                            if (this.tileIndex2 <= 3) {
                                this.fTaskId = this.renderTaskId[this.tileIndex2][0]
                                this.getRendredImg();
                                rin.innerText = this.renderImg.length
                                this.updateActive(this.tileIndex2)
                                this.tileIndex2++;
                            }
                            else {
                                this.updateActive(this.tileIndex2)
                                rin.innerText = this.renderImg.length
                                this.tileIndex2 = 1;
                                this.postImgs()
                            }
                            switch (this.tileIndex2) {
                                case 1:
                                  this.interaction('blender_responded_one');
                                  break;
                                case 2:
                                  this.interaction('blender_responded_two');
                                  break;
                                case 3:
                                  this.interaction('blender_responded_three');
                                  break;
                                case 4:
                                  this.interaction('blender_responded_four');
                                  break;
                                default:
                                  console.log("The value is not 1, 2, 3, or 4");
                              }
                              
                            // this.interaction('blender_responded')
                        }
                    }
            })
            .catch((error) => {
                
                if(this.renderImg.length == 0 && this.tileIndex2 == 4){
                    this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.") 
                }    
                else if (this.tileIndex2 <= 3) {
                    this.fTaskId = this.renderTaskId[this.tileIndex2][0]
                    
                    // rin.innerText = this.renderImg.length
                    this.tileIndex2++;
                    this.updateActive(this.tileIndex2-1)
                   
                    this.getRendredImg();
                }
                console.log(error)
            });
            
        },

        getRendredImg2() {
            
            this.ds.postRaw(
                "/pass_through_api/dave",
                {
                    "_application":"blender_render_status",
                    "task_id" : this.fTaskId,
                },
                (data) => {
                    // console.log(data);

                    if(data == undefined){
                        this.sweetAlert("We apply limited usage slots per hour to provide a superior experience . Please come back later to experience this feature again")
                    }
                    else{
                        if(data.status == 'render in progress.'){
                            this.Download_progress = this.Download_progress + 1;
                            this.updateSpinner();
                            if(this.life < 9){
                                this.life++
                                setTimeout(() => {
                                    this.getRendredImg2();
                                }, 5000);
                            }
                            else{
                                this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.")
                            }
                        }
                        else{
                            this.Download_progress = 99;
                            this.updateSpinner();
                            const renderedUrl = data.response[0].result[0].data.result[0].rendered_url;
                            const bgUrl = data.response[0].result[0].data.result[0].source_url;
                            this.renderImg.push(renderedUrl)
                            this.bgImg.push(bgUrl)

                            this.downloadImg = this.renderImg[this.tempIndax];
                            this.updateRenderImg(0)
                            
                            var rin = document.getElementById('renImgNo')
                            this.tileIndex++;
                            if (this.tileIndex <= 4) {
                                this.fetchImageData();
                                rin.innerText = this.renderImg.length
                                this.updateActive(this.tileIndex-1)
                            } else {
                                this.updateActive(this.tileIndex-1)
                                rin.innerText = this.renderImg.length
                                this.tileIndex = 1;
                                this.postImgs()
                            }
                        }
                    }
                },
                (error) => { 
                    this.sweetAlert("We're thrilled with the overwhelming response from our amazing users! 🎉 However, at the moment, our image generation system is experiencing high demand. Please try after 30 minutes and we'll make sure your request gets the attention it deserves.") 
                    console.log(error)
                }
            );  
        },
        
        postImgs() {
            this.ds.postRaw(
                "/object/generated_image",
                {
                    "user_id":this.ds.user_id,
                    "name": this.data.name,
                    "liked1":this.liked[0],
                    "liked2":this.liked[1],
                    "liked3":this.liked[2],
                    "liked4":this.liked[3],
                    "report1":this.report[0],
                    "report2":this.report[1],
                    "report3":this.report[2],
                    "report4":this.report[3],
                    "rendered1": this.renderImg[0],
                    "rendered2": this.renderImg[1],
                    "rendered3": this.renderImg[2],
                    "rendered4": this.renderImg[3],
                    "generated_bg1": this.bgImg[0],
                    "generated_bg2": this.bgImg[1],
                    "generated_bg3": this.bgImg[2],
                    "generated_bg4": this.bgImg[3],
                },
                (data) => { 
                    console.log(data)
                    this.generated_img_id = data.generated_image_id
                },
                (error) => { 
                    console.log(error)
                }
            )
        },

        RepostImgs() {
            fetch("https://dashboard.iamdave.ai/object/generated_image/"+this.generated_img_id, {
                method: 'PATCH',
                headers: this.ds.headers,
                body: JSON.stringify({
                    
                    "liked1": this.liked[0],
                    "liked2": this.liked[1],
                    "liked3": this.liked[2],
                    "liked4": this.liked[3],
                    
                }),
            })
            .then((response) => response.json())
            .then((data) => {
                console.log(data); 
            })
            .catch((error) => {
                console.error(error);
            });
        },

        ReportImgs() {
            fetch("https://dashboard.iamdave.ai/object/generated_image/"+this.generated_img_id, {
                method: 'PATCH',
                headers: this.ds.headers,
                body: JSON.stringify({
                    
                    "report1":this.report[0],
                    "report2":this.report[1],
                    "report3":this.report[2],
                    "report4":this.report[3],
                    
                }),
            })
            .then((response) => response.json())
            .then((data) => {
                console.log(data); 
            })
            .catch((error) => {
                console.error(error);
            });
        },

        interaction(stage) {
            var bodyData;
            if(this.generated_img_id){
                bodyData = JSON.stringify({
                    
                    "stage" : stage,
                    "user_id" : this.ds.user_id,
                    "generated_image_id":this.generated_img_id
                    
                })
            }
            else{
                bodyData = JSON.stringify({
                    
                    "stage" : stage,
                    "user_id" : this.ds.user_id,
                    
                })
            }
            fetch("https://dashboard.iamdave.ai/object/interaction", {
                method: 'POST',
                headers: this.ds.headers,
                body: bodyData,
            })
            .then((response) => response.json())
            .then((data) => {
                console.log(data); 
            })
            .catch((error) => {
                console.error(error);
            });
        },

        updateRenderImg(dir){
            var rb = document.getElementById('slideRight')
            var lb = document.getElementById('slideLeft')
            if(dir == 1){
                // lb.style.visibility = 'visible';
                this.tempIndax++
                if(this.tempIndax < this.renderImg.length){
                    this.downloadImg = this.renderImg[this.tempIndax];
                    this.like = this.liked[this.tempIndax]
                    this.reports = this.report[this.tempIndax]

                }
                else{
                    // rb.style.visibility = 'hidden';
                    this.tempIndax = this.renderImg.length
                }
            }
            if(dir == 2){
                // rb.style.visibility = 'visible';
                this.tempIndax--
                if(this.tempIndax >= 0){
                    this.downloadImg = this.renderImg[this.tempIndax];
                    this.like = this.liked[this.tempIndax]
                    this.reports = this.report[this.tempIndax]

                }
                else{
                    // lb.style.visibility = 'hidden';
                    this.tempIndax = 0
                }
            }
            if(this.tempIndax <= 0){
                lb.style.visibility = 'hidden';
            }
            else{
                lb.style.visibility = 'visible';
            }
            if(this.tempIndax >= this.renderImg.length-1){
                rb.style.visibility = 'hidden';
            }
            else{
                rb.style.visibility = 'visible';
            }
            if(this.tempIndax >= 3){
                rb.style.visibility = 'visible';
            }
            if(this.tempIndax >= 4){
                this.tempIndax = this.tempIndax - 1
                this.sweetAlert2()
            }
        },

        badgeCreation(){
            const contentToCapture = document.getElementById('contentToCapture');

            html2canvas(contentToCapture, {
                backgroundColor: 'transparent', 
                }).then(function (canvas) {
                this.base64Image = canvas.toDataURL("image/png");
                console.log(this.base64Image);
                }).catch(function (error) {
                console.error('Error capturing content:', error);
            });
            this.badgeShow = false
        },

        downloadImage(imageUrl) {
            fetch(imageUrl)
            .then(response => response.blob())
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const link = document.createElement("a");
                link.href = url;
                link.download = imageUrl.split('/').pop();
                link.style.display = "none";
                document.body.appendChild(link);
                link.click();
                window.URL.revokeObjectURL(url);
                document.body.removeChild(link);
            })
            .catch(error => {
                this.sweetAlertX("error fetching image:"+error)
            });
        },

        imgLikeing(){
            
            if(this.liked[this.tempIndax]){
                this.liked[this.tempIndax] = false;
            }
            else{
                this.liked[this.tempIndax] = true;
            }
            
            this.like = this.liked[this.tempIndax]
            
            if(this.generated_img_id != null){
                this.RepostImgs()
            }
        },

        imgReporting(){
            
            if(this.report[this.tempIndax]){
                this.report[this.tempIndax] = false;
            }
            else{
                this.report[this.tempIndax] = true;
            }
            
            this.reports = this.report[this.tempIndax]
            
            if(this.generated_img_id != null){
                this.ReportImgs()
            }
        },

        updateSpinnerM() {
            var apro = document.getElementById('percentage1')
            if(this.progress < 96)
            {
                apro.textContent = `${this.progress}%`;
                this.progress++
                
                setTimeout(() => {
                    this.updateSpinnerM();
                }, 10);
            }
        },

        updateSpinner() {
            var apro1 = document.getElementById('percentage2')
            if(this.progress < this.Download_progress)
            {
                apro1.textContent = `${this.progress}%`;
                this.progress++
                
                setTimeout(() => {
                    this.updateSpinner();
                }, 550);
            }
            if(this.progress == 99)
            {
                this.isVisible = true;
                this.interaction('images_displayed')
            }
        },

        updateActive(currentActive) {
            let stepCircles = document.querySelectorAll(".circle");
            stepCircles.forEach((circle, i) => {
              if (i < currentActive) {
                circle.classList.add("active");
              } else {
                circle.classList.remove("active");
              }
            });
          
            const activeCircles = document.querySelectorAll(".active"); 
            this.updateProg(((activeCircles.length) / (stepCircles.length - 1)) * 100)
        },

        updateProg(stage){
            let prog = document.getElementById("progress");
            if(this.loadImgProg < stage && this.loadImgProg <= 100)
            {
                prog.style.width = this.loadImgProg + '%';
                this.loadImgProg++
                
                setTimeout(() => {
                    this.updateProg(stage);
                }, 100);
            }
        },

        // shareImage() {
        //     var get_comment = document.getElementById("user_comment").value;
        //     var shareText = 'HD X-440';
          
        //     if (!get_comment == " ") {
        //       shareText = get_comment;
        //     }
          
        //     const imageSrc = this.downloadImg;
          
        //     const dataUrl = this.getDataUrl(imageSrc);
          
        //     const emailBody = `
        //       <html>
        //         <body>
        //           <p>${shareText}</p>
        //           <img src="${dataUrl}" alt="Shared Image" />
        //         </body>
        //       </html>
        //     `;
          
        //     const shareUrl = `mailto:?subject=${encodeURIComponent('Shared Image')}&body=${encodeURIComponent(emailBody)}`;
          
        //     window.location.href = shareUrl;
        //   },
          
        //   getDataUrl(imageSrc) {
        //     const canvas = document.createElement('canvas');
        //     const context = canvas.getContext('2d');
        //     const image = new Image();
           
        //     canvas.width = image.width;
        //     canvas.height = image.height;
           
        //     image.onload = function () {
        //       context.drawImage(image, 0, 0);
        //     };
          
        //     image.src = imageSrc;
          
        //     return canvas.toDataURL('image/jpeg');
        //   },
          
        shareImage() {
            var get_comment = document.getElementById("user_comment").value
            var shareText = 'HD X-440';

            if(!get_comment == " "){
                shareText = get_comment
            }

            if (navigator.share) {
              navigator.share({
                title: 'Shared Image',
                text: shareText, 
                url: this.downloadImg,
              })
              .then(() => {
                console.log('Shared successfully');
              })
              .catch((error) => {
                console.log("Sharing error: " + error);
              });
            } else {
              const imageSrc = this.downloadImg;
          
              const shareUrl = `mailto:?subject=${encodeURIComponent('Shared Image')}&body=${encodeURIComponent(shareText)}%0A%0A${encodeURIComponent(imageSrc)}`;
          
              window.location.href = shareUrl;
            }
          },
        
        sweetAlert(error){
            Swal.fire({
                
                html: `<div class="center-image"><img src="/assets/imgs/Try Again Later.png" width="100" height="50" /></div><div class="custom-image-class"><img src="/assets/imgs/betaTag.png" width="50" height="25" /></div><p>${error}</p>`,
                customClass: {
                    confirmButton: 'custom-b-class1', 
                    image: 'custom-image-class',
                },
                // timer: 30000,
                willClose: () => {
                    if(this.page3){
                        window.location.assign(window.location.href)
                    }
                    else{
                        Swal.close();
                    }                }
            }).then((result) => {
                if (result.isConfirmed) {
                    if(this.page3){
                        window.location.assign(window.location.href)
                    }
                    else{
                        Swal.close();
                    }                }
            })
        },

        sweetAlertX(error){
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: error,
                customClass: {
                    confirmButton: 'custom-b-class1', 
                },            
            }).then((result) => {
                if (result.isConfirmed) {
                    
                        Swal.close();
                    }               
            })
        },

        sweetAlert2(){
            Swal.fire({
                title: 'Click here to',
                text: 'regenerate other options',
                showCloseButton: true,
                confirmButtonText: 'Re-Generate', 
                focusConfirm: false,
                customClass: {
                    confirmButton: 'custom-b-class', 
                    closeButton: 'custom-close-button-class',
                    icon: 'custom-i-class',
                    title: 'custom-t-class',
                },
            }).then((result) => {
                if (result.isConfirmed) {
                    this.tileIndex = 1
                    this.liked = [false,false,false,false]
                    this.like = false;
                    this.report = [false,false,false,false]
                    this.reports = false;
                    this.generated_img_id = null;
                    this.progress = 0;
                    this.Download_progress = 32;
                    this.tempIndax = 0;
                    this.isVisible = false
                    this.updateSpinner()
                    this.pageToggler(3,this.data)
                    this.interaction('reimagine_clicked')
                }
            });
        },

        sweetPop(){
            Swal.fire({
                text: `This is Image Generation AI that creates computer-generated visual content for creative use.
                Users are responsible for ensuring that the generated images comply with applicable laws and respect copyright.
                The AI-generated images do not represent real individuals, objects, or events.Accuracy and appropriateness of the generated images may vary.By using this service, you agree to these terms and conditions.
                Hero shall not be liable for any errors or omissions in the information provided here in.`,
                customClass: {
                    confirmButton: 'custom-b-class1', 
                },            
            })
        },

        sweetInput(){
            Swal.fire({
                title: 'Welcome to',               
                html: `
                <span class='custom-t-class2'>H-D x440 GEN AI EXPLORER!</span>
                    <div class="input-container">
                    <input type="text" id="username" class="swal2-input anim" maxlength="20" placeholder="Type in your name for your personalised badge">
                    <img src="/assets/imgs/SendIcon.svg" x-on:click="checkAvbl2">
                    </div>
                `,
                showCloseButton: true,
                confirmButtonText: 'Submit',
                focusConfirm: false,
                customClass: {
                    confirmButton: 'custom-b-class2', 
                    title: 'custom-t-class2',
                    text: 'custom-t-class2',
                },
            }).then((result) => {
                if (result.isConfirmed) {
                    this.checkAvbl2()
                }
            });
            
        },

        checkAvbl2(){
            const inputValue = document.getElementById('username').value;
            if(!inputValue == ''){
                document.cookie = `user_name = ${inputValue}`;
                this.user_name = inputValue
                Swal.close();
            }    
            else{
                Swal.fire({
                    icon: 'warning',
                    title: 'Opps..!',
                    text: "Text Field Cant Be Empty",
                    focusConfirm: false,
                    customClass: {
                        confirmButton: 'custom-b-class1', 
                    },
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.sweetInput()
                    }
                });
            }        
        },
        addPopstateListener() {
            this.regUser()  
            this.interaction('back_pressed')
            window.location.assign(window.location.href);
          }
    };
}

window.addEventListener('popstate', function(event) {
    indexData().addPopstateListener()
});