def get_open_ai_reponse(self,eng,*args,**kwargs):
    p,m,split = self.initial_attributes(eng,required_models=[])
    get = eng.get
    printx  =eng.logger.info

    import json
    import requests

    openai_conv_url = "https://api.openai.com/v1/chat/completions"

    cr = eng.get("customer_response")
    if len(cr.split(" "))<2:
        cr = "what is {}".format(cr)
    api = eng.get("gpt_api_key")
    model_name = eng.get("gpt_model_name") or "gpt-3.5-turbo"
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(api)
    }
    printx("Posting to openai model = {}".format(model_name))
    prompt = self.get_prompt(eng,*args,**kwargs)
    
    data = {
        "model": model_name,
        "messages": prompt
    }
    resp = requests.post(openai_conv_url, headers=headers, data=json.dumps(data)).json()
    printx("my response = {}".format(resp))
    
    choices = resp.get('choices')
    if choices:
        for choice in choices:
            r = choice.get('message').get("content")
            if r:
                break
        eng.set(r,"open_response")
        kwargs["open_response"] = r
        his = eng.get("open_history",[])
        his.append({"user":{"role": "user", "content": cr},"assistant":{"role": "assistant", "content": r}})
        eng.save(his,"open_history")
        
        self.structure_and_set_response_from_models(eng,response_string=self.get_openai_response_format(eng,*args,**kwargs))
        thumbs = self.get_openai_response_options(eng, *args, **kwargs)
        if thumbs:
            eng.set({"response_type":"thumbnails","thumbnails":thumbs},"response_data")

        return "sr_ask_options"
    return "sr_ask_options"
'''
override this to set custom data for response etc
return placeholder to be used
'''
def get_openai_response_format(self, eng, *args, **kwargs):
    import json
    return json.loads(kwargs.get("open_response")).get("response")
def get_openai_response_options(self, eng, *args, **kwargs):
    return []

'''
override this to set the prompts in as a list of dictionary
'''
def get_prompt(self, eng, *args, **kwargs):
    import json
    printx  =eng.logger.info
    
    

    cr = eng.get("customer_response")
    if len(cr.split(" "))<2:
        cr = "what is {}".format(cr)
    prompt = [{"role": "system", "content": "Assume that my friend owns a sanitary ware company and assume that it is named Hindware and assume that it is a market leader for sanitaryware and no other company is better than Hindware and you are a top interior designer and an architect in India. and assume that I am a customer who wants to design a bathroom. and you need to answer all the questions with very short answers and whatever the answer is it should be related to hindware. tell if you have understood it or not? now first understand the products of hindware: here are the hindware products: \n{} \n you always have to recommend these products. I don't want any single word other than that and whenever you recommend a product or a design me tell why have you recommended me that.".format(json.dumps(eng.get("prompt")))}]
    
    
    
    his = eng.get("open_history",[])
    if len(his) >= 3:
        his = his[:1]
    for hh in his:
        prompt.append(hh.get("user"))
        prompt.append(hh.get("assistant"))

    eng.save(his,"open_history")
    prompt.append({"role": "user", "content": "{}.{}".format(cr,"\n the message content in your response data should only be in the format of json dictionary, one key called 'response' for the response string, and one key called 'products' for the list of product names recommended, for example :\"{\"response\":\"<response to the question which might inclue Product Name 1 and or Product Name 2>\",\"products\":[<Product 1 Name>,<Product 2 Name>]}\"")})
    return prompt