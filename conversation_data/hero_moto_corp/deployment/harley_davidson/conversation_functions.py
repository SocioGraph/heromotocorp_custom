dave_imports  openai_defaults, dave_defaults


def get_prompt(self,eng,*args,**kwargs):
    import json
    printx  =eng.logger.info
    
    

    cr = eng.get("customer_response")
    if len(cr.split(" "))<2:
        cr = "what is {}".format(cr)
    prompt = [{"role": "system", "content": "I need you to find out if any place or monument or any location is mentioned in the phrase."}]

    prompt.append({"role": "user", "content": "The phrase is - {}. your response should be an un-indented RFC8259 compliant JSON response following this format without deviation :{}".format(cr,"{\"location\":\"Mumbai\"}")})
    return prompt



def post_prompt(self,eng,*args,**kwargs):
    import time
    printx  =eng.logger.info
    LIMIT = 6
    LIMIT_TIME = 1*60*60
    lst=[]
    req_hist = eng.get("request_history",[])
    printx("req_hist - {}".format(req_hist))
    
    if len(req_hist) >= LIMIT:
        if (time.time()-req_hist[0])<LIMIT_TIME:
            eng.set({"Error":"Limit Exceeded"},"res")
            return "sr_prompt"
        else:    
            lst = req_hist[1:]
            eng.save(lst,"request_history")         
    if not lst:
        lst=req_hist
    
    lst.append(time.time())
    eng.save(lst,"request_history")
    printx("API call allowed")
    import requests
    import json

    X_API_KEY = "d4bf2941-f667-4e01-bb62-e29a86721fa1" 
    # X_API_KEY = "e7906eb4-ce15-4fb5-813d-f2dae866d3d7" 

    endpoint = "https://api.midjourneyapi.xyz/mj/v2/imagine"

    headers = {
        "X-API-KEY": X_API_KEY
    }
    cr = eng.get("customer_response")

    self.get_open_ai_reponse(eng,*args,**kwargs)


    if eng.get("open_response"):
        place_name = json.loads(eng.get("open_response")).get("location","road")
        pr = "a pristine two lane empty road in the center, no humans, no potholes, leading to {}, shot on 35mm lens, eye-level shot, realistic.".format(place_name)
        data = {
            "prompt": pr,
            "aspect_ratio": "1:1",
            "process_mode": "fast",
            "webhook_endpoint": "",
            "webhook_secret": ""
        }

        response = requests.post(endpoint, headers=headers, json=data)

        resp_dat = response.json()
        printx(resp_dat)
        resp_dat["prompt"] = place_name 
        eng.set(resp_dat,"res")