dave_imports  dave_defaults


def post_prompt(self,eng,*args,**kwargs):
    import time
    printx  =eng.logger.info
    LIMIT = 60000
    LIMIT_TIME = 1*60*60
    lst=[]
    req_hist = eng.get("request_history",[])
    printx("req_hist - {}".format(req_hist))
    
    if len(req_hist) >= LIMIT:
        if (time.time()-req_hist[0])<LIMIT_TIME:
            eng.set({"Error":"Limit Exceeded"},"res")
            return "sr_prompt"
        else:    
            lst = req_hist[1:]
            eng.save(lst,"request_history")         
    if not lst:
        lst=req_hist
    
    lst.append(time.time())
    eng.save(lst,"request_history")
    printx("API call allowed")
    import requests
    import json

    X_API_KEY = "d4bf2941-f667-4e01-bb62-e29a86721fa1" 
    # X_API_KEY = "e7906eb4-ce15-4fb5-813d-f2dae866d3d7" 

    endpoint = "https://api.midjourneyapi.xyz/mj/v2/imagine"

    headers = {
        "X-API-KEY": X_API_KEY
    }
    cr = json.loads(eng.get("customer_response"))
    pr = "a {} {} in {} in the future. 4k image set in the future.".format(cr.get("adjective"),cr.get("vehicle"),cr.get("location"))
    data = {
        "prompt": pr,
        "aspect_ratio": "16:9",
        "process_mode": "fast",
        "webhook_endpoint": "",
        "webhook_secret": ""
    }

    response = requests.post(endpoint, headers=headers, json=data)

    resp_dat = response.json()
    printx(resp_dat)
    eng.set(resp_dat,"res")
    mod  = base_model.Model("hero_mj_images",eng.get("enterprise_id"))
    mod.post({"name" : "","tag1" : cr.get("adjective"), "tag2" : cr.get("vehicle"), "tag3" : cr.get("location"),"name" : cr.get("name"),"task_id":resp_dat.get("task_id") })

