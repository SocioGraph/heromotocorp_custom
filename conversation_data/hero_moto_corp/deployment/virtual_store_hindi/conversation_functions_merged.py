
###############  G:/hero/heromotocorp_custom/conversation_data\dave_defaults/conversation_functions.py #############


def get_greeting(self,engagement,tz = None, *args,**kwargs):
    """
    This is a customer greeting function get called at the time of __init__ call and return greeting text.

    Input Args:
      - greet_text. 
        1. If it has {day_greeting} it will get replace with GM.GE etc. based on current time.
        2. If It has {customer_name} it will get replaced with user first_name, if they are in Facebook or Instagram or Twitter
      - customer_name (optional input)
        1. If present {customer_name} gets replaced with customer_name
      - bot_name (optional input)
        1. If present {bot_name} gets replaced with bot_name
     
    Example:
        greet= self.customer_greeting(engagement,greet_text = "Hi {customer_name}, {day_greeting} I am {bot_name} how can I help you.")
        
        Output:
        Hi Nikit, Good morning I am dave how can I help you.
    """
    cust = engagement.get("customer_profile",{})
    channel_id = cust.get("source",None)
    s = kwargs.get("greet_text",None)
    n = kwargs.get("customer_name", None)
    
    grt = None
    gm = self.validate_datetime(engagement, tz=tz, check_date =False,time_limit = [0,12])
    ga = self.validate_datetime(engagement, tz=tz, check_date =False,time_limit = [12,17])
    ge = self.validate_datetime(engagement, tz=tz, check_date =False,time_limit = [17,24])
    
    if gm["valid_time"]:
        grt = "Good Morning"
    elif ga["valid_time"]:
        grt = "Good Afternoon"
    elif ge["valid_time"]:
        grt = "Good Evening"
    else:
        grt  = "Good Day"
    
    if s:
        if "{day_greeting}" in s:
            if grt:
              s = s.replace("{day_greeting}",grt)
            else:
                s = s.replace("{day_greeting}","")

        if "{bot_name}" in s:
            s= s.replace("{bot_name}","")

        if channel_id:
            if channel_id in ["instagram","facebook","twitter"]:
                if "{customer_name}" in s:
                    s= s.replace("{customer_name}",cust.get("first_name")) 
            s  = s.replace("{customer_name}","")
        elif n:
            s = s.replace("{customer_name}", n)
        else:
            s = s.replace("{customer_name}","")

    return s


def get_model(self, eng, mod  = None,*args, **kwargs):
    """
    Input Args:
    1. mod - mod is a kwarg either a model name ( single string )  or a list of model names.
    2. engagement - engagement as arg.
    Output:
    1. If mod is a  string - it will return a model object
    2. If mod is a list -  it will return a dictionary where key will be model name and value will be model object

    Example:
    1. m = self.get_model(engagement, mod = "product")
    ===> m - product model object
    2. m = self.get_model(engagement, mod = ["product","customer"])
    ===> m - dictionary having product and customer model objects
    """ 
    printx = eng.logger.info; get = eng.get; save = eng.save; M = base_model.Model
    m = {}
    mod = mod or []
    if bool(mod):
        if isinstance(mod, list):
            for i in mod:
                try:
                  m[i] = M(i, eng.enterprise_id)
                except:
                    m[i] =  None
            return m
        elif isinstance(mod, str):
            try:
                m = M(mod, eng.enterprise_id)
                
            except:
                m = None
            return m
    return m
   
def split(self, eng, *args, **kwargs):
    get = eng.get
    channel_id = get("customer_profile",{}).get("source",None)
    return self.spliters(channel_id) if channel_id else self.spliters("test")

def attributes(self, engagement, *args, **kwargs):
    """
    store the value if that is in required
    """
    pass


def initial_attributes(self,engagement,*args,**kwargs):
    p = {}
    m= {}

    response  = ''
    k = kwargs.get("required_models",["product"])
   
    channel_id = engagement.get("customer_profile",{})
    channel_id = channel_id.get("source",None)
    split =  self.spliters(channel_id) if channel_id else self.spliters("test")

    #Base cases - comented for testing
    for i in k:
        m[i+'_model'] = base_model.Model(i, engagement.enterprise_id)

    return p,m,split

def spliters(self,channel_id):
    spliters_dict = {
        "whatsapp_business": "\n",
        "facebook": u"\u000A",
        "twitter": "\n",
        "test": "<br>",
        "instagram": "\n",
        "web-avatar": "<br>",
        "web-chatbot": "<br>"
    }
    return spliters_dict[channel_id]

def parse_selected_option(self, engagement, *args, **kwargs):
    n = None
    
    k = [("en_four","4")]

    for i,v in k:
        if engagement.get(i):
            n = v
            break
        
    if engagement.get("_number_"):
        n = engagement.get("_number_")

    if engagement.get("intent"):
        kwargs["number_picked"] = n
        return self.process_intent(engagement, *args, **kwargs)
    return
def set_tag(self,engagement,tag_level,tag):
    engagement.set(tag,tag_level)

def get_missing_gl_val(self, engagement, *args, **kwargs):
    
    required = engagement.get("required")
    #check if there are required variables for the intent
    if required and len(required) > 0:
        # get first requirement
        i = required[0]
        engagement.logger.info("trying to get variable = {}".format(i))
        required_value = engagement.get("gl_"+i) if i else None
        engagement.logger.info("got required value = {}".format(required_value))
        self.set_tag(engagement,"_tag_1",engagement.get("intent"))
        self.set_tag(engagement,"_tag_2",i)
        if not required_value:
            engagement.logger.info("trying to run function == gl_{} bool == {}".format(i,callable(getattr(self, "get_"+ (i if i else ""), None))))
            # confirm if requirement is met or not
            f = getattr(self,"get_"+i)

            #check if function exists for required variable
            if not callable(getattr(self, "get_"+i, None)):
                engagement.logger.err("get_{} function not defined for intent = {}".format(i,i))
                return  self.process_unknown(engagement)
            ret = f(engagement,*args,**kwargs)
            # if current requirement has sub requirements or is satisfied, returns null after adding to requirement list
            if not ret:
                # if new requirements added, call again to do those first
                return self.get_missing_gl_val(engagement,*args,**kwargs)
            #return system response name for current variable question with options list
            engagement.logger.info("returning system response as = "+ret)
            return ret
        else:
            #if requirement is already met, pop top and call again for next required var 
            self.pop_from_req(engagement)
            return self.get_missing_gl_val(engagement,*args,**kwargs)
    # no further required vars then return none and try do intent
    return None


def get_option_list_from_pivot(self,engagement, pivot_data):
    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(pivot_data.keys())
    options_str_list = ["{}. {}{}".format(k+1,v,split) for k,v in options]
    options_str = "".join(options_str_list)
    return options_str


def get_option_list_from_filter(self,engagement, filter_list, name_key):

    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(filter_list)
    if type(name_key) is list:
        options_list = []
        for k,v in options:
            vals = [v.get(name_keyy) for name_keyy in name_key]
            options_list.append(", ".join(vals))
        return options_list
    
    options_list = [v.get(name_key) for k,v in options]
    
    return options_list


def get_missing_gl_sub_req_val(self, engagement, *args, **kwargs):
    for i in engagement.get("sub_required",[]):
        if not engagement.get("gl"+i):
            return i
    return None


def add_to_required(self, engagement, var):
    hp.logger.info("adding {} to required".format(var))
    current_required = engagement.get("required")
    current_required.insert(0,var)
    engagement.save(current_required,"required")
    hp.logger.info("required ==  {} ".format(engagement.get("required")))

def add_variable_to_engagment(self, engagement,var,val):
    engagement.save(val,"gl_"+var)

    self.pop_from_req(engagement,var)
    # required.pop(var)
    # engagement.save(val,"gl_"+var)
    pass

def pop_from_req(self,engagement,var=None):
    req = engagement.get("required")
    hp.logger.info("trying to remove var from req == {}".format(var))
    if var and var in req:
        hp.logger.info("current required list == {}".format(req))
        hp.logger.info("trying to remove == {}".format(var))
        req.remove(var)
    elif not var:
        if req:
            req.pop(0)
    self.set_tag(engagement,"_tag_4","saved_attr__{}".format(var))
    engagement.save(req,"required")
def before_set_intent(self, engagement, *args, **kwargs):
    ##overrride this to do something before any intent is set
    #get_entity_values(engagement) - uncomment this in order to get user mentioned entities in flow
    pass

def set_intent(self, engagement, *args, **kwargs):
    logger.info("Set intent kwargs == {}".format(kwargs))
    self.before_set_intent(engagement, *args, **kwargs)
  
    engagement.set(kwargs,"set_intent_kwargs")

    required = hp.copy(kwargs.get("required",[])) if isinstance(kwargs.get("required",[]),list) else []
    recheck_required = kwargs.get("recheck_required")
    if recheck_required:
        for attr in recheck_required:
            if engagement.get("gl_{}".format(attr)):
                engagement.save(engagement.get("gl_{}".format(attr)),"gl_old_{}".format(attr))
                engagement.save(None,"gl_{}".format(attr))
    engagement.save(kwargs.get("failure_response","unknown_customer_state"),"gl_failure_response")
    intent = kwargs.get("intent")
    #previous_intent = engagement.get("intent" )#T check if its a diff intent ask for confirmation
    if intent:
        engagement.save(None,"close_case")
        hp.logger.info("got required in if == {}".format(required))
        engagement.save(intent,"intent")
        engagement.save(required,"required")
    hp.logger.info("got intent == {}".format(intent))
    hp.logger.info("got required == {}".format(required))

    return self.process_intent( engagement, *args, **kwargs)

def process_unknown(self, engagement, *args, **kwargs):
    return engagement.get("gl_failure_response", "unknown_customer_state")

def before_process_intent(self, engagement, *args,**kwargs):
    pass

def process_intent(self, engagement, *args,**kwargs):
    self.before_process_intent(engagement, *args,**kwargs)
    previous_intent = engagement.get("intent",None)
    #incase there is no intent anymore
    if not previous_intent:
        engagement.logger.info("No intent found")
    ret = self.get_missing_gl_val(engagement, *args, **kwargs)
    
    if ret:
        return ret
    
    self.set_tag(engagement,"_tag_1",engagement.get("intent"))
    previous_intent = engagement.get("intent",None)
    engagement.logger.info("previous intent --{}".format(previous_intent))
    #incase there is no intent anymore
    if not previous_intent:
        engagement.logger.info("No intent found")
        return self.process_unknown(engagement)
    
    try:
        f = getattr(self,"get_{}".format(engagement.get("intent")))
    except:
        logger.info("f is None")
        f = None
    
    #check if get intent function exists
    
    if not f:
        ret = self.generic_responder(engagement)
        engagement.logger.info("get_{} function not defined for intent = {}".format(engagement.get("intent"),engagement.get("intent")))
        return  ret
    response = f(engagement,*args,**kwargs)
    if not response:
        return  self.process_unknown(engagement)
    engagement.save(None,"intent")
    engagement.save([],"required")
    if kwargs.get('is_unknown'):
        r = engagement.get('response') or ''
        r = "I'm not sure what I understand what you mean.<br>" + r
    return response 


#Formats your response with {splits} and adds options to data/whiteboard/placeholder according to channel
def structure_and_set_response(self, engagement, *args, **kwargs):
    '''
    
    what this is doing 
    input args
    output args or what it sets 
    example usage
    
    '''
    printx = engagement.logger.info
    channel_id = engagement.get("customer_profile",{})
    channel_id = channel_id.get("source",None)

    split =  self.spliters(channel_id) if channel_id else self.spliters("test")

    response_string = kwargs.get("response_string")
    response_data = kwargs.get("sr_data",{})
    response_whiteboard = kwargs.get("response_whiteboard","")
    customer_state = kwargs.get("customer_state")
    title_list = kwargs.get("title_list","")
    is_longtxt = kwargs.get("is_longtxt")
    res_opt = kwargs.get("type_options",[])
    response_options = kwargs.get("response_options")
    res_data = kwargs.get("data",None)
    channel_specific = kwargs.get("channel_specific", "")

    if response_string:
        if "{split}" in response_string:
            response_string = response_string.replace("{split}",split)
   

    if not channel_id or channel_id in ["test","dave","web","web-chatbot","web-avatar","instagram","facebook","nexa", "arena","kiosk","twitter","call","whatsapp_business"] :    
        response_data["response_type"] = "options"
        
        if response_options and isinstance(response_options, (dict,list,tuple)):


            if  is_longtxt and "{response_options}" in response_string and response_options and channel_id in ["twitter","instagram","whatsapp_business","facebook"]:
                options_list_string = split.join([u"{}. {}".format(i+1,v) for i,v in enumerate(response_options)])
                response_string = response_string.replace("{response_options}", options_list_string)
                response_options= ["Option " + str(i+1) for i,v in enumerate(response_options)]
            elif "{response_options}" in response_string:
                response_string = response_string.replace("{response_options}","")
                    
            if isinstance(response_options, list):
                if isinstance(response_options[0], (str, unicode)):
                    response_data["options"] = [[customer_state or "__init__", o] for o in response_options]
                if isinstance(response_options[0], list):
                    response_data["options"] = [[o[0], o[1]] for o in response_options]
                if isinstance(response_options[0], dict):
                    response_data["options"] = [[o.keys()[0], o.values()[0]] for o in response_options]
            elif isinstance(response_options, dict):
                response_data["options"] = response_options.items()
            else:
                response_data["options"] = []
                
        #For setting response_type and data other than response_type = options
    
        if res_data:
            check =False
            if channel_specific:
                if isinstance(channel_specific,list):
                    if channel_id in channel_specific:
                        check = True
                elif isinstance(channel_specific,(str, unicode)):
                    if channel_id == channel_specific:
                        check = True              
            elif channel_id in ["facebook","instagram"]:
                check = True
    
            if check:
                response_data= {}
                response_data = res_data
        #---end---

            res_whiteboard = kwargs.get("whiteboard")
            if res_whiteboard:
                response_whiteboard = res_whiteboard
        
        # type_options - logic
        if res_opt and response_data.get("response_type","") == "options":
            if not response_data.get("options"):
                response_data["options"] = res_opt
            else:
                response_data["options"].extend(res_opt)
        #---end---

    # formatting options for social 

    if not is_longtxt and channel_id in ["instagram"]:
            # response_options = kwargs.get("response_options")
        if response_options and isinstance(response_options,list):
            enumerated_options_list = enumerate(response_options)
            options_str_list = [u"{}. {}{}".format(k+1,v,split) for k,v in enumerated_options_list]
            options_str = "".join(options_str_list)
            if "{response_options}" in response_string:
                response_string = response_string.replace("{response_options}",options_str)
            else:
                response_string = u"{}{}{}".format(response_string, split, options_str)
     
    # formatting options for social

    elif channel_id in ["kiosk", "web3d","web-avatar"]:
        response_options = kwargs.get("response_options")
        if response_options and response_whiteboard in ["options","buttons"]:
            customer_state = kwargs.get("customer_state")
            if customer_state:
                buttons=[]
                for opt in response_options:
                    if type(opt)==list:
                        buttons.append({"customer_response" : opt[0],"customer_state" : customer_state, "title" : opt[1]})
                    else:
                        buttons.append({"customer_response" : opt,"customer_state" : customer_state, "title" : opt})
                response_data["buttons"] = buttons
        elif response_whiteboard in ["popup_url","redirect"]:
            redirect_url = kwargs.get("redirect_url")
            hp.logger.info("redirect_url {}".format(redirect_url))
            if redirect_url:
                response_whiteboard = response_whiteboard
                response_data["redirect"] = redirect_url
                hp.logger.info("response_whiteboard {}".format(response_whiteboard))

        elif response_whiteboard == "long_text":
            data=kwargs.get("long_text_data")
            if data:
                response_whiteboard ="long_text"
                response_data["title"]=data.get("title","")
                response_data["sub_title"]=data.get("sub_title","")
                response_data["description"]=data.get("description","")
                response_data["background"]=data.get("background","")
    engagement.set(response_string,"response")
    if response_whiteboard:
        engagement.set("{}.html".format(response_whiteboard),"reponse_whiteboard")
    elif engagement.get("default_whiteboard"):
        engagement.set("variant_info.html","reponse_whiteboard")
        engagement.set(engagement.get("default_whiteboard"),"variant_image")
    else :
        engagement.set("","reponse_whiteboard")
    engagement.set("static" if kwargs.get("custom_whiteboard") else "file" , "whiteboard_template")
    engagement.logger.info("kwargs data for nudges---{}".format(kwargs.get("nudges")))

    if kwargs.get("nudges"):
        response_data["_follow_ups"]=kwargs.get("nudges")
    engagement.logger.info("response data for nudges---{}".format(response_data))
    engagement.set(response_data if response_data else {},"response_data")

    return "sr_ask_options"
    # chatbot
    # response_type : "options"
    # "options": [
    #         <customer_response option>,
    #         <customer_response option>
    #      ],
    
    # kiosk
    # whiteboard = options.html
    # "whiteboard_template": "file"
    # "options": [
    #         <customer_response option>,
    #         <customer_response option>
    #      ],
    
def set_state_options(self, engagement, *args, **kwargs):
    state_options = kwargs.get("state_options",[])
    engagement.set( state_options, '_custom_state_options')
def get_option_list_from_filter_key_value(self,engagement, filter_list, name_dict):

    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(filter_list)
    if type(name_dict.keys()) is list:
        options_list = []
        for k,v in options:
            vals = ["{} : {}".format(valls,v.get(name_keyy)) for name_keyy,valls in name_dict.iteritems()]
            options_list.append(", ".join(vals))
        return options_list

def check_old_required(self, engagement, attr):
    return engagement.get("gl_old_{}".format(attr),None)

def reset_old_to_new_required(self, engagement, attr):
    self.add_variable_to_engagment(engagement,attr, engagement.get("gl_old_{}".format(attr)))
    engagement.save(None,"gl_old_{}".format(attr))

def get_option_list_from_filter_key_value(self,engagement, filter_list, name_dict):

    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(filter_list)
    if type(name_dict.keys()) is list:
        options_list = []
        for k,v in options:
            vals = ["{} : {}".format(valls,v.get(name_keyy)) for name_keyy,valls in name_dict.iteritems()]
            options_list.append(", ".join(vals))
        return options_list
        
    
def validate_datetime(self, engagement, tz= None, check_time = True,check_date=True,days_limit= 30,*args, **kwargs):
    printx = engagement.logger.info
    dt  = kwargs.get("datetime",hp.now(tz=tz)) 
   
    restrict_day = kwargs.get("restrict_day","").lower()[0:3]
    restrict_dates  = kwargs.get("restrict_dates",None)
    time_limit = kwargs.get("time_limit",None)

    dt_dict  = hp.OrderedDict()
    valid_day = False
    valid_time = False
    old_date = False
    present_time =False
    old_time  = False
    time_over = False
    valid_minute = False
    max_day_limit = False

    if check_date:
        if dt:
            fun = lambda x : [int(i) for i in x.strftime("%d:%m:%Y").split(":")]
            tl = fun(dt)
            ctl = fun(hp.now(tz=tz))
            q = hp.now(tz =tz) + hp.timedelta(days = int(days_limit))
            if bool(dt<=q):
                if tl[1]>=ctl[1]  and tl[2]>=ctl[2]:
                    if tl[1]==ctl[1] and tl[0]<ctl[0]:
                        old_date =True
                    if bool(restrict_day):
                        t = dt.strftime("%a").lower()
                        if restrict_day!=t:
                            valid_day = True
                    if bool(restrict_dates):
                        if isinstance(restrict_dates,(str, unicode)):
                            pass
                        elif isinstance(restrict_dates,list):
                            pass
                else:
                    old_date = True
            else:
                max_day_limit = True
  
    if check_time:
        t= int(dt.hour)
        tm = int(dt.minute)
        if t>0:
            present_time  = True
            if time_limit:
                dtl = dt.strftime("%H-%M").split("-")
                nowtl = hp.now(tz=tz).strftime("%H-%M").split("-")
                
                if t in range(time_limit[0],time_limit[1]):
                    valid_time =True
                    if tm<=60:
                        valid_minute = True
                        
                if int(nowtl[0])>=time_limit[1]:
                    time_over = True
                if dtl[0]<nowtl[0]:
                    old_time = True
                elif dtl[0]==nowtl[0] and dtl[1]<nowtl[1]:
                    old_time= True

    dt_dict["old_date"] = old_date
    dt_dict["valid_day"] = valid_day
    dt_dict["present_time"] = present_time
    dt_dict["valid_time"] = valid_time
    dt_dict["valid_minute"] = valid_minute
    dt_dict["old_time"] = old_time
    dt_dict["time_over"] = time_over
    dt_dict["max_day_limit"] = max_day_limit
      
    return dt_dict


def varify_phone_number(self, engagement, *args,**kwargs):
    num = kwargs.get("phone_number","").replace(" ", "")
    l= len(num)
    if l==11 and num[0] =="0":
        return True, num
    elif l==10:
        return True, num
    elif l==12 and num[0:2]=="91":
        return True, num
    return False, False
   

def unset_gl(self, eng, is_backup= True, *args, **kwargs):
    get = eng.get; save = eng.save
    gl_val = kwargs.get("gl_val",[])
    gl_backup_val = kwargs.get("gl_backup_val",[])
    if is_backup:
        self.backup_glvalues(eng,gl_val = gl_val)
    if gl_val:
        for i in gl_val:
            save(None,"gl_"+i)
    if gl_backup_val:
        for i in gl_val:
            save(None,"gl_backup_"+i)


def backup_glvalues(self, eng, *args, **kwargs):
    get = eng.get; save = eng.save
    gl_val = kwargs.get("gl_val",[])
    if gl_val:
         for i in gl_val:
            save(get("gl_"+i,None),"gl_backup_"+i)

def set_gl(self,eng, *args, **kwargs):
    """
    gl_val - list of tuples of gl-val and gl-name or one tuple
    """
    save = eng.save; printx = eng.logger.info
    gl_val = kwargs.get("gl_val")  
    if gl_val:
        if isinstance(gl_val, list):
            for val,name in gl_val:
                if name.startswith("gl_"):
                    save(val,name)
                else:
                    save(val,"gl_{}".format(name))
        elif isinstance(gl_val,tuple):
              if gl_val[1].startswith("gl_"):
                save(gl_val[0],gl_val[1])
              else:
                save(gl_val[0],"gl_{}".format(gl_val[1]))
                

def pagination(self, eng, make_titles = True,size = 5, *args, **kwargs):
    """
    gl_options_dict - options dict from parent function
    gl_operations_dict - operations dict from main function
    gl_data - any response data from parent function
    gl_filter_list - attributes to be filter from get_option_list_from_filter func
    gl_origin - set true on first parent function call
    """
   
    printx =eng.logger.info; save  = eng.save; get  = eng.get
   
    opt_initial  = get("gl_options_dict",[])
    operations = get("gl_operations_dict",{})
    cust_state = get("gl_customer_state")
    extra_options = get("gl_extra_options",[])
    data = get("gl_data",{})
    fl = get("gl_filter_list")
    nudges =  kwargs.get("nudges",[])

    is_origin = get("gl_origin",False)
    save(False,"gl_origin")
    if is_origin:
        self.set_gl(eng,gl_val = [(0,"next_options")])
    

    x = int(get("gl_next_options")) 
    if x==0:
        x =5
    y  = x + size
    save(y,"gl_next_options")
    opt = opt_initial[x:y]
    
    res_opt = [["cs_pagination","View more"]]
    txt = "or Type 'View more' to view more options."
    if len(opt)<size or (len(opt)==size and not opt_initial[y:]):
        res_opt = []
        txt = ''
    
    save(opt,"required_response_options")
    opt = self.get_option_list_from_filter(eng,opt,fl)
   
    if bool(operations):
        opt = self.string_formatting(eng,operations = operations,target= opt)

    if make_titles:
        op = hp.OrderedDict()
        op["split"] = ","
        tl = self.string_formatting(eng,operations = op,target= opt)
        title_list = [i[0] for i in tl]
    
    if extra_options:
        res_opt.extend(extra_options)

    is_longtxt  = get("gl_is_longtxt",False)
    response = "Here are the more options:{split}{split}{response_options}{split}{split}Select suitable one "+ txt
    self.structure_and_set_response(eng, response_string =response,response_options=opt,customer_state=cust_state,title_list=title_list,data=data,type_options = res_opt,is_longtxt = is_longtxt,nudges = nudges)
    return "sr_ask_options"



def string_formatting(self,engagement, known_words =None,*args, **kwargs):
   """
   target = x
   chars = y,
   tup = tuple on (y, z) -  replace y with z
   operations  = OrderedDict
   target = [ "N,i k,,i?t||pokhar?k_a_r", "i,,a,m?#dave _AI"] list or string or dict 
   Example:
   op["replace"]=  [("_"," "),(",",""),("?",""),(" ",""),("|",""),("#",""),("_","")]
   op["strip"] = ["N","d"]
   op["title"] = None 
   allow to pass the dict of values where we should not change case - 
   """
   known_words = known_words or {}
   sp = lambda x,y: x.split(y)
   rp = lambda x,tup: x.replace(tup[0],tup[1])
   st  = lambda x,y: x.strip(y)
   ew = lambda x,y: x.endswith(y)
   sw = lambda x,y: x.startswith(y)
   tl = lambda x,y: x.title()
   up = lambda x,y: x.upper()
   lw = lambda x,y: x.lower()
   lb = lambda x,y:  x+str(y)

   func_dict ={
       "split": sp,
       "replace": rp,
       "strip": st,
       "endswith":ew,
       "startswith":sw,
       "title": tl,
       "upper": up,
       "lower": lw,
       "linebreak":lb
   }

   target = kwargs.get("target","")
   op = kwargs.get("operations",{}) 
   cust_func = kwargs.get("custom_function")
   
   if callable(cust_func):
       func_dict["custom_function"] = cust_func
   elif isinstance(cust_func,dict):
       for key,val in cust_func.items():
           if callable(val):
                func_dict[key] = val

   if isinstance(target,str):
       if bool(op):
            for i,val in op.items():
                if isinstance(val,list):
                    for j in val:
                        target = func_dict[i](target,j)
                else:
                    target = func_dict[i](target,val) 

   elif isinstance(target,list):
       if bool(op):
            li = []
            for k in target:
                for i,val in op.items():
                    if isinstance(val,list):
                        for j in val:
                            k = func_dict[i](k,j)
                    else:
                        k = func_dict[i](k,val)
                li.append(k)
            target = li

   elif isinstance(target,dict):
       pass

   return target


def get_entity_values(self, eng, *args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    ens = [i.get("variable_name") for i in self.entities.values() if "variable_name" in i.keys()]
    en_vals = get("gl_entity_values",{})
    printx("entity values = %s",ens)
    if ens:
        for i in ens:
            new = get(i)  
            if new:
               en_vals[i] = new
            elif en_vals.get(i,None):
                en_vals.pop(i,None)
    printx("Current entity values= %s",en_vals)
    save(en_vals,"gl_entity_values")
    return None




def post_actions(self, eng, *args, **kwargs):
    action = kwargs.get("action")
    mod_name= kwargs.get("mod_name")
    query_dict= kwargs.get("query_dict")
    res = do_action( {"enterprise_id" : eng.enterprise_id, "role" : "admin"}, { '_action': action, '_name': '_via_api_{}@{}'.format(mod_name, hp.now()), '_model': mod_name, '_data': query_dict })
    
    return res
   

def generic_responder(self, engagement, *args, **kwargs):
    
    m = self.get_model(engagement, mod = "conversation_response")
   
    if not m:
        return self.process_unknown(engagement)
    return self.structure_and_set_response_from_models(engagement)


def find_attrs_in_placeholder(self, placeholder):
    return hp.re.findall(hp.re.escape("{")+"(.*?)"+hp.re.escape("}"),placeholder)
#Formats your response with {splits} and adds options to data/whiteboard/placeholder according to channel

def before_structure_and_set_response_from_models(self, engagement, *args, **kwargs):
    pass
def after_structure_and_set_response_from_models(self, engagement, *args, **kwargs):
    pass

def structure_and_set_response_from_models(self, engagement, *args, **kwargs):
    """
    First tries to get the response information from conversation_response model. based on intent and current required_attr. 

    Input Args:
      - sr_data 
        To specify any system response default "data" 
      - response_string
        send this as a failsafe if the response placeholder is not found in the conversation_response model
      - response_options
        list of options to be used to either create buttons/ enumerated list/ whitebaord buttons
      - customer_response
        incase customer response needs to be sent for all response_options buttons
      - response_whiteboard
        name of whiteboard to use
      - custom_whiteboard
        if you are using a custom whitebaord uploaded specific to your conversation provide true and add case for it
      - default_whiteboard
        ???????
      - response_language
        specify a language to use !!!!!!!!!!!!!!!
      - intent
        if intent is sent in kwargs it will be prioritized over engagement intent value. Use it if you want to structure response without changing currently running intent.
      
     
    Example:
         
    Output:
    
    """
    channel_id = engagement.get("customer_profile",{})
    channel_id = channel_id.get("source",None)

    split =  self.split(engagement)

    response_data = kwargs.get("sr_data",{})
    response_whiteboard = kwargs.get("response_whiteboard","")
    customer_state = kwargs.get("customer_state")
    response_options = kwargs.get("response_options")
    response_language = kwargs.get("response_language")
    system_response_name=kwargs.get("system_response","sr_ask_options")
    nudges =  kwargs.get("nudges",[])
    title_list = kwargs.get("title_list","")
    is_longtxt = kwargs.get("is_longtxt")
    res_opt = kwargs.get("type_options",[])
    res_data = kwargs.get("data",None)
    channel_specific = kwargs.get("channel_specific", "")

    
    resp_model = self.get_model(engagement, mod = "conversation_response")

    resp_filter_dict = {}
    response_string = ""
    intent = kwargs.get("intent",engagement.get("intent",None))
    ignore_required=kwargs.get("ignore_required", False)
    engagement.logger.info("----it---{}".format(intent))
    if intent:
        resp_filter_dict["intent"] = intent
        resp_filter_dict["conversation_id"] = engagement.get("conversation_id") 
        required = engagement.get("required")
        #check if there are required variables for the intent
        if not ignore_required and required and len(required) > 0:
            # get first requirement
            current_required = required[0]
            resp_filter_dict["intent"] = "{}.{}".format(intent,current_required)
        engagement.logger.info("resp_filter_dict-----------{}".format(resp_filter_dict))
        resp_data_list = list(resp_model.filter(**resp_filter_dict))
        
        engagement.logger.info("resp_data_list ------{}".format(resp_data_list))
        if nudges:
            response_data["_follow_ups"] = nudges
        engagement.logger.info("res_data ---{}".format(response_data))
        if resp_data_list:
            resp_data = resp_data_list[0]
            if not response_language:
                response_string = resp_data["placeholder"]
                
            elif response_language == "english":
                response_string = resp_data["placeholder_english"]
                
            elif response_language == "hindi":
                response_string = resp_data["placeholder_hindi"]

            elif response_language == "kannada":
                response_string = resp_data["placeholder_kannada"]
            if resp_data.get("whiteboard"):
                response_whiteboard = resp_data.get("whiteboard")
                if resp_data.get("whiteboard_template"):
                    kwargs["custom_whiteboard"] = resp_data.get("whiteboard_template")
            
            if resp_data.get("redirect"):
                response_data["redirect"] = resp_data.get("redirect")
            
            if resp_data.get("variant_image"):
                # response_data["variant_image"] = resp_data.get("variant_image")
                response_data["variant_image"] = self._make_url(engagement, resp_data.get("variant_image"))

            if resp_data.get("title"):
                response_data["title"] = resp_data.get("title")
            
            if resp_data.get("description"):
                response_data["description"] = resp_data.get("description")
            
            if resp_data.get("background"):
                response_data["background"] = resp_data.get("background")
            
            if resp_data.get("system_response"):
                system_response_name=resp_data.get("system_response")
            
            if resp_data.get("follow_ups"):
                response_data["_follow_ups"] = resp_data.get("follow_ups")
            
            

            attrs_missing = self.find_attrs_in_placeholder(response_string)
            kwarg_attrs_provided = kwargs.get("filler_data_dict",{})
            for k in attrs_missing:
                provided_data = kwarg_attrs_provided.get(k)
                if not provided_data:
                    engagement_key_name = "gl_"+k
                    provided_data = engagement.get("gl_"+k)
                    if not provided_data:
                        #TODO - check output_attr in the object to see what is mandatory to print response
                        hp.logger.info("intent {} does not have required attr to fill in placeholder for {}".format(intent,k))
                        
                        return self.process_unknown(engagement)
                response_string = response_string.replace("{"+k+"}",provided_data)
            hp.logger.info("I got response placeholder attrs as  == {}".format(self.find_attrs_in_placeholder(response_string)))
            
            engagement.logger.info("i got state_options == {}".format(resp_data.get("state_options")))
            if resp_data.get("state_options"):
                resp_state_options = { "state_options" : resp_data.get("state_options")}
                self.set_state_options(engagement, *args, **resp_state_options)
                
    if not response_string:
        response_string = kwargs.get("response_string")


    if response_string:
        if "{split}" in response_string:
            response_string = response_string.replace("{split}",split)
   

    if not channel_id or channel_id in ["test","dave","web","web-chatbot","web-avatar","instagram","facebook","nexa", "arena","kiosk","twitter","call","whatsapp_business"] :    
        response_data["response_type"] = "options"
        
        if response_options and isinstance(response_options, (dict,list,tuple)):


            if  is_longtxt and "{response_options}" in response_string and response_options and channel_id in ["twitter","instagram","whatsapp_business","facebook"]:
                options_list_string = split.join([u"{}. {}".format(i+1,v) for i,v in enumerate(response_options)])
                response_string = response_string.replace("{response_options}", options_list_string)
                response_options= ["Option " + str(i+1) for i,v in enumerate(response_options)]
            elif "{response_options}" in response_string:
                response_string = response_string.replace("{response_options}","")
                    
            if isinstance(response_options, list):
                if isinstance(response_options[0], (str, unicode)):
                    response_data["options"] = [[customer_state or "__init__", o] for o in response_options]
                if isinstance(response_options[0], list):
                    response_data["options"] = [[o[0], o[1]] for o in response_options]
                if isinstance(response_options[0], dict):
                    response_data["options"] = [[o.keys()[0], o.values()[0]] for o in response_options]
            elif isinstance(response_options, dict):
                response_data["options"] = response_options.items()
            else:
                response_data["options"] = []
                
        #For setting response_type and data other than response_type = options
    
        if res_data:
            check =False
            if channel_specific:
                if isinstance(channel_specific,list):
                    if channel_id in channel_specific:
                        check = True
                elif isinstance(channel_specific,(str, unicode)):
                    if channel_id == channel_specific:
                        check = True              
            elif channel_id in ["facebook","instagram"]:
                check = True
    
            if check:
                response_data= {}
                response_data = res_data
        #---end---

       
        # type_options - logic
        if res_opt and response_data.get("response_type","") == "options":
            if not response_data.get("options"):
                response_data["options"] = res_opt
            else:
                response_data["options"].extend(res_opt)
        #---end---

    # formatting options for social 

    if not is_longtxt and channel_id in ["instagram"]:
            # response_options = kwargs.get("response_options")
        if response_options and isinstance(response_options,list):
            enumerated_options_list = enumerate(response_options)
            options_str_list = [u"{}. {}{}".format(k+1,v,split) for k,v in enumerated_options_list]
            options_str = "".join(options_str_list)
            if "{response_options}" in response_string:
                response_string = response_string.replace("{response_options}",options_str)
            else:
                response_string = u"{}{}{}".format(response_string, split, options_str)
    # formatting options for social

    elif channel_id in ["kiosk", "web3d","web-avatar"]:
        if response_options and response_whiteboard in ["options","buttons"]:
            customer_state = kwargs.get("customer_state")
            if customer_state:
                buttons=[]
                for opt in response_options:
                    if type(opt)==list:
                        buttons.append({"customer_response" : opt[0],"customer_state" : customer_state, "title" : opt[1]})
                    else:
                        buttons.append({"customer_response" : opt,"customer_state" : customer_state, "title" : opt})
                response_data["buttons"] = buttons
        elif response_whiteboard == "popup_url":
            redirect_url = kwargs.get("redirect_url")
            if redirect_url:
                response_whiteboard = "popup_url"
                response_data["redirect"] = redirect_url
                hp.logger.info("response_whiteboard {}".format(response_whiteboard))
        elif response_whiteboard == "long_text":
            data=kwargs.get("long_text_data")
            if data:
                response_whiteboard ="long_text"
                response_data["title"]=data.get("title","")
                response_data["sub_title"]=data.get("sub_title","")
                response_data["description"]=data.get("description","")
                response_data["background"]=data.get("background","")
    engagement.set(response_string,"response")
    if response_whiteboard:
        engagement.set("{}.html".format(response_whiteboard),"reponse_whiteboa rd")
    elif engagement.get("default_whiteboard"):
        engagement.set("variant_info.html","reponse_whiteboard")
        engagement.set(engagement.get("default_whiteboard"),"variant_image")
    else :
        engagement.set("","reponse_whiteboard")
    

    engagement.set("static" if kwargs.get("custom_whiteboard") else "file" , "whiteboard_template")
    engagement.set(response_data if response_data else {},"response_data")
    engagement.logger.info("template whiteboard---{}".format(engagement.get("whiteboard_template")))
    engagement.logger.info("response whiteboard---{}".format(engagement.get("reponse_whiteboard")))
    self.after_structure_and_set_response_from_models(engagement, *args, **kwargs)

    return system_response_name

###################################################
def set_nudge_filters(self,engagement,nudge_type):
    ##overridable
    #overide to send specific filters for nudges. if you want specific tags to match for specific pages or cases
    '''if engagement.get("intent") == "test_drive":
        return {"tag":"test_drive"}
    return {"tag" : "SR"}  if engagement.get("temp_data").get("product") == "SR" else {}'''
    return {}

def get_max_nudges(self,engagement):
    ##overridable
    #override this to figure out how many feature_nudges are to be called before kpi nudge is called
    return 3


def get_nudge_repeat_count(self,engagement):
    ##overridable
    #override this to return number of times you can repeat nudge
    return 2
    
def get_available_nudges(self, engagement, nudge_state,*args,**kwargs):
    #return list of parent_nudges to be added in any system response
    ##hasattr(self,before_get_)
    if engagement.get("intent") and kwargs.get("check_intent",True):
        nudge_type = "motivate_nudge"
    else:
        nudge_type = "feature_nudge"
    
    nudge_list = self.filter_nudges(engagement, nudge_type)
    
    nudge_history = engagement.get("nudge_history",{})
    hp.logger.info("nudege_history ======= {}".format(nudge_history))
    nudge_intent_list = [x.get("intent") for x in nudge_list]
    engagement.logger.info("nudge intent list: {}".format(nudge_intent_list))
    ret_nudges = []
    for nudge in nudge_intent_list:
        nudge_count = nudge_history.get(nudge,0)
        max_repeats = self.get_nudge_repeat_count(engagement)
        if nudge_count <= max_repeats:
            ret_nudges.append(nudge_state)
    return ret_nudges



def get_nudge_type(self,engagement, *args,**kwargs):
    if engagement.get("intent"):
        return "motivate_nudge"
    else:
        x=self.filter_nudges(engagement,"kpi_nudge")
        if x:
            nu_count = engagement.get("current_nudge_count",0)
            if nu_count >= self.get_max_nudges(engagement):
                engagement.save(0,"current_nudge_count")
                return "kpi_nudge"
            else:
                nu_count+=1
                engagement.save(nu_count,"current_nudge_count")
    return "feature_nudge"

def filter_nudges(self, engagement, nudge_type,**kwargs):
    resp_model = base_model.Model("conversation_response",engagement.get("enterprise_id"))
    
    resp_filter_dict = self.set_nudge_filters(engagement,nudge_type)
    
    resp_filter_dict["conversation_id"] = engagement.get("conversation_id") 
    resp_filter_dict["nudge_type"] = nudge_type
    engagement.logger.info("resp filter dict for nudges:{}".format(resp_filter_dict))
    resp_data_list = list(resp_model.filter(**resp_filter_dict))
    engagement.logger.info("Here is the resp_datat {}".format(resp_data_list))
    engagement.logger.info("resp data for nudges :{}".format(len(resp_data_list))) #TODO add tags for filters..
    return resp_data_list

def get_next_nudge(self, engagement,nudge_list, nudge_type):
    used_nudges = engagement.get("used_nudges_{}".format(nudge_type),[])

    if len(used_nudges) >= len(nudge_list):
        used_nudges = []
    
    for nudge in nudge_list:
        nudge_name = nudge.get("intent")
        engagement.logger.info("nudge name ==> {}".format(nudge_name))
        if nudge_name not in used_nudges:
            used_nudges.append(nudge_name)
            engagement.logger.info("used nudge name ==> {}".format(used_nudges))
            engagement.save(used_nudges,"used_nudges_{}".format(nudge_type)) 
            return nudge
            


def process_nudge(self, engagement, *args, **kwargs):
    # engagement.save(None,"intent")
    nudge_type = self.get_nudge_type(engagement)
    resp_data_list = self.filter_nudges(engagement, nudge_type)
    # engagement.logger.info("resp data for process nudge:{}".format(resp_data_list))
    engagement.logger.info("found next nudge_type == {}".format(nudge_type))
    
    next_nudge = self.get_next_nudge(engagement,resp_data_list, nudge_type)
    engagement.logger.info("found next nudge == {}".format(next_nudge))
    ph = next_nudge.get("placeholder")
    engagement.logger.info(u"placeholder for process nudge: {}".format(ph))
    nudge_history = engagement.get("nudge_history",{})
    nudge_history[next_nudge.get("intent")] = nudge_history.get(next_nudge.get("intent"),0) + 1
    engagement.save(nudge_history,"nudge_history")
    self.set_tag(engagement,"_tag_1",next_nudge.get("intent"))
    if nudge_type in ["kpi_nudge"]:
        print("found next intent == {}".format(next_nudge.get("intent")))
        return self.set_intent(engagement,intent=next_nudge.get("intent"),required=next_nudge.get("required_attrs",[]),recheck_required=next_nudge.get("recheck_required",[]))
    elif nudge_type == "motivate_nudge":
        engagement.set(ph,"response")
        engagement.set({},"response_data")
        engagement.set("","reponse_whiteboard")
        engagement.set("static","whiteboard_template")
        return "sr_ask_options"
    engagement.set(next_nudge.get("intent"),"intent")
    self.structure_and_set_response_from_models(engagement, response_string=ph)

###################

























###############  G:/hero/heromotocorp_custom/conversation_data/hero_moto_corp/deployment/virtual_store_hindi/conversation_functions.txt #############



#For getting product id
def get_product_id(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    split = self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    cust_state = get("customer_state","")
    printx("here is the cust state %s", cust_state )
    filter_dict = {}
    filter_dict["in_stock"] = True
    filter_dict["in_production"] = True
    temp_data =  get("temporary_data",{})
    prod_id = temp_data.get("product_id")
    if prod_id:
        self.add_variable_to_engagment(eng, "product_id", prod_id)
        return None
    printx("---info--")
    prod_uttered = get("en_product_name","") or get("customer_response","") if cust_state in ["cs_product_id"] else None
    printx("here is our uttered product ===> {}".format(prod_uttered))
    if prod_uttered:
       prod_uttered = prod_uttered.lower()
       filter_dict["aliases"] = prod_uttered
     
       prod_data = list(m["product"].filter(**filter_dict)) 
       if prod_data:   
           self.add_variable_to_engagment(eng, "product_id", prod_data[0].get("product_id",None))
           required_opt = get("required_response_options",[])
           required_opt = [i.lower() for i in required_opt]
           if cust_state in ["cs_product_id"]:
                if required_opt and prod_uttered in required_opt:  
                    save([],"required_response_options")
                    return None

  
    prod_list =  list(m["product"].filter(**filter_dict)) 
    
    prod_list =  self.get_option_list_from_filter(eng,prod_list, "product_name")
    printx("option list = %s", prod_list)
    
    save(prod_list,"required_response_options")

    response = u"Please choose the product from below- {split} {response_options} {split} Which one do you like?"
    printx("Available nudges data {}".format(self.get_available_nudges(eng,"nu_parent_nudge")))
    self.structure_and_set_response_from_models(eng, response_string = response, response_language = "hindi", response_options = prod_list, customer_state = "cs_product_id",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"


#for getting varriant id
def get_variant_id(self,eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    
    m = self.get_model(eng, mod = ["variant","product"]); split = self.split(eng)
    cust_state =  get("customer_state","")
    product_name =  m["product"].get(get("gl_product_id"),{}).get("product_name","")
    filter_dict = {}
    filter_dict["in_stock"] = True 
    filter_dict["in_production"] = True 


    variant_name_uttered = get("en_variant_type") or get("customer_response") if cust_state in ["cs_variant_id"] else None
    printx("This is the uttered variant name %s", variant_name_uttered)
    if variant_name_uttered:
        variant_name_uttered = variant_name_uttered.lower()
        filter_dict["aliases"] = variant_name_uttered
        variant_data = m["variant"].list(_as_option = True, **filter_dict)
        printx("This is the variant data %s", variant_data)
        self.add_variable_to_engagment(eng, "variant_id", variant_data[0].get("variant_id",None))
        required_opt = get("required_response_options",[])
        required_opt = [i.lower() for i in required_opt]
        if cust_state in ["cs_variant_id"]:
            if required_opt and variant_name_uttered in required_opt:  
                save([],"required_response_options")
                return None

    
    filter_dict["product_id"] = get("gl_product_id")

    variant_data = m["variant"].list(_as_option = True, **filter_dict)

    options_list = self.get_option_list_from_filter(eng,variant_data,"variant_name")

    save(options_list,"required_response_options")

    response = u"Here are the variant for {}".format(product_name) + " {split} {response_options} {split} which one do you like?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", response_options=options_list, customer_state= "cs_variant_id",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"

#For form
def get_cust_details(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    details = get('customer_response', {})
    form_data = hp.json.loads(details)
    save(form_data.get('name',"xyz"), 'customer_name')
    save(form_data.get('mobile_number',"1234567891"), 'customer_mobile_number')
    save(form_data.get('email',"zta@gmail.com"), 'customer_email')


#for xtreme

def get_bikes(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    customer_name = get("customer_name", "")
    p = ""
    cust = get("customer_state")

    if cust == "cs_xtreme":
        p = "Xtreme 160R"
    elif cust == "cs_pleasure":
        p = "Pleasure Plus"

    response_string = u"Hi {}, Great choice. I can help you with features and queries on {}".format(customer_name,p)
    self.structure_and_set_response_from_models(eng, response_language = "hindi", response_string = response_string)
    return "sr_ask_options"

#for variant
def get_select_variant(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["variant","product"])
    cust_state = get("customer_state","")
    prod_name = m["product"].get(get("gl_product_id"),{}).get("product_name","").split(" ")[0].lower()
    variant_name = m["variant"].get(get("gl_variant_id"),{}).get("variant_name","").lower().replace(prod_name,"").strip().replace(" ","_")
    printx("here is your variant name %s",variant_name)
    resp_intent = get("intent") + "_" + variant_name
    printx("here is the intent %s",resp_intent)
    save(resp_intent,"intent")
    save("deployment_virtual_store_hindi","conversation_id")
    self.structure_and_set_response_from_models(eng, response_language = "hindi")
    available_nudges = self.get_available_nudges(eng,"nu_parent_nudge")
    eng.set({"_follow_ups" : available_nudges},"nudges_data")
    
    return "sr_ask_options"

#for key specs
def get_key_specs(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    
    m = self.get_model(eng, mod = ["variant","product"]); split = self.split(eng)
    cust_state =  get("customer_state","")
    product =  m["product"].get(get("gl_product_id"),{})
    product_name =  product.get("product_name")

    
    key_specs = product.get("key_specs", [])
    
    key_spec_uttered = get("en_key_spec") or get("customer_response") if cust_state in ["cs_key_spec"] else None
    
    if key_spec_uttered:
        key_spec_uttered = key_spec_uttered.lower()
        self.add_variable_to_engagment(eng, "key_specs", key_spec_uttered)
        required_opt = get("required_response_options",[])
        required_opt = [i.lower() for i in required_opt]
        if cust_state in ["cs_key_spec"]:
            if required_opt and key_spec_uttered in required_opt:  
                save([],"required_response_options")
                return None
    
    save(key_specs,"required_response_options")

    response = u"Here are the available key specs {}".format(product_name) + "{split}{response_options}{split}which one do you like?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", response_options=key_specs, customer_state= "cs_key_spec",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"

def get_select_key_specs(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product_feature"])
    product_data = list(m["product_feature"].filter(**{"product_id" : get("gl_product_id"), "feature_name" : get("gl_key_specs")}))
    printx("This is the product id {}, this is the feature name {}".format(get("gl_product_id"),get("gl_key_specs")))
    printx("This is the product data {}".format(product_data))
    feature_values = []
    if product_data:
        feature_values = product_data[0].get("product_feature_value", [])
    printx("These are the feature values {}".format(feature_values))

    feature_values = list(set(feature_values))

    state_options_variants = [i.lower().strip().replace(" ", "_") for i in feature_values]
   
    opt=  ["cs_select_key_specs_" + str(i) for i in state_options_variants]
    printx("These are the opt values {}".format(opt))
    
    type_options = []

    for i in range(len(opt)):
        type_options.extend([[opt[i],feature_values[i]]])

    printx("These are the type options {}".format(type_options))
    response = u"Here are the list of key specs.{split}{response_options}{split}Select any of the above to know more"
    
    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", type_options= type_options)
    self.set_state_options(eng, state_options=opt)
    
    return "sr_ask_options"

#for colors
def get_select_color(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])

    product = m["product"].get(get("gl_product_id")) or {}

    state_options_colour = [i.lower().strip().replace(" ", "_") for i in product.get("colours")]

    printx("State options list===> {}".format(state_options_colour))

    options_list = product.get("colours",[])
    
    opt=  ["cs_select_colour_" +  str(i) for i in state_options_colour ]

    type_options = []
    
    for i in range(len(opt)):
        type_options.extend([[opt[i],options_list[i]]])

    response = u"We have these {} colors for your ride ".format(len(set(state_options_colour))) + "{type_options}which one do you like?"
        
    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", type_options=type_options)
    self.set_state_options(eng, state_options = opt)
    available_nudges = self.get_available_nudges(eng,"nu_parent_nudge")
    eng.set({"_follow_ups" : available_nudges},"nudges_data") 
    return "sr_ask_options"

#For interactions
def get_interactions(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    prod_name = m["product"].get(get("gl_product_id"),{}).get("product_name","").split(" ")[0].lower()
    product = m["product"].get(get("gl_product_id")) or {}
    state_options_interactions = [i.lower().strip().replace(" ", "_") for i in product.get("interactions")]
    options_list = product.get("interactions",[])
    
    opt=  ["cs_select_interactions_" + prod_name + "_" + str(i) for i in state_options_interactions]
    
    type_options = []
    
    for i in range(len(opt)):
        type_options.extend([[opt[i],options_list[i]]])

    response = "We have these {} interactions for your ride".format(len(state_options_interactions)) + "{split}{type_options}{split}which one do you like to see?"

    self.structure_and_set_response_from_models(eng,response_string=response, response_language = "hindi", type_options= type_options)
    self.set_state_options(eng, state_options = opt)
    
    return "sr_ask_options"

#for build my
def get_build_my_model(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    
    m = self.get_model(eng, mod = ["variant","product"]); split = self.split(eng)
    cust_state =  get("customer_state","")
    product =  m["product"].get(get("gl_product_id"),{})
    product_name =  product.get("product_name")

    build_my_model = product.get("buil_my_model", [])
    
    build_my_model_uttered = get("en_build_my_model") or get("customer_response") if cust_state in ["cs_build_my_model"] else None
    
    if build_my_model_uttered:
        build_my_model_uttered = build_my_model_uttered.lower()
        self.add_variable_to_engagment(eng,"build_my_model", build_my_model_uttered)
        required_opt = get("required_response_options",[])
        required_opt = [i.lower() for i in required_opt]
        if cust_state in ["cs_build_my_model"]:
            if required_opt and build_my_model_uttered in required_opt:  
                save([],"required_response_options")
                return None
    
    save(build_my_model,"required_response_options")

    response = u"We have got lots of accessories for your  {} ride ".format(product_name) + "{split}{response_options}{split} click on the option to know more?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", response_options=build_my_model, customer_state= "cs_build_my_model",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"

def get_build_my(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product_feature","product"])
    product =  m["product"].get(get("gl_product_id"),{})
    product_name =  product.get("product_name")

    product_data = list(m["product_feature"].filter(product_id = get("gl_product_id"), feature_name = get("gl_build_my_model")))
    feature_values = []
    if product_data:
        feature_values = product_data[0].get("product_feature_value", [])

    state_options_build = [i.lower().strip().replace(" ", "_") for i in feature_values]

    if product_name.strip().lower() == "xtreme 160r":
        opt=  ["cs_select_build_my_" + str(product_name.strip().lower().replace(" ","_")) + "_" + str(i) for i in state_options_build]
    else:
        opt = ["cs_build_my_" + "pleasure_" + str(i) for i in state_options_build]

    options_list = product.get("buil_my_model",[])

    type_options = []
    
    for i in range(len(opt)):
        type_options.extend([[opt[i],feature_values[i]]])

    response = u"We have these {} build your ride options for you".format(len(state_options_build)) + "{split}{type_options}{split}which one do you like to see?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", type_options= type_options)
    self.set_state_options(eng, state_options = opt)
    
    return "sr_ask_options"

def get_select_colour_pearl_silver_white(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    product = m["product"].get(get("gl_product_id"),{}).get("product_name","").split(" ")[0].lower()  
    printx("This is our product {}".format(product))
    intent = "select_colour_pearl_silver_white_" + product
    printx("this is the intent {}".format(intent))
    save(intent,"intent") 
    self.structure_and_set_response_from_models(eng)
    return "sr_ask_options"


def get_feature(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product","feature"])
    cust_state = get("customer_state","")
    printx("here are the cust_state ===> {}".format(cust_state))

    feature = get("gl_entity_values",{}).get("en_feature") if cust_state in ["cs_feature"] else None
    printx("Here are the features {}".format(feature)) 
    feature =  list(m['feature'].filter(aliases = feature))[0].get("name")
    product_id = get("gl_product_id").lower()
    product = m["product"].get(product_id)
    product_name = product.get("product_name").split(" ")[0]
    printx("This is the product id {}".format(product_name))

    intent = str(feature).replace(" ", "_") + "_" + str(product_name.lower())
    save(intent,"intent")

    printx("here is our output ===> {}".format(intent))

    
    self.structure_and_set_response_from_models(eng, response_language = "hindi",)
    return "sr_ask_options"

def get_price(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["variant"])
    variant_name = m["variant"].get(get("gl_variant_id"),{}).get("variant_name","")
    price = m["variant"].get(get("gl_variant_id"),{}).get("base_price","")
    printx("here is the price ===> {}".format(variant_name))
    printx("here is the price ===> {}".format(price))
    cust_state = get("customer_state","")
    printx("here is the triggered cust_state ===> {}".format(cust_state))
    printx("here is the price entity ===> {}".format(price))
    response ="{} is available for the base price of {}".format(variant_name,price)
    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", customer_state= "cs_price")
    return "sr_ask_options"

def get_service(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    cust_state = get("customer_state","")
    printx("here is the triggered cust_state ===> {}".format(cust_state))
    service = get("gl_entity_values", {}).values()[0]
    printx("here is the  gl entity ===> {}".format(get("gl_entity_values", {})))
    printx("here is the service entity ===> {}".format(service))
    product_id = get("gl_product_id").lower()
    product = m["product"].get(product_id)
    product_name = product.get("product_name").split(" ")[0] 
    printx("This is the product id {}".format(product_name))
    intent = str(service).replace(" ", "_") + "_" + str(product_name.lower())
    save(intent,"intent")
    printx("here is our output ===> {}".format(intent))
    
    self.structure_and_set_response_from_models(eng, response_language = "hindi",)
    return "sr_ask_options"

def get_max_nudges(self,engagement):
    ##overridable
    #override this to figure out how many feature_nudges are to be called before kpi nudge is called
    return 4

def customer_greeting(self,eng,*args,**kwargs):
    available_nudges = self.get_available_nudges(eng,"nu_parent_nudge")
    eng.set({"_follow_ups" : available_nudges},"nudges_data")

def set_avatar_position(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    x= "50"
    y = "50"
    z = "50"
    w= "50"

    #key of map is combination of (in_configurator,device_type,is_zoomed)
    pos_map = {
        (True, "desktop", False) : [x+"vh", y+"vw"],
        (True, "mobile", True): [z+"vh", w+""]
    }
    default_pos = ["50vh", "50vw"]

    in_configurator = "configurator"
    device_type  = "device_type"
    is_zoomed = "zoomed"
    temp_data = get("temporary_data",{})

    in_configurator = temp_data.get(in_configurator)
    device_type = temp_data.get(device_type)
    is_zoomed = temp_data.get(is_zoomed)
    key = (in_configurator,device_type,is_zoomed)
    return pos_map[key] if pos_map.get(key) else default_pos
    

def after_structure_and_set_response_from_models(self,engagement,*args,**kwargs):
    eng = engagement
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    pos = self.set_avatar_position(eng,*args,**kwargs)
    response_data= get("response_data",{})
    response_data["position"] = pos
    eng.set(response_data,"response_data")

def before_set_intent(self,eng,*args,**kwargs):
    self.get_entity_values(eng)


def set_nudge_filters(self,eng,nudge_type):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    
    intents = [i.get("to_response_function",{}).get("kwargs",{}).get("intent","")  for i in self.customer_states.values() if i.get("to_response_function",{}).get("kwargs",{}).get("recheck_required")]
   
    if nudge_type=="motivate_nudge" and eng.get("intent") in ["select_variant"]:
        return {"tags":"nu_"+ get("intent")+ "_{}".format( get("required",[])[0])}
    elif nudge_type=="feature_nudge" and not eng.get("intent"):
        return {"tags":"nu_feature"}
    return {}

  
def generic_responder(self, eng, *args, **kwargs):
    m = self.get_model(eng, mod = "conversation_response")
   
    if not m:
        return self.process_unknown(eng)
    return self.structure_and_set_response_from_models(eng, nudges = self.get_available_nudges(eng, "nu_parent_nudge"))

def click_options(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    cust_state = get("customer_state", "")
    intent = cust_state.replace("cs_", "")
    save(intent,"intent")
    printx("here is our output ===> {}".format(intent))
    self.structure_and_set_response_from_models(eng, response_language = "hindi",)
    return "sr_ask_options"

def intro(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    cust_state = get("customer_state", "")
    cust_name = get("customer_name", "")
    if cust_state == "cs_config_intro_xtreme":
        p = "एक्सट्रीम 160आर"
    elif cust_state == "cs_config_intro_pleasure":
        p = "Pleasure Plus"

    response_string = u"Hi {}, Welcome to {} configurator page. I can help you to understand more with all the key features of the {}, Please select your desired option from below".format(cust_name,p,p)
    self.structure_and_set_response_from_models(eng, response_string = response_string)
    self.set_state_options(eng,state_options=["cs_build_my","cs_feature","cs_interactions"])
    return "sr_ask_options"
 

def generic_responder(self, engagement, *args, **kwargs):
    
    m = self.get_model(engagement, mod = "conversation_response")
   
    if not m:
        return self.process_unknown(engagement)
    return self.structure_and_set_response_from_models(engagement, response_language = "hindi")

    




