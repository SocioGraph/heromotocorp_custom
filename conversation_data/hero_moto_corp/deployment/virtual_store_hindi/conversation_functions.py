
dave_imports dave_defaults


#For getting product id
def get_product_id(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    split = self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    cust_state = get("customer_state","")
    printx("here is the cust state %s", cust_state )
    filter_dict = {}
    filter_dict["in_stock"] = True
    filter_dict["in_production"] = True
    temp_data =  get("temporary_data",{})
    prod_id = temp_data.get("product_id")
    if prod_id:
        self.add_variable_to_engagment(eng, "product_id", prod_id)
        return None
    printx("---info--")
    prod_uttered = get("en_product_name","") or get("customer_response","") if cust_state in ["cs_product_id"] else None
    printx("here is our uttered product ===> {}".format(prod_uttered))
    if prod_uttered:
       prod_uttered = prod_uttered.lower()
       filter_dict["aliases"] = prod_uttered
     
       prod_data = list(m["product"].filter(**filter_dict)) 
       if prod_data:   
           self.add_variable_to_engagment(eng, "product_id", prod_data[0].get("product_id",None))
           required_opt = get("required_response_options",[])
           required_opt = [i.lower() for i in required_opt]
           if cust_state in ["cs_product_id"]:
                if required_opt and prod_uttered in required_opt:  
                    save([],"required_response_options")
                    return None

  
    prod_list =  list(m["product"].filter(**filter_dict)) 
    
    prod_list =  self.get_option_list_from_filter(eng,prod_list, "product_name")
    printx("option list = %s", prod_list)
    
    save(prod_list,"required_response_options")

    response = u"Please choose the product from below- {split} {response_options} {split} Which one do you like?"
    printx("Available nudges data {}".format(self.get_available_nudges(eng,"nu_parent_nudge")))
    self.structure_and_set_response_from_models(eng, response_string = response, response_language = "hindi", response_options = prod_list, customer_state = "cs_product_id",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"


#for getting varriant id
def get_variant_id(self,eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    
    m = self.get_model(eng, mod = ["variant","product"]); split = self.split(eng)
    cust_state =  get("customer_state","")
    product_name =  m["product"].get(get("gl_product_id"),{}).get("product_name","")
    filter_dict = {}
    filter_dict["in_stock"] = True 
    filter_dict["in_production"] = True 


    variant_name_uttered = get("en_variant_type") or get("customer_response") if cust_state in ["cs_variant_id"] else None
    printx("This is the uttered variant name %s", variant_name_uttered)
    if variant_name_uttered:
        variant_name_uttered = variant_name_uttered.lower()
        filter_dict["aliases"] = variant_name_uttered
        variant_data = m["variant"].list(_as_option = True, **filter_dict)
        printx("This is the variant data %s", variant_data)
        self.add_variable_to_engagment(eng, "variant_id", variant_data[0].get("variant_id",None))
        required_opt = get("required_response_options",[])
        required_opt = [i.lower() for i in required_opt]
        if cust_state in ["cs_variant_id"]:
            if required_opt and variant_name_uttered in required_opt:  
                save([],"required_response_options")
                return None

    
    filter_dict["product_id"] = get("gl_product_id")

    variant_data = m["variant"].list(_as_option = True, **filter_dict)

    options_list = self.get_option_list_from_filter(eng,variant_data,"variant_name")

    save(options_list,"required_response_options")

    response = u"Here are the variant for {}".format(product_name) + " {split} {response_options} {split} which one do you like?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", response_options=options_list, customer_state= "cs_variant_id",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"

#For form
def get_cust_details(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    details = get('customer_response', {})
    form_data = hp.json.loads(details)
    save(form_data.get('name',"xyz"), 'customer_name')
    save(form_data.get('mobile_number',"1234567891"), 'customer_mobile_number')
    save(form_data.get('email',"zta@gmail.com"), 'customer_email')


#for xtreme

def get_bikes(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    customer_name = get("customer_name", "")
    p = ""
    cust = get("customer_state")

    if cust == "cs_xtreme":
        p = "Xtreme 160R"
    elif cust == "cs_pleasure":
        p = "Pleasure Plus"

    response_string = u"Hi {}, Great choice. I can help you with features and queries on {}".format(customer_name,p)
    self.structure_and_set_response_from_models(eng, response_language = "hindi", response_string = response_string)
    return "sr_ask_options"

#for variant
def get_select_variant(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["variant","product"])
    cust_state = get("customer_state","")
    prod_name = m["product"].get(get("gl_product_id"),{}).get("product_name","").split(" ")[0].lower()
    variant_name = m["variant"].get(get("gl_variant_id"),{}).get("variant_name","").lower().replace(prod_name,"").strip().replace(" ","_")
    printx("here is your variant name %s",variant_name)
    resp_intent = get("intent") + "_" + variant_name
    printx("here is the intent %s",resp_intent)
    save(resp_intent,"intent")
    save("deployment_virtual_store_hindi","conversation_id")
    self.structure_and_set_response_from_models(eng, response_language = "hindi")
    available_nudges = self.get_available_nudges(eng,"nu_parent_nudge")
    eng.set({"_follow_ups" : available_nudges},"nudges_data")
    
    return "sr_ask_options"

#for key specs
def get_key_specs(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    
    m = self.get_model(eng, mod = ["variant","product"]); split = self.split(eng)
    cust_state =  get("customer_state","")
    product =  m["product"].get(get("gl_product_id"),{})
    product_name =  product.get("product_name")

    
    key_specs = product.get("key_specs", [])
    
    key_spec_uttered = get("en_key_spec") or get("customer_response") if cust_state in ["cs_key_spec"] else None
    
    if key_spec_uttered:
        key_spec_uttered = key_spec_uttered.lower()
        self.add_variable_to_engagment(eng, "key_specs", key_spec_uttered)
        required_opt = get("required_response_options",[])
        required_opt = [i.lower() for i in required_opt]
        if cust_state in ["cs_key_spec"]:
            if required_opt and key_spec_uttered in required_opt:  
                save([],"required_response_options")
                return None
    
    save(key_specs,"required_response_options")

    response = u"Here are the available key specs {}".format(product_name) + "{split}{response_options}{split}which one do you like?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", response_options=key_specs, customer_state= "cs_key_spec",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"

def get_select_key_specs(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product_feature"])
    product_data = list(m["product_feature"].filter(**{"product_id" : get("gl_product_id"), "feature_name" : get("gl_key_specs")}))
    printx("This is the product id {}, this is the feature name {}".format(get("gl_product_id"),get("gl_key_specs")))
    printx("This is the product data {}".format(product_data))
    feature_values = []
    if product_data:
        feature_values = product_data[0].get("product_feature_value", [])
    printx("These are the feature values {}".format(feature_values))

    feature_values = list(set(feature_values))

    state_options_variants = [i.lower().strip().replace(" ", "_") for i in feature_values]
   
    opt=  ["cs_select_key_specs_" + str(i) for i in state_options_variants]
    printx("These are the opt values {}".format(opt))
    
    type_options = []

    for i in range(len(opt)):
        type_options.extend([[opt[i],feature_values[i]]])

    printx("These are the type options {}".format(type_options))
    response = u"Here are the list of key specs.{split}{response_options}{split}Select any of the above to know more"
    
    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", type_options= type_options)
    self.set_state_options(eng, state_options=opt)
    
    return "sr_ask_options"

#for colors
def get_select_color(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])

    product = m["product"].get(get("gl_product_id")) or {}

    state_options_colour = [i.lower().strip().replace(" ", "_") for i in product.get("colours")]

    printx("State options list===> {}".format(state_options_colour))

    options_list = product.get("colours",[])
    
    opt=  ["cs_select_colour_" +  str(i) for i in state_options_colour ]

    type_options = []
    
    for i in range(len(opt)):
        type_options.extend([[opt[i],options_list[i]]])

    response = u"We have these {} colors for your ride ".format(len(set(state_options_colour))) + "{type_options}which one do you like?"
        
    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", type_options=type_options)
    self.set_state_options(eng, state_options = opt)
    available_nudges = self.get_available_nudges(eng,"nu_parent_nudge")
    eng.set({"_follow_ups" : available_nudges},"nudges_data") 
    return "sr_ask_options"

#For interactions
def get_interactions(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    prod_name = m["product"].get(get("gl_product_id"),{}).get("product_name","").split(" ")[0].lower()
    product = m["product"].get(get("gl_product_id")) or {}
    state_options_interactions = [i.lower().strip().replace(" ", "_") for i in product.get("interactions")]
    options_list = product.get("interactions",[])
    
    opt=  ["cs_select_interactions_" + prod_name + "_" + str(i) for i in state_options_interactions]
    
    type_options = []
    
    for i in range(len(opt)):
        type_options.extend([[opt[i],options_list[i]]])

    response = "We have these {} interactions for your ride".format(len(state_options_interactions)) + "{split}{type_options}{split}which one do you like to see?"

    self.structure_and_set_response_from_models(eng,response_string=response, response_language = "hindi", type_options= type_options)
    self.set_state_options(eng, state_options = opt)
    
    return "sr_ask_options"

#for build my
def get_build_my_model(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    
    m = self.get_model(eng, mod = ["variant","product"]); split = self.split(eng)
    cust_state =  get("customer_state","")
    product =  m["product"].get(get("gl_product_id"),{})
    product_name =  product.get("product_name")

    build_my_model = product.get("buil_my_model", [])
    
    build_my_model_uttered = get("en_build_my_model") or get("customer_response") if cust_state in ["cs_build_my_model"] else None
    
    if build_my_model_uttered:
        build_my_model_uttered = build_my_model_uttered.lower()
        self.add_variable_to_engagment(eng,"build_my_model", build_my_model_uttered)
        required_opt = get("required_response_options",[])
        required_opt = [i.lower() for i in required_opt]
        if cust_state in ["cs_build_my_model"]:
            if required_opt and build_my_model_uttered in required_opt:  
                save([],"required_response_options")
                return None
    
    save(build_my_model,"required_response_options")

    response = u"We have got lots of accessories for your  {} ride ".format(product_name) + "{split}{response_options}{split} click on the option to know more?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", response_options=build_my_model, customer_state= "cs_build_my_model",nudges = self.get_available_nudges(eng,"nu_parent_nudge"))
    return "sr_ask_options"

def get_build_my(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product_feature","product"])
    product =  m["product"].get(get("gl_product_id"),{})
    product_name =  product.get("product_name")

    product_data = list(m["product_feature"].filter(product_id = get("gl_product_id"), feature_name = get("gl_build_my_model")))
    feature_values = []
    if product_data:
        feature_values = product_data[0].get("product_feature_value", [])

    state_options_build = [i.lower().strip().replace(" ", "_") for i in feature_values]

    if product_name.strip().lower() == "xtreme 160r":
        opt=  ["cs_select_build_my_" + str(product_name.strip().lower().replace(" ","_")) + "_" + str(i) for i in state_options_build]
    else:
        opt = ["cs_build_my_" + "pleasure_" + str(i) for i in state_options_build]

    options_list = product.get("buil_my_model",[])

    type_options = []
    
    for i in range(len(opt)):
        type_options.extend([[opt[i],feature_values[i]]])

    response = u"We have these {} build your ride options for you".format(len(state_options_build)) + "{split}{type_options}{split}which one do you like to see?"

    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", type_options= type_options)
    self.set_state_options(eng, state_options = opt)
    
    return "sr_ask_options"

def get_select_colour_pearl_silver_white(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    product = m["product"].get(get("gl_product_id"),{}).get("product_name","").split(" ")[0].lower()  
    printx("This is our product {}".format(product))
    intent = "select_colour_pearl_silver_white_" + product
    printx("this is the intent {}".format(intent))
    save(intent,"intent") 
    self.structure_and_set_response_from_models(eng)
    return "sr_ask_options"


def get_feature(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product","feature"])
    cust_state = get("customer_state","")
    printx("here are the cust_state ===> {}".format(cust_state))

    feature = get("gl_entity_values",{}).get("en_feature") if cust_state in ["cs_feature"] else None
    printx("Here are the features {}".format(feature)) 
    feature =  list(m['feature'].filter(aliases = feature))[0].get("name")
    product_id = get("gl_product_id").lower()
    product = m["product"].get(product_id)
    product_name = product.get("product_name").split(" ")[0]
    printx("This is the product id {}".format(product_name))

    intent = str(feature).replace(" ", "_") + "_" + str(product_name.lower())
    save(intent,"intent")

    printx("here is our output ===> {}".format(intent))

    
    self.structure_and_set_response_from_models(eng, response_language = "hindi",)
    return "sr_ask_options"

def get_price(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["variant"])
    variant_name = m["variant"].get(get("gl_variant_id"),{}).get("variant_name","")
    price = m["variant"].get(get("gl_variant_id"),{}).get("base_price","")
    printx("here is the price ===> {}".format(variant_name))
    printx("here is the price ===> {}".format(price))
    cust_state = get("customer_state","")
    printx("here is the triggered cust_state ===> {}".format(cust_state))
    printx("here is the price entity ===> {}".format(price))
    response ="{} is available for the base price of {}".format(variant_name,price)
    self.structure_and_set_response_from_models(eng,response_string= response, response_language = "hindi", customer_state= "cs_price")
    return "sr_ask_options"

def get_service(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get; split=self.split(eng)
    m = self.get_model(eng, mod = ["product"])
    cust_state = get("customer_state","")
    printx("here is the triggered cust_state ===> {}".format(cust_state))
    service = get("gl_entity_values", {}).values()[0]
    printx("here is the  gl entity ===> {}".format(get("gl_entity_values", {})))
    printx("here is the service entity ===> {}".format(service))
    product_id = get("gl_product_id").lower()
    product = m["product"].get(product_id)
    product_name = product.get("product_name").split(" ")[0] 
    printx("This is the product id {}".format(product_name))
    intent = str(service).replace(" ", "_") + "_" + str(product_name.lower())
    save(intent,"intent")
    printx("here is our output ===> {}".format(intent))
    
    self.structure_and_set_response_from_models(eng, response_language = "hindi",)
    return "sr_ask_options"

def get_max_nudges(self,engagement):
    ##overridable
    #override this to figure out how many feature_nudges are to be called before kpi nudge is called
    return 4

def customer_greeting(self,eng,*args,**kwargs):
    available_nudges = self.get_available_nudges(eng,"nu_parent_nudge")
    eng.set({"_follow_ups" : available_nudges},"nudges_data")

def set_avatar_position(self,eng,*args,**kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    x= "50"
    y = "50"
    z = "50"
    w= "50"

    #key of map is combination of (in_configurator,device_type,is_zoomed)
    pos_map = {
        (True, "desktop", False) : [x+"vh", y+"vw"],
        (True, "mobile", True): [z+"vh", w+""]
    }
    default_pos = ["50vh", "50vw"]

    in_configurator = "configurator"
    device_type  = "device_type"
    is_zoomed = "zoomed"
    temp_data = get("temporary_data",{})

    in_configurator = temp_data.get(in_configurator)
    device_type = temp_data.get(device_type)
    is_zoomed = temp_data.get(is_zoomed)
    key = (in_configurator,device_type,is_zoomed)
    return pos_map[key] if pos_map.get(key) else default_pos
    

def after_structure_and_set_response_from_models(self,engagement,*args,**kwargs):
    eng = engagement
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    pos = self.set_avatar_position(eng,*args,**kwargs)
    response_data= get("response_data",{})
    response_data["position"] = pos
    eng.set(response_data,"response_data")

def before_set_intent(self,eng,*args,**kwargs):
    self.get_entity_values(eng)


def set_nudge_filters(self,eng,nudge_type):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    
    intents = [i.get("to_response_function",{}).get("kwargs",{}).get("intent","")  for i in self.customer_states.values() if i.get("to_response_function",{}).get("kwargs",{}).get("recheck_required")]
   
    if nudge_type=="motivate_nudge" and eng.get("intent") in ["select_variant"]:
        return {"tags":"nu_"+ get("intent")+ "_{}".format( get("required",[])[0])}
    elif nudge_type=="feature_nudge" and not eng.get("intent"):
        return {"tags":"nu_feature"}
    return {}

  
def generic_responder(self, eng, *args, **kwargs):
    m = self.get_model(eng, mod = "conversation_response")
   
    if not m:
        return self.process_unknown(eng)
    return self.structure_and_set_response_from_models(eng, nudges = self.get_available_nudges(eng, "nu_parent_nudge"))

def click_options(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    cust_state = get("customer_state", "")
    intent = cust_state.replace("cs_", "")
    save(intent,"intent")
    printx("here is our output ===> {}".format(intent))
    self.structure_and_set_response_from_models(eng, response_language = "hindi",)
    return "sr_ask_options"

def intro(self, eng, *args, **kwargs):
    printx  = eng.logger.info; save = eng.save; get  = eng.get
    cust_state = get("customer_state", "")
    cust_name = get("customer_name", "")
    if cust_state == "cs_config_intro_xtreme":
        p = "एक्सट्रीम 160आर"
    elif cust_state == "cs_config_intro_pleasure":
        p = "Pleasure Plus"

    response_string = u"Hi {}, Welcome to {} configurator page. I can help you to understand more with all the key features of the {}, Please select your desired option from below".format(cust_name,p,p)
    self.structure_and_set_response_from_models(eng, response_string = response_string)
    self.set_state_options(eng,state_options=["cs_build_my","cs_feature","cs_interactions"])
    return "sr_ask_options"
 

def generic_responder(self, engagement, *args, **kwargs):
    
    m = self.get_model(engagement, mod = "conversation_response")
   
    if not m:
        return self.process_unknown(engagement)
    return self.structure_and_set_response_from_models(engagement, response_language = "hindi")

    




