let adloid_dave_integrations =  [
    {
        "dave_function_parameter": "cs_select_variant_lx",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,variants_section,lx",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_variant_vx",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,variants_section,vx",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_variant_zx",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,variants_section,zx",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_pole_star_blue",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,pole_star_blue",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_matte_green",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,matte_green",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_matte_metallic_red",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,matte_metallic_red",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_matte_vernier_grey",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,matte_vernier_grey",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_midnight_black",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,midnight_black",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_pearl_silver",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,pearl_silver_white",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_sporty_red",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,sporty_red",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_low_fuel_indicator",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,state_of_the_art_technology,1._low_fuel_indicator",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_retro_headlamp",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,1._retro_headlamp",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_sporty_tail_lamp",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,2._sporty_tail_lamp",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_integrated_braking_safety",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,1._integrated_braking_system",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_tubeless_tyres",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,2._tubeless_tyres",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_side_stand_indicator",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,3._side_stand_indicator",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_alloy_wheels",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,amazing_features,1._alloy_wheels",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_interactions_pleasure_seat_interaction",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,interactions_section,seat_interaction",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_interactions_pleasure_headlamp_on",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,interactions_section,headlamp_on",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_interactions_pleasure_taillamp_on",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,interactions_section,taillamp_on",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_hero_connect",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,hero_connect",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_floor_mat",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,floor_mat",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_grip_standalone_1",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,grips,grip_standalone_1",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_grip_standalone_2",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,grips,grip_standalone_2",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_grip_standalone_3",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,grips,grip_standalone_3",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_seat_cover_1",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,seat_covers,seat_cover_1",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_seat_cover_2",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,seat_covers,seat_cover_2",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_seat_cover_3",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,seat_covers,seat_cover_3",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_side_stand",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,side_stand",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_side_grill",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,side_grill",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_build_my_pleasure_step_sari",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,step_sari",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_variant_single_disc",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,variants_section,single_disc",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_variant_double_disc",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,variants_section,double_disc",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_variant_100_million_edition",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,variants_section,100_million_edition",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_vibrant_blue",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,vibrant_blue",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_sports_red",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,sports_red",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_colour_pearl_silver_white",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,colors_section,pearl_silver_white",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_xsense_with_pfi",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,state_of_the_art_technology,1._xsens_with_pfi",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_inverted_lcd_console",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,state_of_the_art_technology,2._inverted_lcd_console",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_auto_sail_technology",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,state_of_the_art_technology,3._auto_sail_technology",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_60kmh_in_47_sec",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,state_of_the_art_technology,4._0-60km/h_in_4.7_sec",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_muscular_streetfighter_stance",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,1._muscular_streetfighter_stance",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_droid_headlamp",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,2._droid_headlamp",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_led_winkers",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,3._led_winkers",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_compact_sporty_exhaust",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,4._compact_sporty_exhaust",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_signature_led_taillamp",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,5._signature_led_taillamp",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_itregrated_pilion_grab",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,cool_design,6._integrated_pillion_grab",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_side_stand_engine_cut_off",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,1._side_stand_engine_cut_off",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_hazard_lights",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,2._hazard_lights",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_single_channel_abs",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,3._single_channel_abs",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_130_mm_wide_radial_rear_tyre",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,4._130mm_wide_radial_rear_tyre",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_broad_37mm_front_suspension_by_showa",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,5._broad_37mm_front_suspension_by_showa",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_7_step_adjustable_rear_mono_shock",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,advance_safety,6._7-step_adjustable_rear_mono_shock",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_street_tuned_160cc_engine",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,amazing_features,1._street_tuned_160cc_engine",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_key_specs_rigid_diamond_frame",
        "screen_page": "configurator page",
        "adloid_open": "features-btn,key_specs_section,amazing_features,2._rigid_diamond_frame",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_interactions_xtreme_headlamp_on",
        "screen_page": "configurator page",
        "adloid_open": "main-panel-toggle,interactions_section,headlamp_on",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_interactions_xtreme_taillamp_on",
        "screen_page": "configurator page",
        "adloid_open": "main-panel-toggle,interactions_section,taillamp_on",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_hero_connect",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,hero_connect",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_engine_guard",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,engine_guard",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_fender",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,fender",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_grip_standalone_1",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,grips,grip_standalone_1",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_grip_standalone_2",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,grips,grip_standalone_2",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_grip_standalone_3",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,grips,grip_standalone_3",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_rim_tapes",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,rim_tapes",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_seat_cover_1",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,seat_covers,seat_cover_1",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_seat_cover_2",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,seat_covers,seat_cover_2",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_seat_cover_3",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,seat_covers,seat_cover_3",
        "adloid_close": "subsection-close,section-close"
    },
    {
        "dave_function_parameter": "cs_select_build_my_xtreme_160r_side_grab_standalone",
        "screen_page": "configurator page",
        "adloid_open": "customise-btn,accessories_section,side_grab_standalone",
        "adloid_close": "section-close"
    },
    {
        "dave_function_parameter": "cs_select_hotspot_circle_xtreme",
        "screen_page": "Virtual showroom landing page",
        "adloid_open": "",
        "adloid_close": ""
    },
    {
        "dave_function_parameter": "cs_select_popup_bike_xtreme",
        "screen_page": "Virtual showroom landing page",
        "adloid_open": "",
        "adloid_close": ""
    },
    {
        "dave_function_parameter": "cs_select_hotspot_circle_pleasure",
        "screen_page": "Virtual showroom landing page",
        "adloid_open": "",
        "adloid_close": ""
    },
    {
        "dave_function_parameter": "cs_select_popup_bike_pleasure",
        "screen_page": "Virtual showroom landing page",
        "adloid_open": "",
        "adloid_close": ""
    },
    {
        "dave_function_parameter": "cs_select_popup_configurator_button",
        "screen_page": "Virtual showroom landing page",
        "adloid_open": "more_info_button",
        "adloid_close": "close-btn"
    },
    {
        "dave_function_parameter": "cs_get_started",
        "screen_page": "Virtual showroom landing page",
        "adloid_open": "22",
        "adloid_close": ""
    },
    {
        "dave_function_parameter": "cs_hero_logo",
        "screen_page": "configurator page",
        "adloid_open": "hero-logo-div",
        "adloid_close": ""
    }
]
let dave_adloid_map = {}
for (let obj of adloid_dave_integrations) {
    dave_adloid_map[obj['dave_function_parameter']] = {
        'open':  obj['adloid_open'].split(',').map(function(i){return i.trim()}),
        'close': obj['adloid_close'].split(',').map(function(i){return i.trim()}),
    }
}
DAVE_SETTINGS.BASE_URL = "https://staging.iamdave.ai";
DAVE_SETTINGS.INTERACTION_USER_ID = "user_id";
DAVE_SETTINGS.CONVERSATION_ID="deployment_virtual_store_english";
DAVE_SETTINGS.SESSION_MODEL="session";
DAVE_SETTINGS.ENTERPRISE_ID="hero_moto_corp";
DAVE_SETTINGS.SIGNUP_API_KEY="635c733e-c56b-3392-a442-b9f2c51508aa";
if ( DAVE_SETTINGS.makeSingle(DAVE_SETTINGS.get_url_params()['id'],'').indexOf('xtreme') >= 0 ) {
    DAVE_SETTINGS.AVATAR_ID="hiro_xtreme-english-male";
} else {
    DAVE_SETTINGS.AVATAR_ID="hiro_pleasure-english-male";
}
DAVE_SETTINGS.SPEECH_SERVER="speech.iamdave.ai";
DAVE_SETTINGS.nowRecording = false;
DAVE_SETTINGS.register_custom_callback('on_load_libraries', function() {
    djQ('#dave-settings').append(`
        <div class='dave-cb-avatar-contt'>
            <canvas id='dave-canvas' height="200" width="200"></canvas>
        </div>
        <div class="rectangle-230">
            <div id="dave-placeholder" class="namaskar-welcome-t">
            </div>
            <div class="group-255">
                <div class="you-can-ask">You can Ask</div>
                <div class="group-1">
                    <img
                        class="path-847"
                        src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/path-847@1x.png"
                        />
                    <img
                        class="path-84"
                        src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/path-848@1x.png"
                        />
                    <img
                        class="path-84"
                        src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/path-848@1x.png"
                        />
                </div>
            </div>
            <div class="group-256">
                <div class="component-5-22">
                    <div class="language">English</div>
                    <img
                        class="path-854"
                        src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/path-854@1x.png"
                        />
                </div>
            </div>
            <div id="dave-quick-access" class="scroll-group-20">
            </div>
            <div class="group-161">
                <button
                    class="mic_black_24dp"
                    src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/mic-black-24dp@1x.png"
                    />
                <button
                    class="mic_recording_24dp"
                    src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/mic-black-24dp@1x.png"
                    />
            </div>
            <img
                class="path-882"
                src="https://anima-uploads.s3.amazonaws.com/projects/6257a4dfb9240570aa0f059c/releases/6257a4f480787dafebf8692d/img/path-882@1x.png"
                />
            <div class="volume_up_black_24dp"></div>
            <div class="volume_down_black_24dp"></div>
        </div>
    `);
    djQ( ".path-882" ).on("click", function(){window.minimize_chatbot()});
    djQ( ".volume_up_black_24dp" ).on("click", function(){
        DAVE_SCENE.mute()
        djQ(".volume_down_black_24dp").show();
        djQ(".volume_up_black_24dp").hide();
    });
    djQ( ".volume_down_black_24dp" ).on("click", function(){
        DAVE_SCENE.unmute()
        djQ(".volume_down_black_24dp").hide();
        djQ(".volume_up_black_24dp").show();
    });
    DAVE_SETTINGS.load_dave(function() {
        window.botchat_data({'query_type': 'auto'}, true, function() {
            DAVE_SCENE.opened = true;
            DAVE_SCENE.volume = 1.0;
        })
        djQ( document ).ready(function() {
            if (window.location.pathname.endsWith('web.html') ||window.location.pathname.endsWith('web_local.html') ) {
                // Remove this for production
                djQ( "#start-button" ).show();
                for ( let obj of adloid_dave_integrations ) {
                    let k = obj['adloid_open'].split(',').slice(-1)[0];
                    console.log("Adding id: " + k.trim());
                    if ( k ) {
                        if ( djQ( "#" + DAVE_SETTINGS.toJqId(k.trim()) ).length <= 0 ) {
                            djQ( "body" ).append(`
                                <button id="${k.trim()}">
                                ${obj['dave_function_parameter']}
                                </button></br>
                            `);
                        }
                    }
                }
                // Remove this for production
                djQ( "#start-button" ).on('click', function() {
                    window.botchat_data({'query_type': 'click'})
                });
            } else {
                if ( window.location.pathname.endsWith('index.htm') || window.location.pathname.endsWith('.com') ) {
                    djQ( document ).one('click', function() {
                        window.botchat_data({'query_type': 'auto', 'customer_state': 'cs_get_started', 'customer_response': 'Get Started'})
                    });
                }
                if ( window.location.pathname.endsWith('index2.html') ) {
                    let up = DAVE_SETTINGS.get_url_params();
                    if ( DAVE_SETTINGS.makeSingle(up['id'], '').indexOf( 'pleasure' ) >= 0 ) {
                        djQ( document ).one('click', function() {
                            window.botchat_data({'query_type': 'auto', 'customer_state': 'cs_config_intro_pleasure', 'customer_response': 'Tell me about Pleasure Plus'})
                        });
                    }
                    if ( DAVE_SETTINGS.makeSingle(up['id'], '').indexOf( 'xtreme' ) >= 0 ) {
                        djQ( document ).one('click', function() {
                            window.botchat_data({'query_type': 'auto', 'customer_state': 'cs_config_intro_xtreme', 'customer_response': 'Tell me about Xtreme 160 R'})
                        });
                    }
                }
            }
            for ( let obj of adloid_dave_integrations ) {
                let k = obj['adloid_open'].split(',').slice(-1)[0]
                if ( k ) {
                    djQ( "#" + DAVE_SETTINGS.toJqId(k.trim()) ).on('click', function() {
                        window.botchat_data({'query_type': 'click', 'customer_state': obj['dave_function_parameter'], 'customer_response': 'Website click'});
                    });
                }
            }
        });
    });
});
window.botchat_data = function botchat_data(params, no_open, callBack) {
    djQ('button#start-recording').prop('disabled', true);
    //pass user input to chat function
    params = params || {};
    if ( !no_open ) {
        djQ( ".dave-cb-avatar-contt" ).show();
    }
    DAVE_SETTINGS.chat(params, function (data) {
        djQ("#dave-placeholder").empty().text(data['placeholder']);
        djQ( ".rectangle-230" ).height(djQ( ".namaskar-welcome-t" ).height() + 90);
        if ( dave_adloid_map[data['customer_state']] && params['customer_response'] != 'Website click' ) {
            if ( DAVE_SETTINGS.adloid_closer ) {
                for (let k of DAVE_SETTINGS.adloid_closer) {
                    djQ( "#" + k.trim() ).click();
                }
            }
            DAVE_SETTINGS.adloid_closer = dave_adloid_map[data['customer_state']]['close']
            for ( let k in dave_adloid_map[data['customer_state']]['open'] ) {
                setTimeout(function() {
                    djQ( "#" + dave_adloid_map[data['customer_state']]['open'][k] ).click();
                }, 500 * k);
            }
        }
        if (data['state_options'] && !djQ.isEmptyObject(data['state_options'])) { 
            djQ( "#dave-quick-access").empty();
            for (const [key, value] of Object.entries(data['state_options'])) {
                djQ( "#dave-quick-access").append(`
                        <div class="overlap-group" data-customer_state="${key}" data-customer_response="${value}">
                            <div class="poppins-medium-chicago-18px" data-customer_state="${key}">${value}</div>
                        </div>
                `);
            }
            djQ(".overlap-group").on("click", function() {
                window.botchat_data({'customer_state': djQ( this ).data('customer_state'), "customer_response": djQ( this ).data('customer_response'), "query_type": "click"})
            });
        }
        if (!no_open) {
            window.maximize_chatbot(true);
        }
        if ( callBack && typeof ( callBack ) == 'function' )  {
            callBack(data);
        }
    }, function (e) {
        window.minimize_chatbot(true);
        console.error("Could not get a response from chatbot");
        console.error(e);
    });
}
window.maximize_chatbot = function maximize_chatbot(avatar) {
    if (avatar) {
        djQ( ".dave-cb-avatar-contt" ).show();
    }
    djQ( ".rectangle-230" ).show();
    DAVE_SETTINGS.on_maximize();
}
window.minimize_chatbot = function minimize_chatbot(avatar) {
    if (avatar) {
        djQ( ".dave-cb-avatar-contt" ).hide();
    }
    djQ( ".rectangle-230" ).hide();
    DAVE_SETTINGS.on_minimize();
}
DAVE_SETTINGS.register_custom_callback("on_chat_start", function(x, params) {
    if (!params.temporary_data) {
        params['temporary_data'] = {}
    }
    params.temporary_data['origin'] = window.location.origin;
    let up = DAVE_SETTINGS.get_url_params();
    for ( k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"] ) {
        if ( up[k] ) {
            params[k] = up[k];
        }
    }
});
DAVE_SETTINGS.register_custom_callback("before_create_interaction", function(params) {
    params['conversation_id'] = DAVE_SETTINGS.CONVERSATION_ID;
});
DAVE_SETTINGS.register_custom_callback("before_create_session", function(params) {
    params['conversation_id'] = DAVE_SETTINGS.CONVERSATION_ID;
});
DAVE_SETTINGS.register_custom_callback('onload', function() {
    DAVE_SCENE.camera.position._z = 4;
    DAVE_SCENE.camera.position._y = 1.8;
    DAVE_SCENE.camera.viewport.y = -0.42;
}, DAVE_SCENE);

if ( DAVE_SETTINGS.SPEECH_SERVER ) {
    let voice_MsgId = undefined;
    let mic_status = undefined
    function recButton_visibility(startRecVis) {
        if (startRecVis) {
            DAVE_SETTINGS.nowRecording = true;
            djQ(".mic_black_24dp").hide();
            djQ(".mic_recording_24dp").show();
        } else {
            DAVE_SETTINGS.nowRecording = false;
            djQ(".mic_black_24dp").show();
            djQ(".mic_recording_24dp").hide();
        }
    }
    navigator.permissions.query({ name: 'microphone' }).then(function (r) {
        if (r.state === 'granted') {
            DAVE_SETTINGS.micAccess = true;
            recButton_visibility();
        } else {
            DAVE_SETTINGS.micAccess = false;
        }
    });
    djQ(document).on("click", ".mic_black_24dp", function (event) {
        djQ(".you-can-ask").empty();
        voice_MsgId = DAVE_SETTINGS.generate_random_string(6);
        if (!DAVE_SETTINGS.micAccess) {
            navigator.mediaDevices.getUserMedia({ audio: true })
                .then(function (auStream) {
                    DAVE_SETTINGS.micAccess = true;
                    console.log('Mic Access is provided')
                })
                .catch(function (e) {
                    console.log('No mic access is provided')
                });
        }
        if (DAVE_SETTINGS.micAccess) {
            if (DAVE_SCENE.loaded) {
                DAVE_SCENE.stop();
            }
            recButton_visibility(true);
            DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording();
        } else {
            recButton_visibility();
            alert('Please allow permission to access microphone');
            djQ(".mic_black_24dp").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
        }
        event.stopPropagation();
    });

    djQ( ".mic_recording_24dp" ).on("click", function () {
        DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording();
        recButton_visibility();
        djQ('.mic_black_24dp').prop('disabled', true);
    });
    DAVE_SETTINGS.register_custom_callback('on_load_streamer', function() {
        DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording = function() {
            recButton_visibility();
            djQ('.mic_black_24dp').prop('disabled', true);
        }
        DAVE_SETTINGS.StreamingSpeech.onSocketConnect = function (o) {
            if (!DAVE_SETTINGS.nowRecording) {
                recButton_visibility();
            }
            djQ('button#start-recording').prop('disabled', false);
        }
        DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable = function (data) {
            stopTimmer();
            if (!data['final_text']) {
                let voice_rawData = "";
                if (data.hasOwnProperty('rec_text')) {
                    voice_rawData = JSON.stringify(data['rec_text']);
                } else {
                    voice_rawData = JSON.stringify(data['recognized_speech']);
                }
                // let voice_rawData = JSON.stringify(data['recognized_speech']) || JSON.stringify(data['rec_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
            } else {
                dave_chatRes_loader('user');
                let voice_rawData = JSON.stringify(data['final_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                console.debug(voice_rawData + ' =====> This is final speech text');
                djQ("#dave-cb-textinput").val('');
                msgBubbles('user', 'speech', dateTiming(), voice_rawData, voice_MsgId, null, voice_MsgId);
                scrollChatarea(500);
            }
        };
        DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable = function (data) {
            if (!data['final_text']) {
                let voice_rawData = JSON.stringify(data['rec_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
                console.debug(voice_rawData + " =====> this is in-time rec text");
            }
        }
        DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable = function (data) {
            if (data.hasOwnProperty('conv_resp')) {
                data = data['conv_resp'];
            } else {
                data = data['conversation_api_response']
            }

            let v_returnSize = Object.keys(data).length;
            if (v_returnSize > 2) {
                djQ( `input[value="${voice_MsgId}"]` ).val(data['response_id'])
                voice_MsgId = data['response_id']
                response_print(data, v_returnSize, dateTiming(), "speech", voice_MsgId);
            } else {
                c_bubbleId = djQ.now();
                userTextMsgBubble("---Silence--Noise---", 'normal', dateTiming(), c_bubbleId)
                msgBubbles('bot', 'normal', dateTiming(), 'Voice Recognition Failed! Please check your microphone settings and try again');
                scrollChatarea(500);
            }
            djQ('button#start-recording').prop('disabled', false);
        }

        DAVE_SETTINGS.StreamingSpeech.onError = function (data) {
            console.error("Speech onError is generated");
            msgBubbles('bot', 'normal', dateTiming(), 'Failed to get a response! Please try with a different query');
            scrollChatarea(500);
            if ( typeof window.startTimmer == 'function' ) {
                window.startTimmer();
            }
            djQ('button#start-recording').prop('disabled', false);
        };

        DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect = function (o) {
            recButton_visibility();
            if ( typeof window.startTimmer == 'function' ) {
                window.startTimmer();
            }
            djQ('button#start-recording').prop('disabled', false);
        };
    });
}
