export BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" && pwd )"
a=${1:-1.0}
mkdir -p ${BASE_PATH}/${a}
cat ${BASE_PATH}/common.js > ${BASE_PATH}/${a}/web.js
cat ${BASE_PATH}/web_specific.js >> ${BASE_PATH}/${a}/web.js
cat ${BASE_PATH}/common.css > ${BASE_PATH}/${a}/web.css
cat ${BASE_PATH}/web_specific.css >> ${BASE_PATH}/${a}/web.css
cp ${BASE_PATH}/${a}/web.js ${BASE_PATH}/web.js
cp ${BASE_PATH}/${a}/web.css ${BASE_PATH}/web.css
