DAVE_SETTINGS.BASE_URL = "https://staging.iamdave.ai";
DAVE_SETTINGS.INTERACTION_USER_ID = "user_id";
DAVE_SETTINGS.CONVERSATION_ID="deployment_virtual_store_english";
DAVE_SETTINGS.SESSION_MODEL="session";
DAVE_SETTINGS.ENTERPRISE_ID="hero_moto_corp";
DAVE_SETTINGS.SIGNUP_API_KEY="635c733e-c56b-3392-a442-b9f2c51508aa";
DAVE_SETTINGS.AVATAR_ID="hiro_xtreme-english-male";
DAVE_SETTINGS.SPEECH_SERVER="speech.iamdave.ai";
DAVE_SETTINGS.nowRecording = false;
DAVE_SETTINGS.register_custom_callback('on_load_libraries', function() {
    djQ('#dave-settings').append(`
        <div class='dave-cb-avatar-contt'>
            <canvas id='dave-canvas' height="200" width="200"></canvas>
        </div>
    `);
    DAVE_SETTINGS.load_dave(function() {
        window.botchat_data({'query_type': 'auto'}, true, function() {
            DAVE_SCENE.opened = true;
            DAVE_SCENE.volume = 1.0;
        })
        $( ".dave-cb-avatar-cont" ).click(function() {
            window.botchat_data({'query_type': 'auto'});
        });
    });
});
window.botchat_data = function botchat_data(params, no_open, callBack) {
    djQ('button#start-recording').prop('disabled', true);
    //pass user input to chat function
    params = params || {};
    if ( !no_open ) {
        djQ( ".dave-cb-avatar-contt" ).show();
    }
    DAVE_SETTINGS.chat(params, function (data) {
        if (!no_open) {
            window.maximize_chatbot(true);
        }
        if ( callBack && typeof ( callBack ) == 'function' )  {
            callBack(data);
        }
    }, function (e) {
        window.minimize_chatbot(true);
        console.error("Could not get a response from chatbot");
        console.error(e);
    });
}
window.maximize_chatbot = function maximize_chatbot(avatar) {
    if (avatar) {
        djQ( ".dave-cb-avatar-contt" ).show();
    }
    DAVE_SETTINGS.on_maximize();
}
window.minimize_chatbot = function minimize_chatbot(avatar) {
    if (avatar) {
        djQ( ".dave-cb-avatar-contt" ).hide();
    }
    DAVE_SETTINGS.on_minimize();
}
DAVE_SETTINGS.register_custom_callback("on_chat_start", function(x, params) {
    if (!params.temporary_data) {
        params['temporary_data'] = {}
    }
    params['refresh_synth'] = true;
    params.temporary_data['origin'] = window.location.origin;
    let up = DAVE_SETTINGS.get_url_params();
    for ( k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"] ) {
        if ( up[k] ) {
            params[k] = up[k];
        }
    }
});
DAVE_SETTINGS.register_custom_callback("before_create_interaction", function(params) {
    params['conversation_id'] = DAVE_SETTINGS.CONVERSATION_ID;
});
DAVE_SETTINGS.register_custom_callback("before_create_session", function(params) {
    params['conversation_id'] = DAVE_SETTINGS.CONVERSATION_ID;
});
DAVE_SETTINGS.register_custom_callback('onload', function() {
    DAVE_SCENE.camera.position._z = 4;
    DAVE_SCENE.camera.position._y = 1.8;
    DAVE_SCENE.camera.viewport.y = -0.42;
}, DAVE_SCENE);

if ( DAVE_SETTINGS.SPEECH_SERVER ) {
    let voice_MsgId = undefined;
    let mic_status = undefined
    function recButton_visibility(startRecVis) {
        if (startRecVis) {
            DAVE_SETTINGS.nowRecording = true;
            djQ(".mic_black_24dp").hide();
            djQ(".mic_recording_24dp").show();
        } else {
            DAVE_SETTINGS.nowRecording = false;
            djQ(".mic_black_24dp").show();
            djQ(".mic_recording_24dp").hide();
        }
    }
    navigator.permissions.query({ name: 'microphone' }).then(function (r) {
        if (r.state === 'granted') {
            DAVE_SETTINGS.micAccess = true;
            recButton_visibility();
        } else {
            DAVE_SETTINGS.micAccess = false;
        }
    });
    djQ(document).on("click", ".mic_black_24dp", function (event) {
        djQ(".you-can-ask").empty();
        voice_MsgId = DAVE_SETTINGS.generate_random_string(6);
        if (!DAVE_SETTINGS.micAccess) {
            navigator.mediaDevices.getUserMedia({ audio: true })
                .then(function (auStream) {
                    DAVE_SETTINGS.micAccess = true;
                    console.log('Mic Access is provided')
                })
                .catch(function (e) {
                    console.log('No mic access is provided')
                });
        }
        if (DAVE_SETTINGS.micAccess) {
            if (DAVE_SCENE.loaded) {
                DAVE_SCENE.stop();
            }
            recButton_visibility(true);
            DAVE_SETTINGS.StreamingSpeech.onStartVoiceRecording();
        } else {
            recButton_visibility();
            alert('Please allow permission to access microphone');
            djQ(".mic_black_24dp").attr('title', 'You have dis-allowed the microphone. Please go to settings to allow microphone for this website');
        }
        event.stopPropagation();
    });

    djQ( ".mic_recording_24dp" ).on("click", function () {
        DAVE_SETTINGS.StreamingSpeech.onStopVoiceRecording();
        recButton_visibility();
        djQ('.mic_black_24dp').prop('disabled', true);
    });
    DAVE_SETTINGS.register_custom_callback('on_load_streamer', function() {
        DAVE_SETTINGS.StreamingSpeech.onAutoStopRecording = function() {
            recButton_visibility();
            djQ('.mic_black_24dp').prop('disabled', true);
        }
        DAVE_SETTINGS.StreamingSpeech.onSocketConnect = function (o) {
            if (!DAVE_SETTINGS.nowRecording) {
                recButton_visibility();
            }
            djQ('button#start-recording').prop('disabled', false);
        }
        DAVE_SETTINGS.StreamingSpeech.onTranscriptionAvailable = function (data) {
            stopTimmer();
            if (!data['final_text']) {
                let voice_rawData = "";
                if (data.hasOwnProperty('rec_text')) {
                    voice_rawData = JSON.stringify(data['rec_text']);
                } else {
                    voice_rawData = JSON.stringify(data['recognized_speech']);
                }
                // let voice_rawData = JSON.stringify(data['recognized_speech']) || JSON.stringify(data['rec_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
            } else {
                dave_chatRes_loader('user');
                let voice_rawData = JSON.stringify(data['final_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                console.debug(voice_rawData + ' =====> This is final speech text');
                djQ("#dave-cb-textinput").val('');
                msgBubbles('user', 'speech', dateTiming(), voice_rawData, voice_MsgId, null, voice_MsgId);
                scrollChatarea(500);
            }
        };
        DAVE_SETTINGS.StreamingSpeech.onStreamingResultsAvailable = function (data) {
            if (!data['final_text']) {
                let voice_rawData = JSON.stringify(data['rec_text'])
                voice_rawData = voice_rawData.slice(1, -1)
                djQ("#dave-cb-textinput").val(djQ("#dave-cb-textinput").val() + " " + voice_rawData);
                console.debug(voice_rawData + " =====> this is in-time rec text");
            }
        }
        DAVE_SETTINGS.StreamingSpeech.onConversationResponseAvailable = function (data) {
            if (data.hasOwnProperty('conv_resp')) {
                data = data['conv_resp'];
            } else {
                data = data['conversation_api_response']
            }

            let v_returnSize = Object.keys(data).length;
            if (v_returnSize > 2) {
                djQ( `input[value="${voice_MsgId}"]` ).val(data['response_id'])
                voice_MsgId = data['response_id']
                response_print(data, v_returnSize, dateTiming(), "speech", voice_MsgId);
            } else {
                c_bubbleId = djQ.now();
                userTextMsgBubble("---Silence--Noise---", 'normal', dateTiming(), c_bubbleId)
                msgBubbles('bot', 'normal', dateTiming(), 'Voice Recognition Failed! Please check your microphone settings and try again');
                scrollChatarea(500);
            }
            djQ('button#start-recording').prop('disabled', false);
        }

        DAVE_SETTINGS.StreamingSpeech.onError = function (data) {
            console.error("Speech onError is generated");
            msgBubbles('bot', 'normal', dateTiming(), 'Failed to get a response! Please try with a different query');
            scrollChatarea(500);
            if ( typeof window.startTimmer == 'function' ) {
                window.startTimmer();
            }
            djQ('button#start-recording').prop('disabled', false);
        };

        DAVE_SETTINGS.StreamingSpeech.onSocketDisConnect = function (o) {
            recButton_visibility();
            if ( typeof window.startTimmer == 'function' ) {
                window.startTimmer();
            }
            djQ('button#start-recording').prop('disabled', false);
        };
    });
}
