# Avatar Plugin for Hero Moto Corp

This is the documentation for how to add the DaveAvatar Plugin to a webpage, and how to call custom functions.

## Code to be added to the head section of each HTML page
  
```
<link  rel="stylesheet"  type="text/css"  href="//chatbot-plugin.iamdave.ai/assets/css/7.0/dave-style.min.css">
<link  rel="stylesheet"  type="text/css"  href="//chatbot-plugin.iamdave.ai/assets/css/7.0/dave-help.min.css">
```
## Code to be added to the body section of each HTML page
```
 <link rel="stylesheet" type="text/css" href="//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/1.0/web.css">
 <script type="text/javascript" src="//chatbot-plugin.iamdave.ai/assets/js/7.0/dave-chatbot-avatar-speech.js"></script>
 <script type="text/javascript" src="//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/1.0/web.js"></script>
 <div class="dave-main-container" id="dave-settings"
     data-base-url="https://staging.iamdave.ai"
     data-conversation-id="deployment_virtual_store_english"
     data-session-model="session"
     data-enterprise-id="hero_moto_corp"
     data-signup-api-key="635c733e-c56b-3392-a442-b9f2c51508aa"
     data-interaction-user-id="user_id"
     data-avatar-id="dave-english-male"
     data-avatar-position-x='calc( 100vw - 400px )'                                                                                                        
     data-avatar-position-y='calc( 100vh - 800px )'
     data-avatar-position-h='200px'
     data-dave-cbTitle='Hero Virtual Assistant' 
     data-speech-server="speech-test.iamdave.ai"
     data-dave-botIcon='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/dave.svg' 
     data-dave-userIcon='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/customer.svg' 
     data-dave-cbIcon='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/dave_button.png' 
     data-dave-cross-img='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/cross.svg'
     data-dave-min-img='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/min.png'
     data-dave-max-img='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/max.svg'
     data-dave-title-icon='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/dave_icon.png'
     data-send-img='//general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/hero_moto_corp/send.png'
     data-default-session-data='{"deployment_scenario": "virtual_store", "location_dict": "{agent_info.ip}","browser": "{agent_info.browser}","os": "{agent_info.os}","device_type": "{agent_info.device}"}'
     data-default-user-data='{"person_type": "visitor", "source": "web-chatbot"}' 
     data-default-interaction-data='{"deployment_scenario": "virtual_store"}'
     data-interaction-origin='interaction_origin'
     data-session-origin='interaction_origin'
     data-cb-height-desktop='0px' 
     data-cb-height-mobile='0px' 
     data-cb-minLockHeight = '0px'
 >
 </div>
 ```

## How to call a custom function

In order to send information about activity conducted on the page towards the chatbot, you can use the following function

```
window.botchat_data({'customer_state': <dave function parameter>, 'query_type': 'click', 'customer_response': 'Website click'}, null, 'open', 'max');
```

Please have a look at the below table to know which function parameter to send to the avatar

|Model|Action|Sub-Action|Selection|Dave function parameter|Screen Page|
|--|--|--|--|--|--|
|Xtreme 160R|Select Variant||Single Disc|cs_select_variant_single_disc|configurator page|
|Xtreme 160R|Select Variant||Double Disc|cs_select_variant_double_disc|configurator page|
|Xtreme 160R|Select Variant||100 Million Edition|cs_select_variant_100_million_edition|configurator page|
|Xtreme 160R|Select colour||pearl silver white|cs_select_colour_pearl_silver_white|configurator page|
|Xtreme 160R|Select colour||Vibrant Blue|cs_select_colour_vibrant_blue|configurator page|
|Xtreme 160R|Select colour||Sports Red|cs_select_colour_sports_red|configurator page|
|Xtreme 160R|select key specs|State of the art technology|xsens with Pfi|cs_select_key_specs_xsense_with_pfi|configurator page|
|Xtreme 160R|select key specs|State of the art technology|Inverted LCD Console|cs_select_key_specs_inverted_lcd_console|configurator page|
|Xtreme 160R|select key specs|State of the art technology|Auto Sail Technology|cs_select_key_specs_auto_sail_technology|configurator page|
|Xtreme 160R|select key specs|State of the art technology|0-60km/H in 4.7 sec|cs_select_key_specs_60kmh_in_47_sec|configurator page|
|Xtreme 160R|select key specs|Cool Design|Muscular Streetfighter Stance|cs_select_key_specs_muscular_streetfighter_stance|configurator page|
|Xtreme 160R|select key specs|Cool Design|Droid Headlamp|cs_select_key_specs_droid_headlamp|configurator page|
|Xtreme 160R|select key specs|Cool Design|LED Winkers|cs_select_key_specs_led_winkers|configurator page|
|Xtreme 160R|select key specs|Cool Design|Compact Sporty Exhaust|cs_select_key_specs_compact_sporty_exhaust|configurator page|
|Xtreme 160R|select key specs|Cool Design|Signature LED Taillamp|cs_select_key_specs_signature_led_taillamp|configurator page|
|Xtreme 160R|select key specs|Cool Design|Integrated Pilion Grab|cs_select_key_specs_itregrated_pilion_grab|configurator page|
|Xtreme 160R|select key specs|Advance Safety|Side Stand Engine Cutt Off|cs_select_key_specs_side_stand_engine_cut_off|configurator page|
|Xtreme 160R|select key specs|Advance Safety|Hazard Lights|cs_select_key_specs_hazard_lights|configurator page|
|Xtreme 160R|select key specs|Advance Safety|Single Channel ABS|cs_select_key_specs_single_channel_abs|configurator page|
|Xtreme 160R|select key specs|Advance Safety|130 mm Wide Radial Rear Tyre|cs_select_key_specs_130_mm_wide_radial_rear_tyre|configurator page|
|Xtreme 160R|select key specs|Advance Safety|Broad 37mm front Suspension by showa|cs_select_key_specs_broad_37mm_front_suspension_by_showa|configurator page|
|Xtreme 160R|select key specs|Advance Safety|7- step Adjustable Rear Mono Shock|cs_select_key_specs_7_step_adjustable_rear_mono_shock|configurator page|
|Xtreme 160R|select key specs|Amazing Features|Street Tuned 160cc Engine|cs_select_key_specs_street_tuned_160cc_engine|configurator page|
|Xtreme 160R|select key specs|Amazing Features|Rigid Diamond Frame|cs_select_key_specs_rigid_diamond_frame|configurator page|
|Xtreme 160R|Interactions||Headlamp On|cs_select_interactions_xtreme_headlamp_on|configurator page|
|Xtreme 160R|Interactions||Taillamp On|cs_select_interactions_xtreme_taillamp_on|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Hero connect|cs_select_build_my_xtreme_160r_hero_connect|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Engine Guard|cs_select_build_my_xtreme_160r_engine_guard|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Fender|cs_select_build_my_xtreme_160r_fender|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Grips|cs_select_build_my_xtreme_160r_grip_standalone_1|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Grips|cs_select_build_my_xtreme_160r_grip_standalone_2|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Grips|cs_select_build_my_xtreme_160r_grip_standalone_3|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Rim Tapes|cs_select_build_my_xtreme_160r_rim_tapes|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Seat Cover|cs_select_build_my_xtreme_160r_seat_cover_1|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Seat Cover|cs_select_build_my_xtreme_160r_seat_cover_2|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Seat Cover|cs_select_build_my_xtreme_160r_seat_cover_3|configurator page|
|Xtreme 160R|Build My Xtreme 160R|Accessories|Side Grab Standalone|cs_select_build_my_xtreme_160r_side_grab_standalone|configurator page|
|Xtreme160R|select hotspot circle xtreme |Zoom-in |select hotspot circle |cs_select_hotspot_circle_xtreme |Virtual showroom landing page|
|Xtreme160R|select popup xtreme bike |open the popup |select the bike |cs_select_popup_bike_xtreme |Virtual showroom landing page|
|virtual showroom|select popup configurator button||click the configurator button | cs_select_popup_configurator_button |Virtual showroom landing page|
|Configurator|select hero logo ||click on hero logo| cs_hero_logo |configurator page|
|Pleasure plus|Select Variant||LX|cs_select_variant_lx|configurator page|
|Pleasure plus|Select Variant||VX|cs_select_variant_vx|configurator page|
|Pleasure plus|Select Variant||ZX|cs_select_variant_zx|configurator page|
|Pleasure plus|Select Colour||Pole Star Blue|cs_select_colour_pole_star_blue|configurator page|
|Pleasure plus|Select Colour||Matte Green|cs_select_colour_matte_green|configurator page|
|Pleasure plus|Select Colour||Matte Metallic Red|cs_select_colour_matte_metallic_red|configurator page|
|Pleasure plus|Select Colour||Matte Vernier Grey|cs_select_colour_matte_vernier_grey|configurator page|
|Pleasure plus|Select Colour||Midnight Black|cs_select_colour_midnight_black|configurator page|
|Pleasure plus|Select Colour||Pearl Silver White|cs_select_colour_pearl_silver|configurator page|
|Pleasure plus|Select Colour||Sporty Red|cs_select_colour_sporty_red|configurator page|
|Pleasure plus|Select Key Specs|State Of Art Technology|Low Fuel Indicator|cs_select_key_specs_low_fuel_indicator|configurator page|
|Pleasure plus|Select Key Specs|Cool Design|Retro Headlamp|cs_select_key_specs_retro_headlamp|configurator page|
|Pleasure plus|Select Key Specs|Cool Design|Sporty Tail Lamp|cs_select_key_specs_sporty_tail_lamp|configurator page|
|Pleasure plus|Select Key Specs|Advance Safety|Integrated Braking Safety|cs_select_key_specs_integrated_braking_safety|configurator page|
|Pleasure plus|Select Key Specs|Advance Safety|Tubeless Tyres|cs_select_key_specs_tubeless_tyres|configurator page|
|Pleasure plus|Select Key Specs|Advance Safety|Side Stand Indicator|cs_select_key_specs_side_stand_indicator|configurator page|
|Pleasure plus|Select Key Specs|Amazing Features|Alloy wheels|cs_select_key_specs_alloy_wheels|configurator page|
|Pleasure plus|Interactions||Seat Interaction|cs_select_interactions_pleasure_seat_interaction|configurator page|
|Pleasure plus|Interactions||Headlamp On|cs_select_interactions_pleasure_headlamp_on|configurator page|
|Pleasure plus|Interactions||Taillamp On|cs_select_interactions_pleasure_taillamp_on|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Hero Connect|cs_build_my_pleasure_hero_connect|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Floor Mat|cs_build_my_pleasure_floor_mat|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Handle Grips|cs_build_my_pleasure_grip_standalone_1|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Handle Grips|cs_build_my_pleasure_grip_standalone_2|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Handle Grips|cs_build_my_pleasure_grip_standalone_3|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Seat Covers|cs_build_my_pleasure_seat_cover_1|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Seat Covers|cs_build_my_pleasure_seat_cover_2|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Seat Covers|cs_build_my_pleasure_seat_cover_3|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Side Stand|cs_build_my_pleasure_side_stand|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Side Grill|cs_build_my_pleasure_side_grill|configurator page|
|Pleasure plus|Build My Pleasure+|Accessories|Step Sari|cs_build_my_pleasure_step_sari|configurator page|
|Pleasure plus|select hotspot circle pleasure |Zoom-in |select hotspot circle |cs_select_hotspot_circle_pleasure |Virtual showroom landing page|
|Pleasure plus|select popup pleasure bike |open the popup |select the bike  |cs_select_popup_bike_pleasure |Virtual showroom landing page|
|virtual showroom|select get started | |click on get started |cs_get_started |Virtual showroom landing page||
|virtual showroom|select guide close||click on guide close|cs_guide_close|Virtual showroom landing page|
|