DAVE_SETTINGS.INTERACTION_USER_ID = "user_id";
DAVE_SETTINGS.register_passive_nudge(60000, "cs_need_assistance", "Can you help me?", null, 'max', null, null, null, {'user_idle': true});
DAVE_SETTINGS.register_custom_callback("on_click_dislike", function() {
    botchat_data({ 
        'customer_state': "cs_need_assistance", "customer_response": "This answer was un-satisfactory", "query_type": "auto",
        "temporary_data": {
            "user_dislike": true
        },
    },'open', 'max');
});
DAVE_SETTINGS.register_custom_callback("on_chat_start", function(x, params) {
    if (!params.temporary_data) {
        params['temporary_data'] = {}
    }
    params.temporary_data['origin'] = window.location.origin;
    let up = DAVE_SETTINGS.get_url_params();
    for ( k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"] ) {
        if ( up[k] ) {
            params[k] = up[k];
        }
    }
});
DAVE_SETTINGS.register_custom_callback("before_create_interaction", function(params) {
    params['conversation_id'] = DAVE_SETTINGS.CONVERSATION_ID;
});
DAVE_SETTINGS.register_custom_callback("before_create_session", function(params) {
    params['conversation_id'] = DAVE_SETTINGS.CONVERSATION_ID;
});
DAVE_SETTINGS.register_custom_callback('onload', function() {
    djQ('.dave-cb-icon').trigger('click')
}, DAVE_SCENE);
let STATE_PAGE_MAP = {}
djQ( document ).ready(function() {
    djQ( '.dave-chatbox-cont' ).on('mouseleave', function( ){ djQ( this ).css('z-index', '999') });
    djQ( '.dave-chatbox-cont' ).on('mouseenter', function( ){ djQ( this ).css('z-index', '99999') })
});
