import random 
import json
import datetime
from dateutil.parser import parse
from xmlrpc.client import boolean
import requests
import os
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, split as pathsplit, splitext, sep as dirsep, isfile
import sys
sys.path.insert(0, dirname(dirname(abspath(__file__))))
from models import model as mod, helpers as hp, validators as val, action as _act, conversation as convers, beats, i2ce_routes
from os import listdir
logger = hp.get_logger(__file__, hp.logging.DEBUG)

def check_token_count():
    url = "https://api.midjourneyapi.xyz/account/info"

    payload={}
    headers = {
    'Content-Type': 'application/json',
    'X-API-KEY': '34c6db11-5097-4607-ad2a-487e5c1cf5e1'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    print(response.text)
