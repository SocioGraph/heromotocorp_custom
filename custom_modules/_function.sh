function parse() {
    python -m json.tool $1 | grep "$2" | awk  '{print $2}' | sed "s/,//g" | sed "s/\"//g"
}

function log() {
   echo "`date +"%Y-%m-%d %H:%M:%S.%N"` $@" 1>&2
}

function update_json() {
    a=""
    for v in "$@"; do
        a="$a,$v"
    done
    python -c "
import json, sys;
def is_number(i):
    if isinstance(i, (int, float, long)):
        return True
    if not isinstance(i, (str, unicode)):
        return False
    i = i.strip()
    try:
        float(i)
    except ValueError:
        return False
    return True
try:
    d = json.load(sys.stdin);
except ValueError:
    print >> sys.stderr, 'error reading file'
    d = {};
for a in \"$a\".split(','):
    if a:
        k, v = map(lambda x: x.strip(), a.split('='));
        if v == 'true':
            v = True;
        if v == 'false':
            v = False
        if v == 'null' or v == 'None':
            v = None
        if is_number(v):
            try:
                d[k] = float(v);
                continue
            except ValueError:
                pass
        d[k] = v;
print json.dumps(d, indent = 4);
"
}

function make_objects() {
    # Path to objects json, directory to write to
    a=$1
    b=${2:-${1/s\.json/}}
    c=$3
    log "creating or checking directory $b"
    rm -rf ${b}
    mkdir -p ${b}
    python -c "
import json
with open('${a}', 'r') as fp:
    j = json.load(fp)
for i, j1 in enumerate(j):
    with open('${b}/{}.json'.format(j1.get('${c}', i)), 'w') as fp:
        json.dump(j1, fp, indent = 4)
    "
    echo `basename ${b}`
}

function init() {

    if [[ -z $1 || $1 == "local" ]]; then
        export SERVER_NAME="${SERVER_NAME:-localhost:${SERVER_PORT:-5000}}"
        export HTTP="${HTTP:-http}"
        export dev_user="${I2CE_ADMIN:-sriram@iamdave.ai}"
        export dev_password="${I2CE_PASSWORD:-D@ve321}"
    else
        export SERVER_NAME=$1
        export HTTP="https"
        unset dev_user
        unset dev_password
    fi

    if [[ -z $1 ]] || [[ $1 == "local" ]]; then
        python -c "
from models import model
from models import json_tricks as json
em = model.Model('enterprise', 'core')
e = em.get('${ENTERPRISE_ID}')
if not e:
    em.post({
        'enterprise_id': '${ENTERPRISE_ID}',
        'name': '${ENTERPRISE_NAME}',
        'email': '${ADMIN_EMAIL}',
        'phone_number': '9980838165',
        'enterprise_description': '${ENTERPRISE_DESCRIPTION}',
        'enterprise_logo': '${ENTERPRISE_LOGO}' or 'https://d3u7t62cos37nz.cloudfront.net/static/img/images/logo.png?t=1595929038',
        'deployment_scenario': 'common',
        'signup_api_key': '${SIGNUP_API_KEY}',
        'origins': '*',
        'do_algorithms': False if '${DO_ALGORITHMS}' in ['False', 'false'] else True,
        'pricing': {
            'customer_conversion': 0,
            'customer_interaction': 0,
            'customer_subscription': 0,
            'daily_subscription': 0,
            'total_email_clicked': 0,
            'total_email_failed': 0,
            'total_email_sent': 0,
            'total_email_viewed': 0,
            'total_sms_failed': 0,
            'total_sms_received': 0,
            'total_sms_sent': 0,
            'user_subscription': 0
        },
        'sms_provider': 'TextLocalSMSSender'
    })
am = model.Model('admin', 'core')
a = am.get('${ADMIN_EMAIL}')
if not a:
    am.post({
        'enterprise_id': '${ENTERPRISE_ID}',
        'user_id': '${ADMIN_EMAIL}',
        'phone_number': '9980838165',
        'admin_name': 'Admin',
        'api_key': '${ADMIN_API_KEY}',
        'password': '${ADMIN_PASSWORD}',
        'validated': True,
    })
ist = model.Model('interaction_stage', '${ENTERPRISE_ID}')
if '$2' == 'delete_all':
    ist.delete_many()
with open('custom_modules/${ENTERPRISE_ID}/interaction_stages.json', 'r') as fp:
    j = json.load(fp)
    for j1 in j:
        ist.post(j1)
" 
        if [[ $SERVER_NAME == *"localhost"* && -z `lsof -i -P -n | grep LISTEN | grep :${SERVER_PORT:-5000}` ]]; then
            nohup python dashboard.py &> log.log &
            log "Waiting for local server to start up...."
            sleep 20
        fi
    fi
    post_models $2 ${MODEL_NAMES}
    rs=$?
    if [ $rs -ne 0 ]; then
        return $rs
    fi
    for m in ${OBJECT_NAMES}; do
        post_objects $m
        rs=$?
        if [ $rs -ne 0 ]; then
            return $rs
        fi
    done
    log "Doing the conversations"
    ls conversation_data/${ENTERPRISE_ID}
    for t in `ls conversation_data/${ENTERPRISE_ID}`; do
        log "Conversation data directory $t"
        if [[ -d conversation_data/${ENTERPRISE_ID}/$t ]]; then
            pushd conversation_data/${ENTERPRISE_ID}/$t
            for c in `ls`; do
                if [[ -d $c ]]; then 
                    post_conversation $t $c $2
                    rs=$?
                    if [ $rs -ne 0 ]; then
                        return $rs
                    fi
                    post_functions $t $c
                    rs=$?
                    if [ $rs -ne 0 ]; then
                        return $rs
                    fi
                    log "Now checking if we want to execure Precaching $SERVER_NAME"
                    if [[ $SERVER_NAME == *"localhost"* || $SERVER_NAME == *"192.168"* || $SERVER_NAME == *"0.0.0.0"* ]]; then 
                        log "Ya, we can do precache"
                        if [[ -z $NO_PRECACHE ]]; then 
                            log "Ok, we don't have NO_PRECACHE tag for $t_$c"
                            post_cache $t $c
                            rs=$?
                            if [ $rs -ne 0 ]; then
                                return $rs
                            fi
                        fi
                    fi
                fi
            done
            popd
        fi
    done
}

function post_model() {
    HEADERS="-H 'Content-Type: application/json' -H 'X-I2CE-ENTERPRISE-ID: ${ENTERPRISE_ID}' -H 'X-I2CE-USER-ID: ${ADMIN_EMAIL}' -H 'X-I2CE-API-KEY: ${ADMIN_API_KEY}'"
    echo "Second arg is $2"
    if [[ $2 == "delete_all" ]]; then
        log "Executing with delete all"
        cat ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/$1.json | update_json "delete_all=true" > c.json
    elif [[ $2 == "update_all" ]]; then
        log "Executing with update all"
        cat ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/$1.json | update_json "update_all=true" > c.json
    else
        cat ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/$1.json > c.json
    fi
    e="curl -s -o out.txt -X POST -w '%{http_code}' ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/model/$1 ${HEADERS} -d @c.json"
    log "POSTING Model $1"
    log $e
    rs=`eval $e`;
    log "HTTP code is: $rs"
    log "Output is: "
    cat out.txt
    if [ $rs -ne 200 ]; then
        log "Failed----------Failed---------Failed"
        return 1
    fi
    rm -rf c.json
    rm -rf out.txt
    log "Finished POSTING Model $1"
    return 0
}

function post_models() {
    if [[ $1 == "delete_all" ]] || [[ $1 == "update_all" ]]; then
        b=$1;
        shift
    else
        b=""
    fi
    if [[ "$#" -lt 1 ]];
    then
        a=${MODEL_NAMES}
    else
        a=$*
    fi
    for k in $a; do
        post_model $k $b
        rs=$?
        if [ $rs -ne 0 ]; then
            log "Failed----------Failed---------Failed"
            return $rs
        fi
    done
}

function post_objects() {
    HEADERS="-H 'Content-Type: application/json' -H 'X-I2CE-ENTERPRISE-ID: ${ENTERPRISE_ID}' -H 'X-I2CE-USER-ID: ${ADMIN_EMAIL}' -H 'X-I2CE-API-KEY: ${ADMIN_API_KEY}'"
    if [[ $1 =~ .*".json" ]]; then
        log "Making json array to a folder"
        a=`make_objects ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/$1 ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/${2:-${1/s\.json/}} $3`
    else
        a=$1
    fi
    rs=200
    if [[ -d ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/$a ]]; then
        for f in ${BASE_PATH}/custom_modules/${ENTERPRISE_ID}/$a/*.json; do
            cat $f > c.json
            e="curl -s -o out.txt -w '%{http_code}' -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/object/$a ${HEADERS} -d \"@c.json\""
            log "POSTING Object $a from $f"
            log $e
            eval $e
            nrs=`eval $e`
            log "HTTP code is: $nrs"
            log "Output is: "
            cat out.txt
            if [ $nrs -ne 200 ] && [ $rs -eq 200 ]; then
                rs=$nrs
            fi
            rm -rf c.json
            rm -rf out.txt
        done
    fi
    log "Finished POSTING Objects $a"
    if [ $rs -ne 200 ]; then
        return 1
    fi
    return 0
}

function post_conversation_old {
    HEADERS="-H 'X-I2CE-ENTERPRISE-ID: ${ENTERPRISE_ID}' -H 'X-I2CE-USER-ID: ${ADMIN_EMAIL}' -H 'X-I2CE-API-KEY: ${ADMIN_API_KEY}'"
    de=`git config --list | grep email | awk -F'=' '{print $2}'`
    if [[ -n $de ]]; then
        email=$de
    else
        email=$ADMIN_EMAIL
    fi
    cat ${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation.json | update_json "conversation_id=$1_$2,developer_email=$email" > c.json
    log "POSTING conversation $1_$2"
    e="curl -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/object/conversation ${HEADERS} -H 'Content-Type: application/json' -d @c.json"
    eval $e
    if [[ -n $3 ]]; then 
        cat ${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation.json | update_json "conversation_id=$1_$2,retrain=true,debug_mode=true,refresh_cache=true,developer_email=$email" > c.json
    else
        cat ${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation.json | update_json "conversation_id=$1_$2,developer_email=$email" > c.json
    fi
    e="curl -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/make-conversation/dave ${HEADERS} -H 'Content-Type: application/json' -d @c.json"
    eval $e
    rm -rf c.json
    log "Finished POSTING conversation $1_$2"
}
function post_conversation {
    HEADERS="-H 'X-I2CE-ENTERPRISE-ID: ${ENTERPRISE_ID}' -H 'X-I2CE-USER-ID: ${ADMIN_EMAIL}' -H 'X-I2CE-API-KEY: ${ADMIN_API_KEY}'"
    de=`git config --list | grep email | awk -F'=' '{print $2}'`
    if [[ -n $de ]]; then
        email=$de
    else
        email=$ADMIN_EMAIL
    fi
    python ${BASE_PATH}/models/conversation_merger.py -f all -j ${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation.json | update_json "conversation_id=$1_$2,developer_email=$email" > c_temp.json
    if [ $? -ne 0 ]; then
        return 1;
    fi
    cat c_temp.json

    log "POSTING conversation $1_$2"
    e="curl -s -o out.txt -w '%{http_code}' -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/object/conversation ${HEADERS} -H 'Content-Type: application/json' -d @c_temp.json"
    rs=`eval $e`
    log "HTTP code is: $rs"
    log "Output is: "
    cat out.txt
    rm -rf c.json
    rm -rf out.txt
    if [ $rs -ne 200 ]; then
        log "Failed----------Failed---------Failed"
        return 1
    fi
    if [[ -n $3 ]]; then 
        cat c_temp.json | update_json "conversation_id=$1_$2,retrain=true,debug_mode=true,refresh_cache=true,developer_email=$email" > c.json
    else
        cat c_temp.json | update_json "conversation_id=$1_$2,developer_email=$email" > c.json
    fi
    e="curl -s -o out.txt -w '%{http_code}' -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/make-conversation/dave ${HEADERS} -H 'Content-Type: application/json' -d @c.json"
    rs=`eval $e`
    log "HTTP code is: $rs"
    log "Output is: "
    cat out.txt
    rm -rf c_temp.json
    rm out.txt
    if [ $rs -ne 200 ]; then
        log "Failed----------Failed---------Failed"
        return 1
    fi
    log "Finished POSTING conversation $1_$2"
    return 0
}
function post_functions_old {
    HEADERS="-H 'X-I2CE-ENTERPRISE-ID: ${ENTERPRISE_ID}' -H 'X-I2CE-USER-ID: ${ADMIN_EMAIL}' -H 'X-I2CE-API-KEY: ${ADMIN_API_KEY}'"
    e="curl -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/conversation-manager/$1_$2 ${HEADERS} -d 'dev_user=${dev_user:-sriram@iamdave.ai}&dev_password=${dev_password:-D@ve321}' --cookie-jar cookie.bin --silent"
    log $e
    eval $e
    if [[ -f "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.py" ]]; then 
        cp "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.py" "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.txt"
    fi
    if [[ -f "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.txt" ]]; then 
        e="curl -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/conversation-manager/$1_$2/py/upload ${HEADERS} -F 'conversation_file=@${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.txt' -b cookie.bin"
        log "POSTING conversation function $1_$2"
        log $e
        eval $e
    fi
    rm -rf cookie.bin
    log "Finished POSTING conversation function $1_$2"
}
function post_functions {
    HEADERS="-H 'X-I2CE-ENTERPRISE-ID: ${ENTERPRISE_ID}' -H 'X-I2CE-USER-ID: ${ADMIN_EMAIL}' -H 'X-I2CE-API-KEY: ${ADMIN_API_KEY}'"
    e="curl -s -o out.txt -w '%{http_code}' -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/conversation-manager/$1_$2 ${HEADERS} -d 'dev_user=${dev_user:-sriram@iamdave.ai}&dev_password=${dev_password:-D@ve321}' --cookie-jar cookie.bin --silent"
    log $e
    rs=`eval $e`
    log "HTTP code is: $rs"
    log "Output is: "
    cat out.txt
    if [ $rs -ne 200 ]; then
        log "Failed----------Failed---------Failed"
        rm -rf cookie.bin
        rm -rf out.txt
        return 1
    fi
    if [[ -f "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.py" ]]; then 
        cp "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.py" "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.txt"
    fi
    if [[ -f "${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.txt" ]]; then 
        python ${BASE_PATH}/models/conversation_functions_merger.py -f ${BASE_PATH}/conversation_data/${ENTERPRISE_ID}/$1/$2/conversation_functions.txt > conversation_functions.txt
        e="curl -s -o out.txt -w '%{http_code}' -L -X POST ${HTTP:-https}://${SERVER_NAME:-dashboard.iamdave.ai}/conversation-manager/$1_$2/py/upload ${HEADERS} -F 'conversation_file=@conversation_functions.txt' -b cookie.bin"
        log "POSTING conversation function $1_$2"
        log $e
        rs=`eval $e`
        log "HTTP code is: $rs"
        log "Output is: "
        cat out.txt
        if [ $rs -ne 200 ]; then
            log "Failed----------Failed---------Failed"
            rm -rf cookie.bin
            rm -rf out.txt
            return 1
        fi
    fi
    rm -rf cookie.bin
    rm -rf conversation_functions.txt
    rm -rf out.txt
    log "Finished POSTING conversation function $1_$2"
    return 0
}

function post_cache {
    python ${BASE_PATH}/init_conversation.py $1 $2
    return $?
}
function post_local_model {
    HTTP="http"
    SERVER_NAME="localhost:${SERVER_PORT:-5000}"
    post_model $*
    return $?
}

function post_local_models {
    HTTP="http"
    SERVER_NAME="localhost:${SERVER_PORT:-5000}"
    post_models $*
    return $?
}

function post_local_objects {
    HTTP="http"
    SERVER_NAME="localhost:${SERVER_PORT:-5000}"
    post_objects $*
    return $?
}
function post_local_conversation {
    dev_user="${I2CE_ADMIN:-sriram@iamdave.ai}"
    dev_password="${I2CE_PASSWORD:-D@ve321}"
    HTTP="http"
    SERVER_NAME="localhost:${SERVER_PORT:-5000}"
    post_conversation $*
    return $?
}
function post_local_functions {
    dev_user="${I2CE_ADMIN:-sriram@iamdave.ai}"
    dev_password="${I2CE_PASSWORD:-D@ve321}"
    HTTP="http"
    SERVER_NAME="localhost:${SERVER_PORT:-5000}"
    post_functions $*
    return $?
}

function post_test_model {
    HTTP="https"
    SERVER_NAME="test.iamdave.ai"
    post_model $*
    return $?
}

function post_test_models {
    HTTP="https"
    SERVER_NAME="test.iamdave.ai"
    post_models $*
    return $?
}
function post_test_objects {
    HTTP="https"
    SERVER_NAME="test.iamdave.ai"
    post_objects $*
    return $?
}
function post_test_conversation {
    dev_user="sriram@i2ce.in"
    dev_password="D@vei2ce"
    HTTP="https"
    SERVER_NAME="test.iamdave.ai"
    post_conversation $*
    return $?
}
function post_test_functions {
    dev_user="sriram@i2ce.in"
    dev_password="D@vei2ce"
    HTTP="https"
    SERVER_NAME="test.iamdave.ai"
    post_functions $*
    return $?
}

function post_staging_model {
    HTTP="https"
    SERVER_NAME="staging.iamdave.ai"
    post_model $*
    return $?
}
function post_staging_models {
    HTTP="https"
    SERVER_NAME="staging.iamdave.ai"
    post_models $*
    return $?
}
function post_staging_objects {
    HTTP="https"
    SERVER_NAME="staging.iamdave.ai"
    post_objects $*
    return $?
}
function post_staging_conversation {
    dev_user="sriram@i2ce.in"
    dev_password="D@vei2ce"
    HTTP="https"
    SERVER_NAME="staging.iamdave.ai"
    post_conversation $*
    return $?
}
function post_staging_functions {
    dev_user="sriram@i2ce.in"
    dev_password="D@vei2ce"
    HTTP="https"
    SERVER_NAME="staging.iamdave.ai"
    post_functions $*
    return $?
}

function post_prod_model {
    HTTP="https"
    SERVER_NAME="dashboard.iamdave.ai"
    post_model $*
    return $?
}
function post_prod_models {
    HTTP="https"
    SERVER_NAME="dashboard.iamdave.ai"
    post_models $*
    return $?
}
function post_prod_objects {
    HTTP="https"
    SERVER_NAME="dashboard.iamdave.ai"
    post_objects $*
    return $?
}
function post_prod_conversation {
    dev_user="sriram@i2ce.in"
    dev_password="D@vei2ce"
    HTTP="https"
    SERVER_NAME="dashboard.iamdave.ai"
    post_conversation $*
    return $?
}
function post_prod_functions {
    dev_user="sriram@i2ce.in"
    dev_password="D@vei2ce"
    HTTP="https"
    SERVER_NAME="dashboard.iamdave.ai"
    post_functions $*
    return $?
}

function copy_environment {
    a=$1
    b=$2
    shift 2
    python $BASE_PATH/copy_environment.py --URL1=$a --URL2=$b --API_KEY1=$ADMIN_API_KEY --USER_ID1=$ADMIN_EMAIL --ENTERPRISE_ID1=$ENTERPRISE_ID --I2CE_ADMIN=$dev_user --I2CE_PASSWORD=$dev_password --SYNC $*
}
