export BASE_PATH="$( cd "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )" && pwd )"
if [[ $SERVER_NAME == "__NULL__" ]]; then
    unset SERVER_NAME
fi
export DO_ALGORITHMS=false
export ENTERPRISE_ID="hero_moto_corp"
export ADMIN_EMAIL="${HERO_ADMIN:-ananth+heromc@i2ce.in}"
export ADMIN_API_KEY="${HERO_API_KEY:-7e4c57f0-fdf1-3d36-90cf-252aaebc3481}"
export ENTERPRISE_NAME="Hero Moto Corp"
export ENTERPRISE_DESCRIPTION="Hero Configurator"
export ADMIN_PASSWORD="${HERO_ADMIN_PASSWORD:-D@veH3ro}"
export SIGNUP_API_KEY="${HERO_SIGNUP_API_KEY:-635c733e-c56b-3392-a442-b9f2c51508aa}"
export MODEL_NAMES="city space deployment product variant colour variant_colour person session interaction conversation_response feature product_feature generated_image"
export OBJECT_NAMES="interaction_stages.json spaces.json deployments.json"

source $BASE_PATH/custom_modules/_function.sh
