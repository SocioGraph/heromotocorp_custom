import React from 'react';
import ReactDOM from 'react-dom/client';
import Dave from './Dave';
import { store } from "./app/store";
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';

const root = ReactDOM.createRoot(document.getElementById('dave'));
const persistor = persistStore(store);
root.render(
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Dave/>
      </PersistGate>
    </Provider>
);

// const root = ReactDOM.createRoot(document.getElementById('dave'));
// root.render(
//   <React.StrictMode>
//     <Dave/>
//   </React.StrictMode>
// );
