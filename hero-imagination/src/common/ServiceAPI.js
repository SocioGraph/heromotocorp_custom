// signup(data, successCallback, errorCallback){
//     var HEADERS;
//     var signupurl = this.host+"/customer-signup";
//     var datao = {
//         "validated": true
//     }
//     if(data)
//         djq.extend(datao, data)
//     var that = this;
//     if(this.settings["signup_model"]){
//         signupurl = `${signupurl}/${this.settings["signup_model"]}`
//     }
//     return djq.ajax({
//         url: signupurl,
//         method: "POST",
//         dataType: "json",
//         contentType: "json",
//         withCredentials: true,
//         headers: {
//             "Content-Type": "application/json",
//             "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
//             "X-I2CE-SIGNUP-API-KEY": this.signp_key
//         },
//         data: JSON.stringify(datao),
//         success: function(data) {
//             HEADERS = {
//                 "Content-Type": "application/json",
//                 "X-I2CE-ENTERPRISE-ID": that.enterprise_id,
//                 "X-I2CE-USER-ID": data[that.user_id_attr],
//                 "X-I2CE-API-KEY": data.api_key
//             }
//             that.headers = HEADERS;
//             Utills.setCookie(that.authentication_cookie_key, JSON.stringify(HEADERS), 24);
//             that.user_id = data[that.user_id_attr];
//             that.api_key = data.api_key;
//             that.signin_required = false;
//         },
//         error: function (r) {
//             console.log(r.responseJSON || r)
//             errorCallback(r.responseJSON || r);
//         }
//     }).done(function (data) {
//         // console.log(data)
//         HEADERS = {
//             "Content-Type": "application/json",
//             "X-I2CE-ENTERPRISE-ID": that.enterprise_id,
//             "X-I2CE-USER-ID": data[that.user_id_attr],
//             "X-I2CE-API-KEY": data.api_key
//         }
//         that.headers = HEADERS;
//         Utills.setCookie(that.authentication_cookie_key, JSON.stringify(HEADERS), 240);
//         successCallback(data);
//         that.headers = HEADERS;
//     });
// }
import $ from "jquery";
import axios from "axios";
export const daveAPI = (host,enterprise_id, settings) => {
    return {
        authentication_cookie_key : settings["authentication_cookie_key"] || "authentication",
        headers :Utills.getCookie(authentication_cookie_key) || null,
        host : host,
        user_id_attr : settings["user_id_attr"] || "user_id",
        enterprise_id : enterprise_id,
        signp_key : settings["signup_apikey"],
        api_key : settings["api_key"],
        user_id : settings["user_id"],
        settings : settings,
        postRaw : (url, data, successCallback, errorCallback) => {
            // if(this.signin_required){
            //     throw "Auth required";
            // }
            // var async = data["async"] == undefined ? true : data["async"];
            // delete data["async"];
            // var that = this;
            return axios.post(url,body,{
                params : parameter,
                headers : headers || window.HEADERS
            });
            return $.ajax({
                url: this.host + url,
                method: "POST",
                dataType: "json",
                contentType: "json",
                async: async,
                withCredentials: true,
                headers: this.headers,
                data: JSON.stringify(data),
                success: function(data) {
                    console.debug("Posted object :: ", data);
                    successCallback(data);
                },
                error: function (err) {
                    err = err.responseJSON || err;
                    console.log("Error While posting object :: ", err)
                    errorCallback(err);
                }
            })
        }
    }
}