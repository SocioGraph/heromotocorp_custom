import React, { useEffect } from 'react';
import './TabHome.css';
import { useSelector , useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setIdeaName } from '../feature/IdeaNameSlice';

export default function TabHome() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { ideaName } = useSelector( state => state.ideaName_reducer);
  const updateIdeaName = (e) => {
    dispatch(setIdeaName(e.target.value));
  }
  const navigateToImagination = () =>{
    ideaName && navigate("/imagination");
  }
   useEffect(() => {
   }, [ideaName])
   
  return (
    <div id="dave-tab-home">
      <img src={require('./../resources/images/webBg.png')} alt="" id='dave-tab-home-bg'/>
      <div id="dave-tab-home-idea-container">
        <div id="dave-idea-title">Reimagine the Future of Mobility</div>
        
        <div class="dave-form-group">
            <div id="dave-idea-text">Name Your Ride!</div>
            <div id="dave-form-container">
              <div id="dave-idea-input-container">
                <img src={require("./../resources/images/webNameBg.png")} alt="" id="dave-idea-input-bg" />
                <input type="text" id="dave-idea-input-box" value={ideaName} onChange={updateIdeaName}/>
              </div>
            </div>
            <div id="dave-idea-submit" onClick={navigateToImagination}>
              <img src={require("./../resources/images/tabHomeSubmit.png")} alt="" id="dave-idea-submit-bg" />
            </div>
        </div>
        
      </div>
      <div id="dave-hero-logo">
        <img src={require('./../resources/images/webLogo.png')} alt="" id='dave-hero-logo-img'/>
      </div>
    </div>
  )
}
