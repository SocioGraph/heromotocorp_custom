import React, { useEffect } from 'react';
import { useSelector , useDispatch } from "react-redux";
import { setIdeaName } from '../feature/IdeaNameSlice';
import { setAdjective , setVehicleType , setLocation , setSelectedField , setEngagementId , setUserId , fieldSubmit , setStatusCheck  } from '../feature/IdeaNameSlice';
import DaveService from '../common/DaveService';
import { useNavigate } from "react-router-dom";
import './Imagination.css';
import Swal from 'sweetalert2';




export default function Imagination() {
  // useEffect(() => {
  //   var host = "https://dashboard.iamdave.ai";
  //   var enterprise_id = "hero_moto_corp";
  //   var signup_key = "635c733e-c56b-3392-a442-b9f2c51508aa";
  //   var ds = new DaveService(host, enterprise_id, {"signup_key": signup_key});
  //   console.log(ds);
  //   const uniqueIdentifier = Math.floor(Math.random() * 1000000);
  //   var dynamicEmail = `hero+${uniqueIdentifier}@i2ce.in`;
  //   dispatch(setUserId(dynamicEmail));
  //   function initConversation() { 
  //     console.log(ds);
  //     ds.postRaw(
  //         "/conversation/deployment_harley_davidson/"+dynamicEmail,
  //         {},
  //         (data) => { 
  //           console.log(data);
  //             console.log("engagement_id" + data["engagement_id"])
  //             dispatch(setEngagementId(data["engagement_id"]));
  //         },
  //         (error) => { 
  //             console.log(error)
  //         }
  //     )
  //   }
  //   function successCallback(data) {
  //     console.log(data);
  //     initConversation()
  //   }
  //   function errorCallback(data) {
  //     console.log(data)
  //   }
  //   ds.signup({
  //     "user_id": dynamicEmail,
  //     "person_type": "visitor",
  //     "source": "web-avatar",
  //     "origin": window.location.origin,
  //     "validated": true,
  //     "referrer": document.referrer,
  //     "application": "reimagine"
  //   },successCallback,errorCallback)
  // }, []);
  

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { ideaName , activeFields , imaginedAdjective , imaginedVehicleType , imaginedLocation , engagement_id , user_id , headers , taskId , imageId , imageURL , imageStatus , statusCheck } = useSelector( state => state.ideaName_reducer);
  const adjective = ["Futuristic","Holographic","Nano-enhanced","Anti-gravity","Bioluminescent","Augmented","Neural","Synthwave","Cybernetic","Translucent","Levitating","Adaptive","Teleporting","Hyperdimensional","Bio-mimetic","Interstellar","Chrono-synchronized","Aetherial","Holotropic"];
  const vehicleType = ["Electric bike","Electric scooter","Hoverboard","Self-driving car","Drone delivery vehicle","High-speed train","Electric skateboard","Personal electric pod","Smart bicycle","Autonomous shuttle","Electric unicycle","Flying drone taxi","Solar-powered car","Electric wheelchair","Electric longboard","Segway-like transporter","Personal gyrocopter","Magnetic levitation train","Folding electric scooter","Electric cargo bike"];
  const location = ["Timeless India","Sky City","Neo Tokyo","Luna Base","Mars Colony","Aqua Dome","Futuristic Park","Solar Hub","Cyberspace Plaza","Time Square","Terra Station","Bio Haven","Echo Isles","Zen Garden","Micro Metropolis","Data Haven","Oasis Hub","Nano City","Star Port","Jet Stream","Echo Hub","Underwater"];
  const adjectiveFocused = () => {
    dispatch(setSelectedField("adjective"))
  };
  const vehicleTypeFocused = (e) => {
    e.target.style.backgroundColor = "blue";
    dispatch(setSelectedField("vehicleType"))
  };
  const locationFocused = () => {
    dispatch(setSelectedField("location"))
  };
  const adjectiveClicked = (e) => {
    dispatch(setAdjective(e.target.innerText))
  }
  const vehicleTypeClicked = (e) => {
    dispatch(setVehicleType(e.target.innerText))
  }
  const locationClicked = (e) => {
    dispatch(setLocation(e.target.innerText))
  }

  const submitFields = () => {
    if (ideaName && imaginedAdjective && imaginedVehicleType && imaginedLocation) {
      dispatch(fieldSubmit({
        headers: headers,
        engagement_id: engagement_id,
        user_id: user_id,
        fieldsData: {
          name: ideaName,
          adjective: imaginedAdjective,
          vehicle: imaginedVehicleType,
          location: imaginedLocation
        }
      }));
      
      
    }
    function _submit() {
      navigate("/tab");
      dispatch(setIdeaName(""));
      dispatch(setSelectedField(""));
      dispatch(setAdjective(""));
      dispatch(setVehicleType(""));
      dispatch(setLocation(""));
    }
    // ideaName &&  imaginedAdjective && imaginedVehicleType && imaginedLocation && dispatch(fieldSubmit({
    //   headers : headers,
    //   engagement_id:engagement_id,
    //   user_id : user_id,
    //   fieldsData : {
    //     name : ideaName,
    //     adjective : imaginedAdjective,
    //     vehicle : imaginedVehicleType,
    //     location : imaginedLocation
    //   }
    // }));
    // dispatch(setIdeaName(""));
    // dispatch(setSelectedField(""));
    // dispatch(setAdjective(""));
    // dispatch(setVehicleType(""));
    // dispatch(setLocation(""));
    // navigate("/tab");


    Swal.fire({
      title: "Creating your ride.",
      html: "Please wait.",
      timer: 30000,
      imageUrl: require('../resources/images/a2.gif') ,
      timerProgressBar: true,
      color: "#e30d18",
      background: "#000000", 
      confirmButtonColor: "#e30d18",
      showConfirmButton: false,
      didClose: () => { 
        _submit();
      }
    })
  };

  useEffect(() => {
    !ideaName && navigate("/tab");
  }, [ideaName])
  

  useEffect(() => {
  }, [activeFields]);

  // useEffect(() => {
  //   if(taskId) {
  //     dispatch(setStatusCheck(setInterval(() => {
  //       dispatch(passThrough({headers:headers,taskId:taskId}));
  //     }, 5000)));
  //     console.log("taskId"+taskId);
  //   }
  // }, [taskId])

  // useEffect(() => {
  //   if( imageStatus === "finished") {
  //     clearInterval(statusCheck);
  //     dispatch(setStatusCheck(""));
  //   }
  // }, [imageStatus]);

  // useEffect(() => {
  //   imageURL && dispatch(jobList({headers: headers}));
  // }, [imageURL]);

  // useEffect(() => {
  //   console.log("imageId "+imageId);
  //   imageId && dispatch(statusPatch({headers: headers, imageId: imageId, imageURL: imageURL}));
  // }, [imageId])
    
  

  return (
    <div id="dave-tab-imagination">
        <img src={require('./../resources/images/webBg.png')} alt="" id='dave-tab-home-bg'/>
        <div id="dave-imagination-container">
            <div id="dave-imagination-name-container">
                <img src={require("./../resources/images/webNameBg.png")} alt="" id="dave-idea-name-bg" />
                <span id="dave-imagination-name">{ideaName}</span>
            </div>
            <div id="dave-imagination-field-container">
              <img src={require("./../resources/images/prompt_borders.png")} alt="" id="dave-idea-fields-bg" />
              <span className="dave-imagination-text">I imagine it to be a
                <span id="dave-imagination-adjective" className='dave-imagination-field' onChange={e => dispatch(setAdjective(e.target.value))} onClick={adjectiveFocused} style={activeFields === "adjective" ? { "background": "linear-gradient(0deg, #E30D18, #E30D18),linear-gradient(0deg, #FFFFFF, #FFFFFF)" } : { "background": "linear-gradient(0deg, rgba(227, 13, 24, 0.5), rgba(227, 13, 24, 0.5))" }} >{ imaginedAdjective || '(Adjective)'}</span> <span id="dave-imagination-vehicle-type" className='dave-imagination-field' onChange={e => dispatch(setVehicleType(e.target.value))} onClick={vehicleTypeFocused} style={activeFields === "vehicleType" ? { "background": "linear-gradient(0deg, #E30D18, #E30D18),linear-gradient(0deg, #FFFFFF, #FFFFFF)" } : { "background": "linear-gradient(0deg, rgba(227, 13, 24, 0.5), rgba(227, 13, 24, 0.5))" }} >{ imaginedVehicleType || '(Vehicle Type)'}</span> in <span id="dave-imagination-location" className='dave-imagination-field' onChange={e => dispatch(setLocation(e.target.value))} onClick={locationFocused} style={activeFields === "location" ? { "background": "linear-gradient(0deg, #E30D18, #E30D18),linear-gradient(0deg, #FFFFFF, #FFFFFF)" } : { "background": "linear-gradient(0deg, rgba(227, 13, 24, 0.5), rgba(227, 13, 24, 0.5))" }} >{ imaginedLocation || '(Location/Area)'}</span>
              </span>
            </div>
            {activeFields && <div id="dave-fields-suggestion-container">
              <div id="dave-imagination-field-submit-container" onClick={submitFields}>
                <img src={require('./../resources/images/tabHomeSubmit.png')} alt="" id="dave-imagination-field-submit" />
              </div>
              <span className="dave-field-suggestion-title">Select a keyword to fill the prompt</span>
              <div id="dave-field-suggestion-list-container">
                {activeFields === "adjective"? adjective.map(item => <div className="dave-suggestion-button" onClick={adjectiveClicked}  style={imaginedAdjective === item ? {"background":"rgba(227, 13, 24, 1)","color":"rgba(255, 255, 255, 1)"} : {"background":"rgba(255, 255, 255, 1)","color":"rgba(227, 13, 24, 1)"}}>{item}</div> ): activeFields === "vehicleType" ? vehicleType.map(item => <div className="dave-suggestion-button" onClick={vehicleTypeClicked} style={imaginedVehicleType === item ? {"background":"rgba(227, 13, 24, 1)","color":"rgba(255, 255, 255, 1)"} : {"background":"rgba(255, 255, 255, 1)","color":"rgba(227, 13, 24, 1)"}}>{item}</div> ) :activeFields === "location" ? location.map(item => <div className="dave-suggestion-button" onClick={locationClicked} style={imaginedLocation === item ? {"background":"rgba(227, 13, 24, 1)","color":"rgba(255, 255, 255, 1)"} : {"background":"rgba(255, 255, 255, 1)","color":"rgba(227, 13, 24, 1)"}}>{item}</div> ):""}
              </div>
            </div>}
            {/* <div id="dave-imagination-field-submit-container" onClick={submitFields}>
              <img src={require('./../resources/images/tabHomeSubmit.png')} alt="" id="dave-imagination-field-submit" />
            </div> */}
        </div>
        <div id="dave-hero-logo">
          <img src={require('./../resources/images/webLogo.png')} alt="" id='dave-hero-logo-img'/>
        </div>
    </div>
  )
}
