import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from 'axios';
import { ConfigContext } from "../config";


export const fieldSubmit = createAsyncThunk("fieldSubmit", async (para,{rejectWithValue}) => {
    const { engagement_id , user_id , fieldsData , headers } = para;
    // const HEADERS = {
    //     'Content-Type': 'application/json',
    //     'X-I2CE-ENTERPRISE-ID': "hero_moto_corp",
    //     'X-I2CE-USER-ID' : "ananth+heromc@i2ce.in",
    //     'X-I2CE-API-KEY' : "7e4c57f0-fdf1-3d36-90cf-252aaebc3481"
    // }
    const body = {
        "customer_state" : "cs_prompt",
        "system_response" : "sr_init",
        "engagement_id" : engagement_id,
        "customer_response" :JSON.stringify(fieldsData)
    }
    try {
        const response = await axios.post(ConfigContext.base_url+"/conversation/deployment_hero_mj_event/"+user_id,body,{headers : headers});
        const result = response.data;
        console.log(result)
        return result;
    } catch (error) {
        return rejectWithValue(error.response);
    }
});

// export const jobList = createAsyncThunk("jobList", async (para,{rejectWithValue}) => {
//     const { headers } = para;
//     console.log(headers);
//     try {
//         const response = await axios.get("https://dashboard.iamdave.ai/objects/hero_mj_images",{
//             params : {
//                 _sort_by : "created",
//                 _sort_reverse : true,
//                 page_size : 1
//             },
//             headers : headers
//         });
//         const result = response.data.data[0];
//         console.log(result);
//         return result;
//     } catch (error) {
//         return rejectWithValue(error);
//     }
// }); 

// export const passThrough = createAsyncThunk("passThrough", async (para,{rejectWithValue}) => {
//     const { headers , taskId } = para;
//     const body = {
//         "_application":"goapi",
//         "task_id": taskId
//     };
//     try {
//         const response = await axios.post("https://dashboard.iamdave.ai/pass_through_api/dave",body,{headers : headers});
//         const result = response.data;
//         console.log(result);
//         return result;
        
//     } catch (error) {
//         console.log(error);
//         return rejectWithValue(error.response);
//     }
// });

// export const statusPatch = createAsyncThunk("statusPatch", async (para,{rejectWithValue}) => {
//     const { headers , imageId , imageURL } = para;
//     const body = {
//         "status" : "complete",
//         "generated_image": imageURL
//     }
//     try {
//         const response = await axios.patch("https://dashboard.iamdave.ai/object/hero_mj_images/"+imageId,body,{headers : headers});
//         const result = response.data;
//         return result;
//     } catch (error) {
//         return rejectWithValue(error);
//     }
// });

export const ideaName = createSlice({
    name : 'ideaName',
    initialState : {
        headers : {
            'Content-Type': 'application/json',
            'X-I2CE-ENTERPRISE-ID': "hero_moto_corp",
            'X-I2CE-USER-ID' : "",
            'X-I2CE-API-KEY' : ""
        },
        ideaName : "",
        activeFields : "",
        imaginedAdjective : "",
        imaginedVehicleType : "",
        imaginedLocation : "",
        engagement_id : "",
        user_id : "",
        fieldsSubmitData : "",
        fieldsSubmitLoading : "",
        fieldsSubmitError : "",
        jobListData : "",
        jobListLoading : "",
        jobListError : "",
        taskId : "",
        imageId : "",
        imageURL : "",
        imageStatus : "",
        statusCheck : "",
        passThroughData : "",
        passThroughLoading : "",
        passThroughError : "",
        statusPatchData : "",
        statusPatchLoading : "",
        statusPatchError : ""
    },
    reducers : {
        setIdeaName: (state,action) => {
            state.ideaName = action.payload;
        },
        setHeaders: (state,action) => {
            state.headers['X-I2CE-USER-ID'] = action.payload.user_id;
            state.headers['X-I2CE-API-KEY'] = action.payload.api_key;
        },
        setSelectedField: (state,action) => {
            state.activeFields = action.payload;
        },
        setAdjective: (state,action) => {
            state.imaginedAdjective = action.payload;
        },
        setVehicleType: (state,action) => {
            state.imaginedVehicleType = action.payload;
        },
        setLocation: (state,action) => {
            state.imaginedLocation = action.payload;
        },
        setEngagementId: (state,action) => {
            state.engagement_id = action.payload;
        },
        setUserId: (state,action) => {
            state.user_id = action.payload;
        },
        setTaskId: (state,action) => {
            state.taskId = action.payload;
        },
        setImageId: (state,action) => {
            state.imageId = action.payload;
        },
        setImageURL: (state,action) => {
            state.imageURL = action.payload;
        },
        setImageStatus: (state,action) => {
            state.imageStatus = action.payload;
        },
        setStatusCheck: (state,action) => {
            state.statusCheck = action.payload;
        }
    },
    extraReducers : {
        [fieldSubmit.pending] : (state) => {
            state.fieldsSubmitLoading = true;
            state.fieldsSubmitError = "";
        },
        [fieldSubmit.fulfilled] : (state, action) => {
            state.fieldsSubmitLoading = false;
            state.fieldsSubmitData = action.payload;
            state.taskId = action.payload.data.data.task_id
        },
        [fieldSubmit.rejected] : (state, action) => {
            state.fieldsSubmitLoading = false;
            state.fieldsSubmitError = action.payload;
        },
    }
})

export const { setHeaders , setTaskId , setImageId , setImageURL , setIdeaName , setSelectedField , setAdjective , setVehicleType , setLocation , setEngagementId , setUserId , setImageStatus , setStatusCheck } = ideaName.actions;
export default ideaName.reducer;