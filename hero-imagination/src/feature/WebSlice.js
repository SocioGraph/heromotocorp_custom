import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from 'axios';
import { ConfigContext } from "../config";

export const jobList = createAsyncThunk("jobList", async (para,{rejectWithValue}) => {
    const { headers } = para;
    console.log(headers);
    try {
        const response = await axios.get(ConfigContext.base_url+"/objects/hero_mj_images",{
            params : {
                _sort_by : "created",
                _sort_reverse : false,
                page_size : 1,
                status: "pending"
            },
            headers : headers
        });
        const result = response.data.data[0];
        console.log(result);
        return result;
    } catch (error) {
        return rejectWithValue(error);
    }
}); 

export const passThrough = createAsyncThunk("passThrough", async (para,{rejectWithValue}) => {
    const { headers , taskId } = para;
    const body = {
        "_application":"goapi",
        "task_id": taskId
    };
    try {
        const response = await axios.post(ConfigContext.base_url+"/pass_through_api/dave",body,{headers : headers});
        const result = response.data;
        console.log(result);
        return result;
        
    } catch (error) {
        console.log(error);
        return rejectWithValue(error.response);
    }
});

export const statusPatch = createAsyncThunk("statusPatch", async (para,{rejectWithValue}) => {
    const { headers , imageId , imageURL } = para;
    const body = {
        "status" : "complete",
        "generated_image": imageURL
    }
    console.log("status patched")
    try {
        const response = await axios.patch(ConfigContext.base_url+"/object/hero_mj_images/"+imageId,body,{headers : headers});
        const result = response.data;
        return result;
    } catch (error) {
        return rejectWithValue(error);
    }
});

export const webSlice = createSlice({
    name : 'webSlice',
    initialState : {
        intervalInstance : "",
        passThroughInterval : "",
        activeJob : "",
        taskId : "",
        imageId : "",
        imageStatus : "",
        imageURL : "",
        pendingAdjective : "",
        fulfilledAdjective : "",
        pendingVehicleType : "",
        fulfilledVehicleType : "",
        pendingLocation : "",
        fulfilledLocation : "",
        pendingName : "",
        fulfilledName : "",
        jobListData : "",
        jobListLoading : "",
        jobListError : "",
        passThroughData : "",
        passThroughLoading : "",
        passThroughError : "",
        statusPatchData : "",
        statusPatchLoading : "",
        statusPatchError : "",
        upcomingMessage : "",
        currentMessage : "",
        upcomingMessageHide : true,
        currentMessageHide : true,
        slider : true,
        sliderInterval : "",
    },
    reducers : {
        // setTaskId: (state,action) => {
        //     state.taskId = action.payload;
        // },
        // setImageId: (state,action) => {
        //     state.imageId = action.payload;
        // },
        // setImageURL: (state,action) => {
        //     state.imageURL = action.payload;
        // },
        setImageStatus: (state,action) => {
            state.imageStatus = action.payload;
        },
        // setStatusCheck: (state,action) => {
        //     state.statusCheck = action.payload;
        // },
        // setActiveJobList: (state,action) => {
        //     state.activeJobList = action.payload;
        // },
        setSlider: (state,action) => {
            state.slider = action.payload;
        },
        setIntervalInstance: (state,action) => {
            state.intervalInstance = action.payload;
        },
        setSliderInterval: (state,action) => {
            state.sliderInterval = action.payload;
        },
        setPassThroughInterval: (state,action) => {
            state.passThroughInterval = action.payload;
        }
        
    },
    extraReducers : {
        [jobList.pending] : (state) => {
            state.jobListLoading = true;
            state.jobListError = "";
        },
        [jobList.fulfilled] : (state, action) => {
            state.jobListLoading = false;
            state.jobListData = action.payload;
            if (action.payload) {
                if(action.payload.status === "pending") {
                    state.fulfilledAdjective = "";
                    state.fulfilledVehicleType = "";
                    state.fulfilledLocation = "";
                    //state.fulfilledName = "";
                    state.pendingAdjective = action.payload.tag1;
                    state.pendingVehicleType = action.payload.tag2;
                    state.pendingLocation = action.payload.tag3;
                    state.pendingName = action.payload.name;
                    state.taskId = action.payload.task_id;
                    state.imageId = action.payload.generated_image_id;
                    state.activeJob = true;
    
                    // state.upcomingMessage = action.payload.meta.task_request.prompt.split(". 4k image set in the future.")[0]
                    state.upcomingMessage = `<b>${action.payload.name}</b> imagine it to be a ${action.payload.tag1} ${action.payload.tag2} in ${action.payload.tag3}`
                    state.upcomingMessageHide = false;
                    // state.imageStatus = "";
                } else if(action.payload.status === "complete") {
                    state.pendingAdjective = "";
                    state.pendingVehicleType = "";
                    state.pendingLocation = "";
                    state.pendingName = "";
                    // state.fulfilledAdjective = action.payload.tag1;
                    // state.fulfilledVehicleType = action.payload.tag2;
                    // state.fulfilledLocation = action.payload.tag3;
                    // state.fulfilledName = action.payload.name;
                    // state.activeJob = false;
                    state.imageURL = action.payload.generated_image;
                    state.taskId = action.payload.task_id;
                }
            } else {
                state.activeJob = false;
            }
        },
        [jobList.rejected] : (state, action) => {
            state.jobListLoading = false;
            state.jobListError = action.payload;
        },
        [passThrough.pending] : (state) => {
            state.passThroughLoading = true;
            state.passThroughError = "";
        },
        [passThrough.fulfilled] : (state, action) => {
            state.passThroughLoading = false;
            state.passThroughData = action.payload;
            state.imageStatus = action.payload.status;
            if(action.payload.status ===  "processing") {
                state.activeJob = true;
                // state.upcomingMessage = action.payload.meta.task_request.prompt.split(". 4k image set in the future.")[0]
                // state.upcomingMessageHide = false;
            } else if(action.payload.status ===  "finished") {
                state.activeJob = true;
                state.currentMessage = action.payload.meta.task_request.prompt.split(". 4k image set in the future.")[0];
                state.fulfilledName = state.pendingName;
                state.upcomingMessageHide = true;
                state.slider = state.imageStatus === action.payload.status && state.slider === true ? true : false;

            }
            if(action.payload.task_result.image_url) {
                state.imageURL = action.payload.task_result.image_url;    
            }

        },
        [passThrough.rejected] : (state, action) => {
            state.passThroughLoading = false;
            state.passThroughError = action.payload;
        },
        [statusPatch.pending] : (state) => {
            state.statusPatchLoading = true;
            state.statusPatchError = "";
        },
        [statusPatch.fulfilled] : (state, action) => {
            state.statusPatchLoading = false;
            state.statusPatchData = action.payload;
        },
        [statusPatch.rejected] : (state, action) => {
            state.statusPatchLoading = false;
            state.statusPatchError = action.payload;
        }
    }
})
export const { setIntervalInstance , setSlider , setSliderInterval , setPassThroughInterval , setImageStatus } = webSlice.actions;

// export const { setTaskId , setImageId , setImageURL , setImageStatus , setStatusCheck , setActiveJobList , setCounterStatus } = webSlice.actions;
export default webSlice.reducer;