import React, { useEffect } from 'react'
import DaveService from './common/DaveService';
import { useNavigate } from "react-router-dom";
import { setHeaders , setEngagementId , setUserId  } from './feature/IdeaNameSlice';
import { useSelector , useDispatch } from "react-redux";
import './Home.css'
import {ConfigContext} from "./config";



export default function Home() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        var host = ConfigContext.base_url;
        var enterprise_id = "hero_moto_corp";
        var signup_key = "635c733e-c56b-3392-a442-b9f2c51508aa";
        var ds = new DaveService(host, enterprise_id, {"signup_key": signup_key});
        console.log(ds);
        const uniqueIdentifier = Math.floor(Math.random() * 1000000);
        var dynamicEmail = `hero+${uniqueIdentifier}@i2ce.in`;
        dispatch(setUserId(dynamicEmail));
        function initConversation() { 
          console.log(ds);
          ds.postRaw(
              "/conversation/deployment_harley_davidson/"+dynamicEmail,
              {},
              (data) => { 
                  console.log("engagement_id" + data["engagement_id"])
                  dispatch(setEngagementId(data["engagement_id"]));
              },
              (error) => { 
                  console.log(error)
              }
          )
        }
        function successCallback(data) {
          console.log(data);
          dispatch(setHeaders(data))
          initConversation()
        }
        function errorCallback(data) {
          console.log(data)
        }
        ds.signup({
          "user_id": dynamicEmail,
          "person_type": "visitor",
          "source": "web-avatar",
          "origin": window.location.origin,
          "validated": true,
          "referrer": document.referrer,
          "application": "reimagine"
        },successCallback,errorCallback)
      }, []);

    const routeTab = () =>{
        navigate("/tab");
    }

    const routeWeb = () =>{
        navigate("/web");
    }

  return (
    <div id="dave-tab-imagination">
        <img src={require('./resources/images/webBg.png')} alt="" id='dave-tab-home-bg'/>
        <div id="dave-routes">
            <div id="dave-tab-route" className='dave-route-button' onClick={routeTab}>IPAD</div>
            <div id="dave-web-route" className='dave-route-button' onClick={routeWeb}>Screen</div>
        </div>
        <div id="dave-hero-logo">
          <img src={require('./resources/images/webLogo.png')} alt="" id='dave-hero-logo-img'/>
        </div>
    </div>
  )
}
