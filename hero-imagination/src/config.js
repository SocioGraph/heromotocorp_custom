import React from 'react'

export const ConfigContext = React.createContext("global_var");


// ConfigContext.base_url = 'https://dashboard.iamdave.ai';


if (window.location.origin.includes("dxge3f9act1s9.cloudfront.net")) { 
    ConfigContext.base_url = 'https://dashboard.iamdave.ai';
} else {
    ConfigContext.base_url = 'https://test.iamdave.ai';
}
