import { configureStore , combineReducers } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";
import ideaName from '../feature/IdeaNameSlice';
import webSlice from '../feature/WebSlice';
// import login from '../feature/LoginSlice';

const persistConfig = {
  key : "root",
  version : 1,
  storage,
  whitelist: ['ideaName_reducer'],
  blacklist: ['webSlice_reducer'],

//   whitelist: ['login_reducer','user_reducer'],
//   blacklist: ['addCustomer_reducer','ticketStatus_reducer','reassignTicket_reducer','notification_reducer','chatAction_reducer','sorting_reducer','logout_reducer','searchJob_reducer','pagination_reducer']
};

const reducers = combineReducers({
//   login_reducer: login,
    ideaName_reducer: ideaName,
    webSlice_reducer: webSlice
});

const persistedReducer = persistReducer(persistConfig,reducers);
export const store = configureStore({
  reducer: persistedReducer
});