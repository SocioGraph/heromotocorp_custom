import React, { useEffect , useState } from 'react';
import { useSelector , useDispatch } from "react-redux";
import { statusPatch , jobList , passThrough , setIntervalInstance , setSlider , setSliderInterval , setPassThroughInterval , setImageStatus } from '../feature/WebSlice';
import './Screen.css';
import Swal from 'sweetalert2'


function _cache(url) {
  var z = new Image(url);
  z.onload = () => {
    console.log("Image loaded :: ", url);
   }
}

export default function Screen() {
  const dispatch = useDispatch();
  const { headers } = useSelector( state => state.ideaName_reducer);
  const { imageStatus , passThroughInterval , intervalInstance , taskId, activeJob, jobListData, imageURL, passThroughData, imageId, upcomingMessage, currentMessage, upcomingMessageHide, sliderInterval, slider, fulfilledName, jobListLoading } = useSelector(state => state.webSlice_reducer);
  // let [imageStatus, setImageStatus] = useState('processing');

  const sliderData = [
    { image:require("./../resources/images/img1.webp") },
    { image:require("./../resources/images/img2.webp") },
    { image:require("./../resources/images/img3.webp") },
    { image:require("./../resources/images/img4.webp") },
    { image: require("./../resources/images/img5.png") },
    { image: require("./../resources/images/img6.png") },
    { image: require("./../resources/images/img7.png") },
    { image:require("./../resources/images/img8.png") }
  ];

  for (let url of sliderData) {
    _cache(url.image);
  }
    
  // console.log(sliderData);

  const [current, setCurrent] = useState(0);
  const [out, setOut] = useState(false);
  var [intr, setIntr] = useState('');
  const length = sliderData.length;

  const nextSlide = () => {
    console.log("I m called");
    setOut(true);
    setTimeout(() => {
      setOut(false);
      setCurrent(current === length - 1 ? 0 : current + 1);
    }, 1000)
    
  };

  useEffect(() => {
    setIntr(true);
    dispatch(jobList({headers: headers}));
    // dispatch(setIntervalInstance(setInterval(() => {
    //   dispatch(jobList({headers: headers}));
    // }, 10000)));
    return () => {
      clearInterval(intervalInstance);
      dispatch(setIntervalInstance(""));
    }
  }, [])

  

  useEffect(() => {
    !jobListLoading && nextSlide();
  }, [jobListLoading])
  

  useEffect(() => {
    if (activeJob) {
      clearInterval(intervalInstance);
      dispatch(setIntervalInstance(""));
      !passThroughInterval && dispatch(setPassThroughInterval(setInterval(() => {
        clearInterval(intervalInstance);
        dispatch(setIntervalInstance(""));
        dispatch(passThrough({ headers: headers, taskId: taskId }))
      }, 10000)))
    } else {
      clearInterval(passThroughInterval);
      dispatch(setPassThroughInterval(""));
      !intervalInstance && dispatch(setIntervalInstance(setInterval(() => {
        !intr && dispatch(jobList({headers: headers}));
      }, 10000)));
    }
    return () => {
      clearInterval(intervalInstance);
      dispatch(setIntervalInstance(""));
      clearInterval(passThroughInterval);
      dispatch(setPassThroughInterval(""));
    }
  }, [activeJob,taskId,jobListData])

  // useEffect(() => {
  //   // imageStatus === "finished" && (statusPatch({headers: headers, imageId : imageId ,imageURL : imageURL}))

  //   console.log("imageStatus" + imageStatus);
  //   if(imageStatus === "finished") {
  //     statusPatch({headers: headers, imageId : imageId ,imageURL : imageURL});
  //     clearInterval(passThroughInterval);
  //   }
  // }, [taskId,passThroughData,imageId,imageURL,imageStatus])
  
  useEffect(() => {
  }, [currentMessage,upcomingMessage])

  useEffect(() => {
    console.log("imageStatus" + imageStatus);
    if(imageStatus === "finished") {
      dispatch(statusPatch({headers: headers, imageId : imageId ,imageURL : imageURL}));
      dispatch(setSlider(false));
      setTimeout(() => {
        dispatch(setSlider(true));
        dispatch(jobList({headers: headers}));
        dispatch(setIntervalInstance(setInterval(() => {
          dispatch(jobList({headers: headers}));
        }, 10000)));
      }, 60000);
      dispatch(setImageStatus(""));
      clearInterval(passThroughInterval);
      dispatch(setPassThroughInterval(""));

      // jobList();
      // dispatch(setIntervalInstance(setInterval(() => {
      //   dispatch(jobList({headers: headers}));
      // }, 10000)));
    }
    return () => {
      clearInterval(intervalInstance);
      dispatch(setIntervalInstance(""));
      clearInterval(passThroughInterval);
      dispatch(setPassThroughInterval(""));
    }
  }, [imageStatus])
  
  useEffect(() => {
    if (!upcomingMessageHide) {
      _showImaginatingAlert("UPCOMING : I imagine " + upcomingMessage);
    } else {
      Swal.close();
    }
  }, [upcomingMessageHide])

  function _showImaginatingAlert() {
    Swal.fire({
      // title: "Creating your ride.",
      title: "UPCOMING",
      // html: "Please wait.",
      html: `${upcomingMessage}`,
      // timer: 30000,
      imageUrl: require('../resources/images/a2.gif') ,
      timerProgressBar: true,
      color: "#e30d18",
      background: "#000000", 
      confirmButtonColor: "#e30d18",
      showConfirmButton: false
    })
  }

  // useEffect(() => {
  //   const sliderActive = setTimeout(() => {
  //     dispatch(setSlider(true));
  //   }, 30000);
  //   return () => {
  //     clearTimeout(sliderActive);
  //   }
  // }, [slider])
  
  
  return (
    <div id="dave-web">
      <img src={require("./../resources/images/webBg.png")} alt="" id="dave-web-bg" />
      <div id="dave-web-logo">
        <img src={require("./../resources/images/webLogo.png")} alt="" id="dave-web-logo-img" />
      </div>
      {/* {
        !upcomingMessageHide && <div id="dave-web-upcoming-container">
          <span id="dave-web-upcoming-message">UPCOMING : I Imagine it to be {upcomingMessage}</span>
        </div>
      } */}
      {/* <div id="dave-web-imagined-container">
        <div id="dave-web-imagined-name-container">
          <img src={require("./../resources/images/webNameBg.png")} alt="" id="dave-web-imagined-name-bg" />
          <span id="dave-web-imagined-name">{fulfilledName}</span>
        </div>
        <div id="dave-web-imagined-img-container">
          <img src={imageURL} alt="" id="dave-web-imagined-img" />
        </div>
        <div id="dave-web-imagined-text-container">
          <img src={require("./../resources/images/webImaginedTextBg.png")} alt="" id="dave-web-imagined-name-bg" />
          <span id="dave-web-imagined-text">I Imagine {currentMessage}</span>
        </div>
        <div id="dave-web-upcoming-loader-container">
          <img src={require("./../resources/images/webLoader.png")} alt="" id="dave-web-upcoming-loader" />
        </div>
      </div> */}
      { slider ? sliderData.map((slide, index) => {
        return (
          <div
            className={index === current ? 'slide active' : 'slide'}
            style={{ animation :  (current && out ? 'fadeOut 1s' : '') }}
            key={index}
          >
            {index === current && (
              <img src={slide.image} alt='travel image' className='image' />
            )}
          </div>
        );
      }) :  <div id="dave-web-imagined-container">
          <div id="dave-web-imagined-name-container">
            <img src={require("./../resources/images/webNameBg.png")} alt="" id="dave-web-imagined-name-bg" />
            <span id="dave-web-imagined-name">{fulfilledName}</span>
          </div>
          <div id="dave-web-imagined-img-container">
            <img src={imageURL} alt="" id="dave-web-imagined-img" />
          </div>
          <div id="dave-web-imagined-text-container">
            <img src={require("./../resources/images/webImaginedTextBg.png")} alt="" id="dave-web-imagined-name-bg" />
            <span id="dave-web-imagined-text">I Imagine it to be {currentMessage}</span>
          </div>
          <div id="dave-web-upcoming-loader-container">
            <img src={require("./../resources/images/webLoader.png")} alt="" id="dave-web-upcoming-loader" />
          </div>
        </div>
      }
    </div>
  )
}
