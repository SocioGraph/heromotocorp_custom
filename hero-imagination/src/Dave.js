import React from "react";
import TabHome from "./Tablet/TabHome";
import Imagination from './Tablet/Imagination';
import Screen from './Web/Screen';
import Home from './Home';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import './Dave.css'

export default function Dave() {
  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/tab" element={<TabHome/>}></Route>
          <Route path="/imagination" element={<Imagination/>}></Route> 
          <Route path="/web" element={<Screen/>}></Route> 
          <Route path="*" element={<Home/>}/>
          <Route path="/" element={<Home/>}/>
        </Routes>
      </Router>
    </>
  )
}
